package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.print.attribute.standard.DateTimeAtCompleted;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Discount;
import sg.edu.nus.iss.se24.pt1.service.DiscountService;
import sg.edu.nus.iss.se24.pt1.service.DiscountServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.DiscountListTable;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.Font;

/**
 * Created by Myo Wai Yan Kyaw on 3/19/2016.
 * 
 * @author jinxiang
 */
public class DiscountPanel extends JPanel {
	private AdminstrationDialog parentDialog;
	private DiscountServiceI discountService = new DiscountService();

	private JTextField txtDiscountCode;
	private JTextArea txtDescription;
	private JTextField txtPeriodOfDiscount;
	private JTextField txtPercentOfDiscount;
	private JTextField txtStartDate;
	private JTable table;
	private JRadioButton rdbtnMember;
	private JRadioButton rdbtnAll;
	private JButton btnEdit;
	private JButton btnAdd;

	public DiscountPanel() {
		this(null);
	}

	public DiscountPanel(AdminstrationDialog dialog) {
		parentDialog = dialog;
		setLayout(new BorderLayout(0, 0));
		setSize(Constants.APP_DIMENSION);

		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		DiscountListTable tableModel = new DiscountListTable(listDiscount());
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel_3.add(scrollPane, BorderLayout.CENTER);

		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 140, 250, 0 };
		gbl_panel_4.rowHeights = new int[] { 33, 93, 33, 33, 33, 33, 0 };
		gbl_panel_4.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		JLabel lblDiscountCode = new JLabel(Constants.STRING_LABEL_DISCOUNT_CODE);
		GridBagConstraints gbc_lblDiscountCode = new GridBagConstraints();
		gbc_lblDiscountCode.fill = GridBagConstraints.BOTH;
		gbc_lblDiscountCode.insets = new Insets(0, 0, 5, 5);
		gbc_lblDiscountCode.gridx = 0;
		gbc_lblDiscountCode.gridy = 0;
		panel_4.add(lblDiscountCode, gbc_lblDiscountCode);

		txtDiscountCode = new JTextField();
		txtDiscountCode.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				txtDiscountCode.setText(txtDiscountCode.getText().toUpperCase());
			}
		});
		txtDiscountCode.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_txtDiscountCode = new GridBagConstraints();
		gbc_txtDiscountCode.fill = GridBagConstraints.BOTH;
		gbc_txtDiscountCode.insets = new Insets(0, 0, 5, 0);
		gbc_txtDiscountCode.gridx = 1;
		gbc_txtDiscountCode.gridy = 0;
		panel_4.add(txtDiscountCode, gbc_txtDiscountCode);
		txtDiscountCode.setColumns(10);

		JLabel lblDescription = new JLabel(Constants.STRING_LABEL_DISCOUNT_DESCRIPTION);
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.anchor = GridBagConstraints.NORTH;
		gbc_lblDescription.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 0;
		gbc_lblDescription.gridy = 1;
		panel_4.add(lblDescription, gbc_lblDescription);

		txtDescription = new JTextArea();
		GridBagConstraints gbc_txtDescription = new GridBagConstraints();
		gbc_txtDescription.fill = GridBagConstraints.BOTH;
		gbc_txtDescription.insets = new Insets(0, 0, 5, 0);
		gbc_txtDescription.gridx = 1;
		gbc_txtDescription.gridy = 1;
		panel_4.add(txtDescription, gbc_txtDescription);
		txtDescription.setColumns(10);

		JLabel lblStartDate = new JLabel("Start Date(YYYY-MM-DD)");
		GridBagConstraints gbc_lblStartDate = new GridBagConstraints();
		gbc_lblStartDate.fill = GridBagConstraints.BOTH;
		gbc_lblStartDate.insets = new Insets(0, 0, 5, 5);
		gbc_lblStartDate.gridx = 0;
		gbc_lblStartDate.gridy = 2;
		panel_4.add(lblStartDate, gbc_lblStartDate);

		txtStartDate = new JTextField();
		txtStartDate.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				txtStartDate.setText(txtStartDate.getText().toUpperCase());
				if(txtStartDate.getText().trim().equals("ALWAYS")) {
					txtPeriodOfDiscount.setText("ALWAYS");
				}
			}
		});
		GridBagConstraints gbc_txtStartDate = new GridBagConstraints();
		gbc_txtStartDate.fill = GridBagConstraints.BOTH;
		gbc_txtStartDate.insets = new Insets(0, 0, 5, 0);
		gbc_txtStartDate.gridx = 1;
		gbc_txtStartDate.gridy = 2;
		panel_4.add(txtStartDate, gbc_txtStartDate);

		JLabel lblPeriodOfDiscount = new JLabel(Constants.STRING_LABEL_DISCOUNT_PERIOD);
		GridBagConstraints gbc_lblPeriodOfDiscount = new GridBagConstraints();
		gbc_lblPeriodOfDiscount.fill = GridBagConstraints.BOTH;
		gbc_lblPeriodOfDiscount.insets = new Insets(0, 0, 5, 5);
		gbc_lblPeriodOfDiscount.gridx = 0;
		gbc_lblPeriodOfDiscount.gridy = 3;
		panel_4.add(lblPeriodOfDiscount, gbc_lblPeriodOfDiscount);

		txtPeriodOfDiscount = new JTextField();
		txtPeriodOfDiscount.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				txtPeriodOfDiscount.setText(txtPeriodOfDiscount.getText().toUpperCase());
			}
		});
		GridBagConstraints gbc_txtPeriodOfDiscount = new GridBagConstraints();
		gbc_txtPeriodOfDiscount.fill = GridBagConstraints.BOTH;
		gbc_txtPeriodOfDiscount.insets = new Insets(0, 0, 5, 0);
		gbc_txtPeriodOfDiscount.gridx = 1;
		gbc_txtPeriodOfDiscount.gridy = 3;
		panel_4.add(txtPeriodOfDiscount, gbc_txtPeriodOfDiscount);
		txtPeriodOfDiscount.setColumns(10);

		JLabel lblPercentOfDiscount = new JLabel(Constants.STRING_LABEL_DISCOUNT_PERCENT);
		GridBagConstraints gbc_lblPercentOfDiscount = new GridBagConstraints();
		gbc_lblPercentOfDiscount.fill = GridBagConstraints.BOTH;
		gbc_lblPercentOfDiscount.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercentOfDiscount.gridx = 0;
		gbc_lblPercentOfDiscount.gridy = 4;
		panel_4.add(lblPercentOfDiscount, gbc_lblPercentOfDiscount);

		txtPercentOfDiscount = new JTextField();
		GridBagConstraints gbc_txtPercentOfDiscount = new GridBagConstraints();
		gbc_txtPercentOfDiscount.fill = GridBagConstraints.BOTH;
		gbc_txtPercentOfDiscount.insets = new Insets(0, 0, 5, 0);
		gbc_txtPercentOfDiscount.gridx = 1;
		gbc_txtPercentOfDiscount.gridy = 4;
		panel_4.add(txtPercentOfDiscount, gbc_txtPercentOfDiscount);
		txtPercentOfDiscount.setColumns(10);

		JLabel lblMemberOrAll = new JLabel(Constants.STRING_LABEL_DISCOUNT_MEMBER_ALL);
		GridBagConstraints gbc_lblMemberOrAll = new GridBagConstraints();
		gbc_lblMemberOrAll.fill = GridBagConstraints.BOTH;
		gbc_lblMemberOrAll.insets = new Insets(0, 0, 0, 5);
		gbc_lblMemberOrAll.gridx = 0;
		gbc_lblMemberOrAll.gridy = 5;
		panel_4.add(lblMemberOrAll, gbc_lblMemberOrAll);

		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 1;
		gbc_panel_5.gridy = 5;
		panel_4.add(panel_5, gbc_panel_5);
		rdbtnMember = new JRadioButton(Constants.STRING_LABEL_DISCOUNT_MEMBER);
		rdbtnMember.setSelected(true);
		panel_5.add(rdbtnMember);

		rdbtnAll = new JRadioButton(Constants.STRING_LABEL_DISCOUNT_ALL);
		panel_5.add(rdbtnAll);

		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnMember);
		group.add(rdbtnAll);

		JPanel panel_2 = new JPanel();
		add(panel_2, BorderLayout.SOUTH);

		btnAdd = new JButton(Constants.BUTTON_ADD_LABEL);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDiscount();
			}
		});
		panel_2.add(btnAdd);

		btnEdit = new JButton(Constants.BUTTON_EDIT_LABEL);
		btnEdit.setEnabled(false);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editDiscount();
			}
		});
		panel_2.add(btnEdit);

		JButton btnClear = new JButton(Constants.BUTTON_CLEAR_LABEL);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearDiscount();
			}
		});
		panel_2.add(btnClear);

		JButton btnClose = new JButton(Constants.BUTTON_CLOSE_LABEL);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentDialog.close();
			}
		});
		panel_2.add(btnClose);

		ListSelectionModel selectionModel = table.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectRow = table.getSelectedRow();
				if (selectRow != -1) {
					setDiscount(tableModel.getList().get(selectRow));
				} else {
					clearDiscount();
				}
			}
		});
	}

	protected void editDiscount() {
		char flg = 'A';
		if (rdbtnMember.isSelected())
			flg = 'M';
		try {
			discountService.edit(txtDiscountCode.getText().trim(), txtDescription.getText().trim(), txtStartDate.getText().trim(),
					txtPeriodOfDiscount.getText().trim(), txtPercentOfDiscount.getText().trim(), flg);
			refreshDiscountList();
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

	private void addDiscount() {
		char flg = 'A';
		if (rdbtnMember.isSelected())
			flg = 'M';
		try {
			discountService.add(txtDiscountCode.getText().trim(), txtDescription.getText().trim(), txtStartDate.getText().trim(),
					txtPeriodOfDiscount.getText().trim(), txtPercentOfDiscount.getText().trim(), flg);
			refreshDiscountList();
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

	private Discount getDiscount() {
		char flg = 'A';
		if (rdbtnMember.isSelected())
			flg = 'M';
		return new Discount(txtDiscountCode.getText().trim(), txtDescription.getText().trim(),
				txtStartDate.getText().trim(), txtPeriodOfDiscount.getText().trim(),
				Integer.valueOf(txtPercentOfDiscount.getText().trim()), flg);

	}

	private void setDiscount(Discount discount) {
		txtDiscountCode.setText(discount.getDiscountCode());
		txtDiscountCode.setEnabled(false);
		txtDescription.setText(discount.getDescription());
		txtStartDate.setText(discount.getStartDate());
		txtPeriodOfDiscount.setText(discount.getPeriodOfDiscount());
		txtPercentOfDiscount.setText(discount.getPercentageDiscount().toString());
		btnAdd.setEnabled(false);
		btnEdit.setEnabled(true);
		if (discount.getMemberFlg() == 'M')
			rdbtnMember.setSelected(true);
		else
			rdbtnAll.setSelected(true);
	}

	private void clearDiscount() {
		txtDiscountCode.setText(null);
		txtDiscountCode.setEnabled(true);
		txtDescription.setText(null);
		txtStartDate.setText(null);
		txtPeriodOfDiscount.setText(null);
		txtPercentOfDiscount.setText(null);
		rdbtnMember.setSelected(true);
		rdbtnAll.setSelected(false);
		btnAdd.setEnabled(true);
		btnEdit.setEnabled(false);
	}

	private List<Discount> listDiscount() {
		return discountService.getAll();
	}
	
	private void refreshDiscountList() {
		((DiscountListTable) table.getModel()).fireTableDataChanged();

	}
}
