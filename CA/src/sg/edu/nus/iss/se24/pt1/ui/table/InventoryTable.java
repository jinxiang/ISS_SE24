/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Inventory;

/**
 * @author jinxiang
 *
 */
public class InventoryTable extends AbstractTableModel {
	private final List<Inventory> inventoryList;

	/**
	 * @param inventoryList
	 */
	public InventoryTable(List<Inventory> inventoryList) {
		this.inventoryList = inventoryList;
	}

	private final String[] columnNames = new String[] { "Product ID", "Product Name", "Price", "Quantity Available",
			"Threshold", "Order Quantity" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class, Double.class, Integer.class, Integer.class,
			Integer.class };

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111602267312702310L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return inventoryList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Inventory row = inventoryList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getProductId();
		} else if (1 == columnIndex) {
			return row.getProductName();
		} else if (2 == columnIndex) {
			return row.getPrice();
		} else if (3 == columnIndex) {
			return row.getQuantityAvailable();
		} else if (4 == columnIndex) {
			return row.getThreshold();
		} else if (5 == columnIndex) {
			return row.getOrderQuantity();
		}
		return null;
	}

}
