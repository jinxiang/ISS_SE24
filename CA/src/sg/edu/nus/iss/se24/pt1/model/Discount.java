package sg.edu.nus.iss.se24.pt1.model;

/**
 * Created by Myo Wai Yan Kyaw on 3/13/2016.
 */

public class Discount {
	
	private String discountCode;
	private String description;
	private String startDate;
	private String periodOfDiscount;
	private Integer percentageDiscount;
	private Character memberFlg;
	
	public Discount() {
		
	}
	
	/**
	 * @param discountCode
	 * @param description
	 * @param startDate
	 * @param periodOfDiscount
	 * @param percentageDiscount
	 * @param memberFlg
	 */
	public Discount(String discountCode, String description, String startDate, String periodOfDiscount,
			Integer percentageDiscount, Character memberFlg) {
		this.discountCode = discountCode;
		this.description = description;
		this.startDate = startDate;
		this.periodOfDiscount = periodOfDiscount;
		this.percentageDiscount = percentageDiscount;
		this.memberFlg = memberFlg;
	}

	/**
	 * @return the discountCode
	 */
	public String getDiscountCode() {
		return discountCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @return the periodOfDiscount
	 */
	public String getPeriodOfDiscount() {
		return periodOfDiscount;
	}

	/**
	 * @return the percentageDiscount
	 */
	public Integer getPercentageDiscount() {
		return percentageDiscount;
	}

	/**
	 * @return the memberFlg
	 */
	public Character getMemberFlg() {
		return memberFlg;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return discountCode + "," + description + "," + startDate + "," + periodOfDiscount + "," + percentageDiscount + "," + memberFlg;
	}
}
