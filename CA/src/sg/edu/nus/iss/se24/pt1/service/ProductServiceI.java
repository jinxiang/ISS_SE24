/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.ProductCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Inventory;
import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.model.Transaction;

/**
 * created by Nilar
 * 
 * @author jinxiang
 *
 */
public interface ProductServiceI {

	public boolean add(String productID, String productName, String description, String availableQty, String price,
			String barCodeNo, String reorderQty, String orderQty)
			throws ProductCodeInvalidException, ValidationException, ProductCode_BarCodeExistException;

	public boolean edit(String productID, String productName, String description, String availableQty, String price,
			String barCodeNo, String reorderQty, String orderQty)
			throws ProductCodeInvalidException, ValidationException;

	public boolean remove(String productID);

	public String getProductID(String CategoryID);

	public boolean isDuplicate(String productName, String categoryCode) throws ValidationException;

	public ArrayList<Product> getAll();

	public ArrayList<Product> getProductByCategory(String categoryCode);

	public boolean removeProductByCategory(String category);

	public List<Inventory> getInventoryList();

	public List<Transaction> getTransactionList(Date transactDate);

	public Product findByProductID(String productID);
	
	public void genereateInventoryList();
	
	public Product findByBarCode(String BarCode);
	
	public boolean isExistProductUnderThreadshold();
}
