/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Member;;

//Created by Nguwar

public class MemberListTable extends AbstractTableModel {
	private final List<Member> memberList;

	/**
	 * @param inventoryList
	 */
	public MemberListTable(List<Member> memberList) {
		this.memberList = memberList;
	}

	private final String[] columnNames = new String[] { "Member ID", "Member Name", "Loyalty Point" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class, Integer.class };

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111602267312702310L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return memberList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Member row = memberList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getMemberID();
		} else if (1 == columnIndex) {
			return row.getMemberName();
		} else if (2 == columnIndex) {
			return row.GetLoyaltyPointAccumulated();
		}
		return null;
	}

	/**
	 * @return the memberList
	 */
	public List<Member> getList() {
		return memberList;
	}
}
