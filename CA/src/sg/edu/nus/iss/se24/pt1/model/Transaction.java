package sg.edu.nus.iss.se24.pt1.model;

import java.text.ParseException;
import java.util.Date;

import sg.edu.nus.iss.se24.pt1.dao.Transformer;

//created by Nguwar
public class Transaction {

	private Integer transactionID;
	private String productID;
	private String memberID;
	private Integer purchasedQuantity;
	private Date transactionDate;

	private Product product;
	private Member member;

	// default constructor
	public Transaction() {
	}

	// constructor 
	public Transaction(int TransactionID, String ProductID, String MemberID, int PurchasedQuantity) {
		this.transactionID = TransactionID;
		this.productID = ProductID;
		if (MemberID == "" || MemberID == null)
			this.memberID = "PUBLIC";
		else
			this.memberID = MemberID;
		this.purchasedQuantity = PurchasedQuantity;

		this.transactionDate = new Date();
	}

	public Integer getTransactionID() {
		return this.transactionID;
	}

	public String getProductID() {
		return this.productID;
	}

	public String getMemberID() {
		return this.memberID;
	}

	public Integer getPurchasedQuantity() {
		return this.purchasedQuantity;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public void setPurchaseQuantity(Integer purchasedQuantity) {
		this.purchasedQuantity = purchasedQuantity;
	}

	public String toString() {
		try {
			return this.getTransactionID() + "," + this.getProductID() + "," + this.getMemberID() + ","
					+ this.getPurchasedQuantity() + "," + Transformer.dateToString(getTransactionDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String showLineForReceipt(){
		Double total = this.getProduct().getPrice() * this.getPurchasedQuantity();
		
		return String.format("%1$10s", this.getProductID()) +String.format("%1$5s", this.getPurchasedQuantity()) + String.format("%1$5s", this.getProduct().getPrice()) 
				+ String.format("%1$8s", total);
	}

}
