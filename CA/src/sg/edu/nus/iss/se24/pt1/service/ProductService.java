package sg.edu.nus.iss.se24.pt1.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.edu.nus.iss.se24.pt1.dao.ProductDAO;
import sg.edu.nus.iss.se24.pt1.dao.ProductDAOImpl;
import sg.edu.nus.iss.se24.pt1.dao.TransactionDAO;
import sg.edu.nus.iss.se24.pt1.dao.TransactionDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.ProductCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Inventory;
import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.model.Transaction;

/**
 * 
 * @author jinxiang, Nilar
 *
 */
public class ProductService implements ProductServiceI {

	ProductDAO productDAO;
	TransactionDAO transactionDAO;

	public ProductService() {
		productDAO = ProductDAOImpl.getInstance();
		transactionDAO = TransactionDAOImpl.getInstance();
	}

	@Override
	public boolean add(String productID, String productName, String description, String availableQty, String price,
			String barCodeNo, String reorderQty, String orderQty)
			throws ProductCodeInvalidException, ValidationException, ProductCode_BarCodeExistException {
		validateProduct(productID, productName, description, availableQty, price, barCodeNo, reorderQty, orderQty);
		String productIDRegx = "^([A-Z]{3})[/][0-9]$";
		Pattern pattern;
		Matcher matcher;
		pattern = Pattern.compile(productIDRegx);
		matcher = pattern.matcher(productID);

		if (!matcher.matches())
			throw new ProductCodeInvalidException();
		else {
			boolean getResult=productDAO.insertProduct(new Product(productID.trim(), productName.trim(), description.trim(),
					Integer.parseInt(availableQty), Double.parseDouble(price), barCodeNo.trim(), Integer.parseInt(reorderQty),
					Integer.parseInt(orderQty)));
			if (getResult){
				productDAO.commit();
			}
			else
			{
				throw new ProductCode_BarCodeExistException();
			}
			return getResult;
			
		}
	}

	@Override
	public boolean edit(String productID, String productName, String description, String availableQty, String price,
			String barCodeNo, String reorderQty, String orderQty) throws ValidationException {
		validateProduct(productID, productName, description, availableQty, price, barCodeNo, reorderQty, orderQty);
		boolean getResult= productDAO.updateProduct(
				new Product(productID.trim(), productName.trim(), description.trim(),
						Integer.parseInt(availableQty.trim()), Double.parseDouble(price.trim()), barCodeNo.trim(), Integer.parseInt(reorderQty.trim()),
						Integer.parseInt(orderQty.trim())));
		if(getResult){
			productDAO.commit();
		}
		return getResult;

	}

	@Override
	public boolean remove(String productID) {
		return productDAO.deleteProduct(productID);

	}

	@Override
	public String getProductID(String categoryCode) {
		ArrayList<Product> produtlist = this.getProductByCategory(categoryCode);
		Product latestproduct = new Product();
		if (produtlist.size() > 0) {
			latestproduct = produtlist.stream().max((a, b) -> Integer.compare(getIntValueProductID(a.getProductID()),
					getIntValueProductID(b.getProductID()))).get();
			return (categoryCode + '/' + (getIntValueProductID(latestproduct.getProductID()) + 1));
		} else {
			return categoryCode + '/' + 1;
		}
	}

	@Override
	public boolean isDuplicate(String productName, String categoryCode) throws ValidationException {
		if (productName == null || productName.trim().length() == 0)
			throw new ValidationException("Product", "Product Name Is Empty.");
		ArrayList<Product> productList = this.getProductByCategory(categoryCode);
		for (Product p : productList) {
			if (p.getProductName().equals(productName.trim())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ArrayList<Product> getAll() {
		return (ArrayList<Product>) productDAO.findAll();
	}

	@Override
	public ArrayList<Product> getProductByCategory(String categoryCode) {
		return (ArrayList<Product>) productDAO.findByCategory(categoryCode);
	}

	@Override
	public boolean removeProductByCategory(String categoryCode) {
		ArrayList<Product> lstProduct = new ArrayList<Product>();
		lstProduct = this.getProductByCategory(categoryCode);
		if (lstProduct.size() > 0) {
			for (Product p : lstProduct) {
				if (!productDAO.deleteProduct(p.getProductID()))
					return false;
			}
		}
		return true;
	}

	private int getIntValueProductID(String completeProductID) {
		String getProductID = completeProductID;
		String intValueProductID = getProductID.substring(getProductID.indexOf('/') + 1, getProductID.length());
		return Integer.parseInt(intValueProductID);
	}

	@Override
	public List<Inventory> getInventoryList() {
		List<Inventory> inventoryList = new ArrayList<Inventory>();
		for (Product product : getAll()) {
			if (product.getAvailableQty() <= product.getReOrderQty()) {
				Inventory inventory = new Inventory();
				inventory.setProductId(product.getProductID());
				inventory.setProductName(product.getProductName());
				inventory.setPrice(product.getPrice());
				inventory.setQuantityAvailable(product.getAvailableQty());
				inventory.setThreshold(product.getReOrderQty());
				inventory.setOrderQuantity(product.getOrderQty());
				inventoryList.add(inventory);
			}
		}
		return inventoryList;
	}

	@Override
	public List<Transaction> getTransactionList(Date transactDate) {
		List<Transaction> list = null;
		if (transactDate == null) {
			list = getTransactionList();
		} else {
			list = transactionDAO.findByDate(transactDate);
		}
		for (Transaction transaction : list) {
			Product product = productDAO.findByProductID(transaction.getProductID());
			transaction.setProduct(product);
		}
		return list;
	}

	@Override
	public Product findByProductID(String productID) {
		Product product = new Product();
		List<Product> listProduct = this.getAll();
		for (Product p : listProduct) {
			if (p.getProductID().equals(productID)) {
				product = p;
				return product;

			}
		}
		return null;
	}

	private List<Transaction> getTransactionList() {
		List<Transaction> list = transactionDAO.findAll();
		for (Transaction transaction : list) {
			Product product = productDAO.findByProductID(transaction.getProductID());
			transaction.setProduct(product);
		}
		return list;
	}

	private void validateProduct(String productID, String productName, String description, String availableQty,
			String price, String barCodeNo, String reorderQty, String orderQty) throws ValidationException {
		if (productID == null || productID.trim().length() == 0)
			throw new ValidationException("Product", "Product ID Is Empty.");
		if (productName == null || productName.trim().length() == 0)
			throw new ValidationException("Product", "Product Name Is Empty.");
		if (description == null || description.trim().length() == 0)
			throw new ValidationException("Product", "Product Description Is Empty.");
		if (availableQty == null || availableQty.trim().length() == 0)
			throw new ValidationException("Product", "Available Quanlity Is Empty.");
		try {
			Integer.parseInt(availableQty);
		} catch (NumberFormatException e) {
			throw new ValidationException("Product", "Available Quanlity must be an Integer");
		}
		if (price == null || price.trim().length() == 0)
			throw new ValidationException("Product", "Price Is Empty.");
		try {
			Double.parseDouble(price);
		} catch (NumberFormatException e) {
			throw new ValidationException("Product", "Price must be an Double");
		}
		if (barCodeNo == null || barCodeNo.trim().length() == 0)
			throw new ValidationException("Product", "Product Bar Code Is Empty.");
		if (reorderQty == null || reorderQty.trim().length() == 0)
			throw new ValidationException("Product", "Threshold Quanlity Is Empty.");
		try {
			Integer.parseInt(reorderQty);
		} catch (NumberFormatException e) {
			throw new ValidationException("Product", "Threshold Quanlity must be valid numeric value");
		}
		if (orderQty == null || orderQty.trim().length() == 0)
			throw new ValidationException("Product", "OrderQty Is Empty.");
		try {
			Integer.parseInt(orderQty);
		} catch (NumberFormatException e) {
			throw new ValidationException("Product", "OrderQty must be an Integer");
		}
		if (description.matches("\\d+")){
			throw new ValidationException("Product", "Product Description must not be numeric value");
			
		}
	}
	
	public Product findByBarCode(String BarCode){
		ArrayList<Product> lstProduct = this.getAll();
		for(Product product: lstProduct){
			if(product.getBarCodeNo().equals(BarCode)){
				return product;
			}
		}
		return null;
		
	}

	@Override
	public void genereateInventoryList() {
		List<Inventory> list = getInventoryList();
		productDAO.insertPurchasedOrder(list);
	}
	
	@Override
	public boolean isExistProductUnderThreadshold(){
		List<Inventory>lstInventory= this.getInventoryList();
		if (lstInventory.size()>0)
		{
			return true;
		}
		else{
			return false;
		}
			
	}
}
