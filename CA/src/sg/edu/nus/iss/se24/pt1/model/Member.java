package sg.edu.nus.iss.se24.pt1.model;

//created by Nguwar
public class Member {    
	   
	private String memberName;
	private String memberID;
	private Integer loyaltyPointAccumulated;
	
	//default constructor
	public Member(){}
	
	//constructor for new member
	public Member(String MemberName, String MemberID) {
		this.memberName = MemberName;
		this.memberID = MemberID;
		this.loyaltyPointAccumulated = -1;
	}
	
	//another constructor for loyalty point input
	public Member(String MemberName, String MemberID, Integer LoyaltyPoint){
		this.memberName = MemberName;
		this.memberID = MemberID;
		this.loyaltyPointAccumulated = LoyaltyPoint;
	}
	
	public String getMemberName(){
		return this.memberName;
	}
	
	public String getMemberID(){
		return this.memberID;
	}
	
	public Integer GetLoyaltyPointAccumulated(){
		return this.loyaltyPointAccumulated;
	}
	
	public String toString(){
		return this.getMemberName()+","+this.getMemberID()+","+this.GetLoyaltyPointAccumulated();
	}
	
}
