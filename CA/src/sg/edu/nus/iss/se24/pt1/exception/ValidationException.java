/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.exception;

/**
 * @author jinxiang
 *
 */
public class ValidationException extends Exception {
	private String clazz;
	private String message;
	/**
	 * @param clazz
	 * @param message
	 */
	public ValidationException(String clazz, String message) {
		super(message);
		this.clazz = clazz;
		this.message = message;
	}
	/**
	 * @return the clazz
	 */
	public String getClazz() {
		return clazz;
	}
	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
