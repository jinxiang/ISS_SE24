package sg.edu.nus.iss.se24.pt1.exception;

/**
 * Created by xinqu on 4/2/2016.
 */
public class VendorNotExistException extends Throwable {
    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    private String clazz;

    public VendorNotExistException(String claszz, String vendorName) {
        super(String.format(STRING_ERROR_FORMAT, vendorName));
        this.clazz=claszz;
    }

    private static final String STRING_ERROR_FORMAT = "Venfor Name : %s does not exist.";
}
