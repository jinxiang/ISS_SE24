package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import sg.edu.nus.iss.se24.pt1.model.Vendor;
import sg.edu.nus.iss.se24.pt1.service.CategoryService;
import sg.edu.nus.iss.se24.pt1.service.CategoryServiceI;
import sg.edu.nus.iss.se24.pt1.service.VendorService;
import sg.edu.nus.iss.se24.pt1.ui.table.VendorListTable;

/**
 * Created by Nguwar
 * 
 * @author jinxiang
 *
 */
public class VendorPanel extends JPanel {
	private AdminstrationDialog parentDialog;
	private CategoryServiceI categoryService = new CategoryService();
	private JTextField txtVendorName;
	private JTextField txtDescription;
	private JTable table;
	private JComboBox<String> cmbCategory;
	private JLabel lblCategoryName;

	public VendorPanel() {
		this(null);
	}

	public VendorPanel(AdminstrationDialog dialog) {
		parentDialog = dialog;
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(Constants.APP_DIMENSION);

		// Title Panel
		JPanel TitlePanel = new JPanel();
		add(TitlePanel, BorderLayout.NORTH);
		TitlePanel.setLayout(new GridLayout(1, 1, 0, 0));
		{
			JPanel FilterPanel = new JPanel();
			FlowLayout fl_FilterPanel = (FlowLayout) FilterPanel.getLayout();
			fl_FilterPanel.setAlignment(FlowLayout.LEFT);
			TitlePanel.add(FilterPanel);
			{
				JLabel lblCategory1 = new JLabel(Constants.STRING_LABEL_TAB_CATEGORY);
				FilterPanel.add(lblCategory1);
			}
			{
				cmbCategory = new JComboBox(categoryService.listCategoryCode());
				cmbCategory.addItemListener(new ItemListener() {

					@Override
					public void itemStateChanged(ItemEvent e) {
						generateSelectedVendor(e.getItem().toString());
					}
				});
				FilterPanel.add(cmbCategory);
			}
		}

		// Content Panel
		JPanel ContentPanel = new JPanel();
		add(ContentPanel, BorderLayout.CENTER);
		ContentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		{
			JPanel ListPanel = new JPanel();
			ContentPanel.add(ListPanel);
			ListPanel.setLayout(new BorderLayout(0, 0));
			{
				table = new JTable();
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				JScrollPane scrollPane = new JScrollPane(table);
				scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				ListPanel.add(scrollPane, BorderLayout.CENTER);
			}
		}
		{
			JPanel DetailPanel = new JPanel();
			ContentPanel.add(DetailPanel);
			GridBagLayout gbl_DetailPanel = new GridBagLayout();
			gbl_DetailPanel.columnWidths = new int[] { 134, 134, 0 };
			gbl_DetailPanel.rowHeights = new int[] {33, 33, 33, 33, 33};
			gbl_DetailPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
			gbl_DetailPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
			DetailPanel.setLayout(gbl_DetailPanel);
			{
				JLabel lblVendorName = new JLabel(Constants.STRING_LABEL_VENDOR_NAME);
				GridBagConstraints gbc_lblVendorName = new GridBagConstraints();
				gbc_lblVendorName.fill = GridBagConstraints.BOTH;
				gbc_lblVendorName.insets = new Insets(0, 0, 5, 5);
				gbc_lblVendorName.gridx = 0;
				gbc_lblVendorName.gridy = 0;
				DetailPanel.add(lblVendorName, gbc_lblVendorName);
			}
			{
				txtVendorName = new JTextField();
				GridBagConstraints gbc_txtVendorName = new GridBagConstraints();
				gbc_txtVendorName.fill = GridBagConstraints.BOTH;
				gbc_txtVendorName.insets = new Insets(0, 0, 5, 0);
				gbc_txtVendorName.gridx = 1;
				gbc_txtVendorName.gridy = 0;
				DetailPanel.add(txtVendorName, gbc_txtVendorName);
				txtVendorName.setColumns(10);
			}
			{
				JLabel lblDescription = new JLabel(Constants.STRING_LABEL_VENDOR_DESCRIPTION);
				GridBagConstraints gbc_lblDescription = new GridBagConstraints();
				gbc_lblDescription.fill = GridBagConstraints.BOTH;
				gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
				gbc_lblDescription.gridx = 0;
				gbc_lblDescription.gridy = 1;
				DetailPanel.add(lblDescription, gbc_lblDescription);
			}
			{
				txtDescription = new JTextField();
				GridBagConstraints gbc_txtDescription = new GridBagConstraints();
				gbc_txtDescription.insets = new Insets(0, 0, 5, 0);
				gbc_txtDescription.fill = GridBagConstraints.BOTH;
				gbc_txtDescription.gridx = 1;
				gbc_txtDescription.gridy = 1;
				DetailPanel.add(txtDescription, gbc_txtDescription);
				txtDescription.setColumns(10);
			}
			{
				JLabel lblCategory2 = new JLabel(Constants.STRING_LABEL_TAB_CATEGORY);
				GridBagConstraints gbc_lblCategory2 = new GridBagConstraints();
				gbc_lblCategory2.anchor = GridBagConstraints.WEST;
				gbc_lblCategory2.insets = new Insets(0, 0, 5, 5);
				gbc_lblCategory2.gridx = 0;
				gbc_lblCategory2.gridy = 2;
				DetailPanel.add(lblCategory2, gbc_lblCategory2);
			}
			{
				lblCategoryName = new JLabel(Constants.STRING_LABEL_CATEGORY_NAME);
				lblCategoryName.setEnabled(false);
				GridBagConstraints gbc_lblCategoryName = new GridBagConstraints();
				gbc_lblCategoryName.anchor = GridBagConstraints.WEST;
				gbc_lblCategoryName.insets = new Insets(0, 0, 5, 0);
				gbc_lblCategoryName.gridx = 1;
				gbc_lblCategoryName.gridy = 2;
				DetailPanel.add(lblCategoryName, gbc_lblCategoryName);
			}
		}

		// Button Panel
		JPanel ButtonPanel = new JPanel();
		add(ButtonPanel, BorderLayout.SOUTH);
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		JButton btnAdd = new JButton(Constants.BUTTON_ADD_LABEL);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addVendor();
			}
		});
		ButtonPanel.add(btnAdd);
		JButton btnEdit = new JButton(Constants.BUTTON_EDIT_LABEL);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editVendor();
			}
		});
		ButtonPanel.add(btnEdit);
		// JButton btnRemove = new JButton(Constants.BUTTON_DELETE_LABEL);
		// ButtonPanel.add(btnRemove);
		JButton btnClear = new JButton(Constants.BUTTON_CLEAR_LABEL);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearVendor();
			}
		});
		ButtonPanel.add(btnClear);
		JButton btnClose = new JButton(Constants.BUTTON_CLOSE_LABEL);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentDialog.close();
			}
		});
		ButtonPanel.add(btnClose);

		ListSelectionModel selectionModel = table.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectRow = table.getSelectedRow();
				if (selectRow != -1) {
					setVendor(((VendorListTable) table.getModel()).getList().get(selectRow));
				} else {
					clearVendor();
				}
			}

		});
		if(cmbCategory.getSelectedItem() != null) {
			generateSelectedVendor(cmbCategory.getSelectedItem().toString());
		}
	}

	protected void editVendor() {
		boolean flg;
		try {
			flg = new VendorService().edit(lblCategoryName.getText(), txtVendorName.getText(), txtDescription.getText());
			if (flg) {
				generateSelectedVendor(lblCategoryName.getText().trim());
			}
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}catch (VendorNotExistException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

	protected void addVendor() {
		boolean flg;
		try {
			flg = new VendorService().add(lblCategoryName.getText(), txtVendorName.getText(), txtDescription.getText());
			if (flg) {
				generateSelectedVendor(lblCategoryName.getText().trim());
			}
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}catch (VendorExistedException e){
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

	protected void generateSelectedVendor(String string) {
		List<Vendor> list = new VendorService().getAllByCategoryCode(string);
		VendorListTable tableModel = new VendorListTable(list);
		table.setModel(tableModel);
		clearVendor();
		lblCategoryName.setText(string);
		validate();
	}

	private void setVendor(Vendor vendor) {
		txtVendorName.setText(vendor.getVendorName());
		txtDescription.setText(vendor.getDescription());
	}

	private Vendor getVendor() {
		Vendor vendor = new Vendor(txtVendorName.getText().trim(), txtDescription.getText().trim());
		return vendor;
	}

	private void clearVendor() {
		txtVendorName.setText(null);
		txtDescription.setText(null);
		txtVendorName.requestFocus(true);
	}
	
	private void refreshVendor() {
		((VendorListTable) table.getModel()).fireTableDataChanged();

	}
}
