package sg.edu.nus.iss.se24.pt1.dao;

import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.model.Inventory;
import sg.edu.nus.iss.se24.pt1.model.Product;

/**
 * created by Nilar
 *
 */
public interface ProductDAO {

	public List<Product> findAll();

	public Product findByName(String name);

	public Product findByProductID(String productID);

	public List<Product> findByThresholdQty();

	public List<Product> findByCategory(String categoryCode);

	public boolean insertProduct(Product product) throws ProductCode_BarCodeExistException;

	public boolean updateProduct(Product product);

	public boolean deleteProduct(String productID);
	
	public void insertPurchasedOrder(List<Inventory> inventoryList);

	void commit();
	
	public Product findByBarCode(String barCode);
}
