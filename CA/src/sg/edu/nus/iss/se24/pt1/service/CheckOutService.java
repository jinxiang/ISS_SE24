package sg.edu.nus.iss.se24.pt1.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JOptionPane;

import sg.edu.nus.iss.se24.pt1.model.Transaction;
import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.model.CheckOut;
import sg.edu.nus.iss.se24.pt1.model.Discount;
import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.dao.TransactionDAO;
import sg.edu.nus.iss.se24.pt1.dao.TransactionDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.ProductCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.dao.ProductDAO;
import sg.edu.nus.iss.se24.pt1.dao.ProductDAOImpl;
import sg.edu.nus.iss.se24.pt1.dao.DiscountDAO;
import sg.edu.nus.iss.se24.pt1.dao.DiscountDAOImpl;
import sg.edu.nus.iss.se24.pt1.dao.MemberDAO;
import sg.edu.nus.iss.se24.pt1.dao.MemberDAOImpl;

//Created by Nguwar

public class CheckOutService implements CheckOutServiceI {
	
	private TransactionDAO database;
	private DiscountServiceI discountService;
	private ProductServiceI productService;
	private MemberServiceI memberService;
	
	//To keep all items in one sales order.
	private CheckOut model;

	public CheckOutService() {
		database = TransactionDAOImpl.getInstance();
		
		productService = new ProductService();
		discountService = new DiscountService();
		memberService = new MemberService();
		
		model = new CheckOut();
		setDiscount();
	}
	
	//Call this method after got member ID on UI
	@Override
	public void updateMemberID(String MemberID){
		model.member = memberService.getByMemberID(MemberID);
		
		if(model.member != null)
		{
			model.memberID = MemberID;
			ArrayList<Transaction> tempList = new ArrayList<Transaction>();
			for(Transaction t : model.list){
				Transaction newTransaction = new Transaction(t.getTransactionID(),t.getProductID(),model.memberID,t.getPurchasedQuantity());
				
				Product product = productService.findByProductID(t.getProductID());
				newTransaction.setProduct(product);
				
				tempList.add(newTransaction);
			}
			
			model.list.clear();
			model.list.addAll(tempList);
			
			this.setDiscount();
			this.setRedeemPoint();
			this.refresh();
		}
	}

	//Call this method when add item on UI
	@Override
	public Transaction addLine(String productBarcode, Integer quantity){
		Product product = productService.findByBarCode(productBarcode);
		
		Transaction newTransaction = new Transaction(this.getNewTransactionID(), product.getProductID(), model.memberID, quantity);
		
		newTransaction.setProduct(product);
		
		model.list.add(newTransaction);
		
		model.tenderedAmount = 0.00;
		this.refresh();
		
		return newTransaction;
	}
	
	//Call this method when edit item on UI
	@Override
	public Transaction editLine(Transaction transactionToEdit, Integer quantitytoEdit){
		transactionToEdit.setPurchaseQuantity(quantitytoEdit);
		
		model.tenderedAmount = 0.00;
		this.refresh();
		
		return transactionToEdit;
	}
	
	//Call this method when delete item on UI
	@Override
	public boolean deleteLine(Transaction transactionToDelete){
		model.list.remove(transactionToDelete);
		
		model.tenderedAmount = 0.00;
		this.refresh();
		
		return true;
	}
	
	@Override
	public boolean makePayment(){
		boolean getResult = database.insertTransaction(model.list);
		if(getResult){
			try {
				this.rewardPoint();
				this.updateInventoryQty();
				database.commit();
				
			} catch (ValidationException e) {
				getResult = false;
			} catch (ProductCodeInvalidException e) {
				getResult = false;
			} finally {
				//
			}
		}
		return getResult;
	}
	
	//Call this method when chose to print receipt on UI
	@Override
	public void printReceipt(){
		Date today = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		//print to console
		System.out.println("***************RECEIPT****************");
		System.out.println("DATE : "+ dateFormat.format(today));
		System.out.println("CUSTOMER : "+ model.memberID);
		System.out.println("--------------------------------------");
		System.out.println(String.format("%1$3s", "NO") + String.format("%1$10s", "ITEM") + String.format("%1$5s", "QTY") + String.format("%1$5s", "QTY") + String.format("%1$8s", "AMOUNT"));
		System.out.println("--------------------------------------");
		Integer i = 1;
		for(Transaction t : model.list){
			System.out.println(String.format("%1$3s", i++) + t.showLineForReceipt());
		}
		System.out.println("--------------------------------------");
		System.out.println("TOTAL : " + model.totalAmount.toString());
		if(model.discountPercentage != 0)
			System.out.println("DISCOUNT("+model.discountPercentage.toString()+"): " + model.discountAmount.toString());
		System.out.println("--------------------------------------");
		System.out.println("BALANCE : " + model.totalAmount.toString());
		System.out.println();
		System.out.println("TENDERED AMOUNT : " + model.tenderedAmount.toString());
		if(model.redeemedPoint != 0)
			System.out.println("REDEEM("+model.redeemedPoint.toString()+" Pts): " + model.redeemAmount.toString());
		System.out.println("--------------------------------------");
		System.out.println("CHANGE : " + model.change.toString());
		System.out.println("**************************************");
	}
	
	//Call this method when click redeem point button on UI(should call this method only one time)
	@Override
	public void redeemPoint(){
		if(model.memberID.equalsIgnoreCase("PUBLIC") == false){
			if(model.member != null){
				model.redeemedPoint = model.pointToRedeem;
				model.redeemAmount = (double) (model.redeemedPoint/20);
				model.pointToRedeem = 0;
				this.refresh();
			}
		}
	}
	
	@Override
	public Double getTotalAmount(){
		return model.totalAmount;
	}
	
	@Override
	public Double getDiscountAmount(){
		return model.discountAmount;
	}
	
	@Override
	public Double getRedeemAmount(){
		return model.redeemAmount;
	}
	
	@Override
	public Integer getRedeemedPoint(){
		return model.redeemedPoint;
	}
	
	@Override
	public Integer getAvailablePointToRedeem(){
		return model.pointToRedeem;
	}
	
	@Override
	public List<Transaction> getLines(){
		return model.list;
	}
	
	@Override
	public Integer getDiscountPercentage(){
		return model.discountPercentage;
	}
	
	@Override
	public Member getMember(){
		return model.member;
	}
	
	@Override
	public Double getTenderedAmount(){
		return model.tenderedAmount;
	}
	
	@Override
	public Double getBalance(){
		return model.balance;
	}
	
	@Override
	public Double getChange(){
		return model.change;
	}
	
	@Override
	//will auto call this method to get new transaction number
	public Integer getNewTransactionID(){
		List<Transaction> transactionArrayList = database.findAll();
			
		Integer ID = 1;
			
		if(transactionArrayList != null)
		{
			Transaction latestTransaction = transactionArrayList.stream().
				max((a,b)->Integer.compare(a.getTransactionID(), b.getTransactionID())).
				get();
			ID = (latestTransaction != null)? (latestTransaction.getTransactionID() + 1) : 1;
		}
		
		return ID;  
	}
		
	@Override
	public void setTenderedAmount(Double tenderedAmount){
		model.tenderedAmount = tenderedAmount;
		this.refresh();
	}
	
	@Override
	public boolean hasBelowThresholdLevel(){
		return productService.isExistProductUnderThreadshold();
	}
	
	@Override
	public boolean isProductAlreadyAdded(String productBarCode){
		Product product = productService.findByBarCode(productBarCode);
		
		if(product != null && model.list.size() > 0){
			for(Transaction t : model.list){
				if(product.getProductID().equals(t.getProductID()))
					return true;
			}
		}
		
		return false;
	}
	
	//will auto call this method every time add/edit/update/redeem/read member ID
	private void refresh(){
		calculateTotalAmount();
	}
	
	//will auto call this method to calculate Total Amount
	private void calculateTotalAmount(){
		model.totalAmount = 0.00;
		
		if(model.list.size() > 0){
			for(Transaction t : model.list){
				String productID = t.getProductID();
				Product product = productService.findByProductID(productID);
				if(product != null)
					model.totalAmount += t.getPurchasedQuantity() * product.getPrice();
			}
		
			//discount
			model.discountAmount = (model.totalAmount * model.discountPercentage)/100;
			
			//balance
			model.balance = model.totalAmount - model.discountAmount;
			
			//change
			if(model.tenderedAmount > 0)
				model.change = (model.tenderedAmount + model.redeemAmount ) - model.balance;
			else
				model.change = 0.00;
		}
		else{
			model.totalAmount = 0.00;
			model.discountAmount = 0.00;
			model.balance = 0.00;
			model.tenderedAmount = 0.00;
			model.redeemAmount = 0.00;
			model.change = 0.00;
			setRedeemPoint();
		}
	}
	
	//will auto call this method for rewarding point to customer after made payment
	private void rewardPoint() throws ValidationException{
		if(model.memberID.equalsIgnoreCase("PUBLIC") == false){
			
			Integer pointToUpdate = 0;
			
			if(model.totalAmount >= 10){

				Integer pointToReward = (int) (model.totalAmount/ 10);
				
				Integer loyaltyPoint = (model.member.GetLoyaltyPointAccumulated() == -1)? 0: model.member.GetLoyaltyPointAccumulated();
				
				pointToUpdate =	loyaltyPoint - model.redeemedPoint + pointToReward;
				
				memberService.edit(model.member.getMemberID(), model.member.getMemberName(), pointToUpdate.toString());
			
			} else if(model.member.GetLoyaltyPointAccumulated() == - 1){
				memberService.edit(model.member.getMemberID(), model.member.getMemberName(), pointToUpdate.toString());
			}
		}
	}
	
	//will auto call this method to set discount in constructor/read member id
	private void setDiscount(){
		model.discountPercentage = discountService.getDiscountPercent(model.memberID);
		
		//First time member will get 20% discount
		if(model.member != null){
			if(model.discountPercentage < 20 & model.member.GetLoyaltyPointAccumulated() == -1) 
				model.discountPercentage = 20;
		}
	}
	
	//will auto call this method to set redeem point in read member id
	private void setRedeemPoint(){
		if(model.memberID.equalsIgnoreCase("PUBLIC") == false){
			if(model.member != null){
				
				model.pointToRedeem = (model.member.GetLoyaltyPointAccumulated() == -1)? 0: model.member.GetLoyaltyPointAccumulated();
				model.redeemedPoint = 0;
				model.redeemAmount = 0.00;
			}
		}
	}
	
	//will auto call this method to update inventory quantity
	private void updateInventoryQty() throws ProductCodeInvalidException, ValidationException{
		if(model.list != null){
			for(Transaction t : model.list){
				Product p = t.getProduct();
				
				if(p != null){
					Integer qtyToUpdate = p.getAvailableQty() - t.getPurchasedQuantity();
					
					productService.edit(p.getProductID(), p.getProductName(), 
							p.getDescription(), qtyToUpdate.toString(), 
							p.getPrice().toString(), p.getBarCodeNo(), 
							p.getReOrderQty().toString(), p.getOrderQty().toString());
				}
			}
		}
	}
	
	@Override
	public boolean isValidateCheckOut() throws ValidationException {
		if(model.list.size() == 0){
			throw new ValidationException("Checkout Screen", "No item(s) purchased!");
		}
		if(model.list.size() > 0){
			for(Transaction t : model.list){
				if(t.getPurchasedQuantity() <= 0){
					throw new ValidationException("Checkout Screen", "Zero purchased quantity!");
				}
			}
		}
		if(model.balance == 0){
			throw new ValidationException("Checkout Screen", "Zero balance!");
		}
		if(model.tenderedAmount == 0){
			throw new ValidationException("Checkout Screen", "Zero tendered amount!");
		}
		if(model.change < 0){
			throw new ValidationException("Checkout Screen", "Negative change!");
		}
		
		return true;		
	}
	
	
}
