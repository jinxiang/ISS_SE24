/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

import sg.edu.nus.iss.se24.pt1.dao.StorekeeperDAO;
import sg.edu.nus.iss.se24.pt1.dao.StorekeeperDAOImpl;
import sg.edu.nus.iss.se24.pt1.model.Storekeeper;

/**
 * @author jinxiang
 *
 */
public class UserService implements UserServiceI {
	private StorekeeperDAO dao;
	
	public UserService() {
		dao = StorekeeperDAOImpl.getInstance();
	}

	/* (non-Javadoc)
	 * @see sg.edu.nus.iss.se24.pt1.service.UserServiceI#authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String username, String password) {
		if (username != null && password != null) {
			Storekeeper user = dao.findByName(username.trim());
			if (user != null && user.getStoreKeeperPassword().equals(password.trim()))
				return true;
		}
		return false;
	}

}
