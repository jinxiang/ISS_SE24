/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.text.ParseException;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.dao.Transformer;
import sg.edu.nus.iss.se24.pt1.model.Transaction;

/**
 * @author jinxiang
 *
 */
public class TransactionReportTable extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6744581991540751381L;

	private List<Transaction> list;

	private static final String[] columnNames = new String[] { "Transaction ID", "Member ID", "Product ID",
			"Product Name", "Product Description", "Quantity", "Date" };
	
	@SuppressWarnings("rawtypes")
	private static final Class[] columnClass = new Class[] { Integer.class, String.class, String.class,
			String.class, String.class, Integer.class, String.class};

	/**
	 * @param list
	 */
	public TransactionReportTable(List<Transaction> list) {
		this.list = list;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Transaction transaction = list.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return transaction.getTransactionID();
		case 1:
			return transaction.getMemberID();
			
		case 2:
			return transaction.getProductID();
			
		case 3:
			return transaction.getProduct().getProductName();
		
		case 4:
			return transaction.getProduct().getDescription();
		
		case 5:
			return transaction.getPurchasedQuantity();
			
		case 6:
			try {
				return Transformer.dateToString(transaction.getTransactionDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		default:
			break;
		}
		return null;
	}

}
