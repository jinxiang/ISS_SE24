package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Product;

/**
 * created by Nilar
 * 
 * @author jinxiang
 *
 */
public class ProductTable extends AbstractTableModel {
	private final List<Product> productList;

	public ProductTable(List<Product> productList) {
		this.productList = productList;
	}

	private final String[] columnNames = new String[] { "Product ID", "Product Name", "Description",
			"Quantity Available", "Price", "Bar Code No", "Threshold", "Order Quantity" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class, String.class, Integer.class, Double.class,
			String.class, Integer.class, Integer.class };

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return productList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Product row = productList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getProductID();
		} else if (1 == columnIndex) {
			return row.getProductName();
		} else if (2 == columnIndex) {
			return row.getDescription();
		} else if (3 == columnIndex) {
			return row.getAvailableQty();
		} else if (4 == columnIndex) {
			return row.getPrice();
		} else if (5 == columnIndex) {
			return row.getBarCodeNo();
		} else if (6 == columnIndex) {
			return row.getReOrderQty();
		} else if (7 == columnIndex) {
			return row.getOrderQty();
		}
		return null;
	}

	/**
	 * @return the productList
	 */
	public List<Product> getList() {
		return productList;
	}
}
