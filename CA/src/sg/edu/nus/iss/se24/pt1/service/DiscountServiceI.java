/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

import java.text.ParseException;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Discount;

/**
 * @author Myo Wai Yan Kyaw
 *
 */
public interface DiscountServiceI {

	public List<Discount> getAll();

	public Discount getByDiscountID(String discountID);

	public boolean add(String discountCode, String description, String startDate, String periodOfDiscount,
			String percentageDiscount, char memberFlg) throws ValidationException;

	public boolean edit(String discountCode, String description, String startDate, String periodOfDiscount,
			String percentageDiscount, char memberFlg) throws ValidationException;

	public boolean remove(String discountID);

	public int getDiscountPercent(String memberID);
	
	public void validateDiscount(String discountCode, String description, String startDate, String periodOfDiscount,
			String strPercentageDiscount, char strMemberFlg, boolean editMode) throws ValidationException;
	
	public boolean isTodayValidDiscountDay(Discount d) throws ParseException;
	
	//public Member findByID(String memberID);
}
