package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * @author jinxiang
 *
 */
public class AdminstrationDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private StoreFrame parent;

	/**
	 * Create the dialog.
	 */
	public AdminstrationDialog() {
		this(null, null);
	}

	public AdminstrationDialog(final StoreFrame frame, String title) {
		super(frame, title, true);
		parent = frame;
		setResizable(false);
		setPreferredSize(Constants.APP_DIMENSION);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new GridLayout(1, 1));
		getContentPane().add(contentPanel);
		{
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			contentPanel.add(tabbedPane);
			buildMemberTab(tabbedPane);
			buildProductTab(tabbedPane);
			buildCategoryTab(tabbedPane);
			buildVendorTab(tabbedPane);
			buildDiscountTab(tabbedPane);
		}
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeAdminScreen();
			}
		});
		pack();
	}

	private void buildMemberTab(JTabbedPane tabbedPane) {
		JPanel productPanel = new MemberPanel(this);
		tabbedPane.addTab(Constants.STRING_LABEL_TAB_MEMBER, null, productPanel, null);
	}

	private void buildProductTab(JTabbedPane tabbedPane) {
		JPanel productPanel = new ProductPanel(this);
		tabbedPane.addTab(Constants.STRING_LABEL_TAB_PRODUCT, null, productPanel, null);
	}

	private void buildDiscountTab(JTabbedPane tabbedPane) {
		JPanel discountPanel = new DiscountPanel(this);
		tabbedPane.addTab(Constants.STRING_LABEL_TAB_DISCOUNT, null, discountPanel, null);
	}

	private void buildCategoryTab(JTabbedPane tabbedPane) {
		JPanel categoryPanel = new CategoryPanel(this);
		tabbedPane.addTab(Constants.STRING_LABEL_TAB_CATEGORY, null, categoryPanel, null);
	}

	private void buildVendorTab(JTabbedPane tabbedPane) {
		JPanel vendorPanel = new VendorPanel(this);
		tabbedPane.addTab(Constants.STRING_LABEL_TAB_VENDOR, null, vendorPanel, null);
	}

	public void close() {
		parent.closeAdminScreen();
	}

}
