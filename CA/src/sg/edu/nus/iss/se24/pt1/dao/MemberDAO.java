package sg.edu.nus.iss.se24.pt1.dao;

import java.util.List;

import sg.edu.nus.iss.se24.pt1.model.Member;

//Created by Nguwar

public interface MemberDAO {
	List<Member> findAll();
	Member findByID(String memberID);
    boolean insertMember(Member memberToInsert);
    boolean updateMember(Member memberToUpdate);
    boolean deleteMember(String memberID);
    void commit();
}
