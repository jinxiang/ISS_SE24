package sg.edu.nus.iss.se24.pt1.dao;

import java.util.Date;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.model.Transaction;

//Created by Nguwar

public interface TransactionDAO {
	List<Transaction> findAll();
	List<Transaction> findByID(Integer transactionID);
	Transaction findByID(Integer transactionID,String productID);
	List<Transaction> findByDate(Date filterDate);
	List<Transaction> findByProduct(String productID);
    boolean insertTransaction(List<Transaction> transactionsToInsert);
	void commit();
}
