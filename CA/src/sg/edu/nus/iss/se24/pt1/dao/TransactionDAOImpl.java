package sg.edu.nus.iss.se24.pt1.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.model.Transaction;

//Created by Nguwar

public class TransactionDAOImpl extends GenericFileDAO<Transaction> implements TransactionDAO {
	private static TransactionDAOImpl instance = null;
	private ArrayList<Transaction> data;

	public static TransactionDAOImpl getInstance(){
		if(instance == null){
			synchronized (TransactionDAOImpl.class){
				if(instance == null){
					instance = new TransactionDAOImpl();
					return instance;
				}
			}
		}
		return instance;
	}
	private TransactionDAOImpl() {
		super(Transaction.class);
		data = load();
	}

	@Override
	public List<Transaction> findAll() {
		return data;
	}

	@Override
	public List<Transaction> findByID(Integer transactionID) {
		ArrayList<Transaction> findArrayList = new ArrayList<Transaction>();
        for(Transaction transaction:data){
            if(transaction.getTransactionID()==transactionID){
                findArrayList.add(transaction);
            }
        }
		return findArrayList;
	}
	
	@Override
	public Transaction findByID(Integer transactionID,String productID) {
        for(Transaction transaction:data){
            if(transaction.getTransactionID()==transactionID && transaction.getProductID().equalsIgnoreCase(productID)){
                return transaction;
            }
        }
		return null;
	}

	@Override
	public List<Transaction> findByDate(Date filterDate) {
		List<Transaction> findArrayList = new ArrayList<Transaction>();
        for(Transaction transaction:data){
            if(transaction.getTransactionDate().equals(filterDate)){
                findArrayList.add(transaction);
            }
        }
		return findArrayList;
	}

	@Override
	public List<Transaction> findByProduct(String productID) {
		ArrayList<Transaction> findArrayList = new ArrayList<Transaction>();
        for(Transaction transaction:data){
            if(transaction.getProductID().equalsIgnoreCase(productID)){
                findArrayList.add(transaction);
            }
        }
		return findArrayList;
	}
	
	@Override
	public synchronized boolean insertTransaction(List<Transaction> transactionsToInsert) {
        Boolean toInsert=true;
        if(transactionsToInsert != null){
	        int transactionID = transactionsToInsert.get(0).getTransactionID();
	        for(Transaction transaction:data){
	            if(transaction.getTransactionID() == transactionID){
	                toInsert = false;
	                break;
	            }
	        }
	        if(toInsert) {
	        	for(Transaction transaction:transactionsToInsert){
	        		data.add(transaction);
	        	}
	        }
        }
        return toInsert;
	}

	@Override
	public synchronized void commit() {
		this.commit(data);
	}
}
