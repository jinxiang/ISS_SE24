package sg.edu.nus.iss.se24.pt1.service;

import java.util.*;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.model.Transaction;

//Created by Nguwar

public interface CheckOutServiceI {
	
	//Call this method after got member ID on UI
	public void updateMemberID(String MemberID);
	
	//Call this method when add item on UI
	public Transaction addLine(String productCodeID, Integer quantity);
	
	//Call this method when edit item on UI
	public Transaction editLine(Transaction transactionToEdit, Integer quantitytoEdit);
	
	//Call this method when delete item on UI
	public boolean deleteLine(Transaction transactionToDelete);
	
	//Call this method when click pay now button on UI
	public boolean makePayment();
	
	//Call this method when chose to print receipt on UI
	public void printReceipt();
	
	//Call this method when click redeem point on UI
	public void redeemPoint();
	
	public Double getTotalAmount();
	
	public Double getDiscountAmount();
	
	public Double getRedeemAmount();
	
	public Integer getRedeemedPoint();
	
	public Integer getAvailablePointToRedeem();
	
	public List<Transaction> getLines();
	
	public Member getMember();
	
	public Integer getDiscountPercentage();
	
	public Double getTenderedAmount();
	
	public Double getBalance();
	
	public Double getChange();
	
	public Integer getNewTransactionID();
	
	public void setTenderedAmount(Double tenderedAmount);
	
	public boolean hasBelowThresholdLevel();
	
	public boolean isProductAlreadyAdded(String productBarCode);

	public boolean isValidateCheckOut() throws ValidationException;
}
