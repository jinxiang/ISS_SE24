/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.Dimension;

/**
 * @author jinxiang
 *
 */
public final class Constants {
	public static Dimension APP_DIMENSION = new Dimension(800, 600);

	public static String BUTTON_ADD_LABEL = "Add";
	public static String BUTTON_EDIT_LABEL = "Edit";
//	public static String BUTTON_DELETE_LABEL = "Delete";
	public static String BUTTON_OK_LABEL = "OK";
	public static String BUTTON_SAVE_LABEL = "Save";
	public static String BUTTON_RESET_LABEL = "Reset";
	public static String BUTTON_CLEAR_LABEL = "Clear";
	public static String BUTTON_CLOSE_LABEL = "Close";
	public static String BUTTON_CANCEL_LABEL = "Cancel";
	public static String BUTTON_LOGIN_LABEL = "Login";

	// Main Screen
	public static String STRING_TITLE_APPLICATION = "University Sourvenir Store Application";
	public static String STRING_TITLE_LOGIN_SCREEN = "User Login";
	public static String STRING_TITLE_LOGOFF = "Log Off";
	public static String STRING_MESSAGE_LOGOFF = "Good Bye";
	public static String STRING_TITLE_USER_INFO = "Welcome %s";

	// Login Screen
	public static String STRING_LABEL_USERNAME = "Username";
	public static String STRING_LABEL_PASSWORD = "Password";
	public static String MESSAGE_AUTHENTICATED_FAILED = "Invalid username or password";
	
	// Checkout Screen
	public static String STRING_TITLE_CHECKOUT_SCREEN = "Checkout Screen";

	// Inventory Screen
	public static String STRING_TITLE_INVENTORY_SCREEN = "Inventory List";
	public static String STRING_LABEL_PO = "Generate Purchase Order";

	// Reporting Screen
	public static String STRING_TITLE_REPORTING_SCREEN = "Reporting";
	public static String STRING_LABEL_LIST_MEMBERS = "Members List";
	public static String STRING_LABEL_LIST_CATEGORY = "Categories List";
	public static String STRING_LABEL_LIST_TRANSACTION = "Transactions List";
	public static String STRING_LABEL_LIST_PRODUCT = "Products List";

	// Administration Screen
	public static String STRING_TITLE_ADMINISTEATION_SETUP_SCREEN = "Administration Setup";
	public static String STRING_LABEL_TAB_MEMBER = "Member";
	public static String STRING_LABEL_MEMBER_ID = "Member ID:";
	public static String STRING_LABEL_NAME = "Member Name:";
	public static String STRING_LABEL_POINT = "Loyalty Point:";
	public static String STRING_MESSAGE_DELETE_MEMEBER = "Would you like to delete this member?";
	public static String STRING_MESSAGE_NEW_MEMBER="Since this customer is new member, system will automatically convert the point into default point";
	public static String STRING_TITLE_NEW_MEMBER="Notification for new member";
	
	public static String STRING_LABEL_TAB_PRODUCT = "Product";
	public static String STRING_LABEL_CATEGORY = "Category";
	public static String STRING_LABEL_PRODUCT_ID = "Product ID";
	public static String STRING_LABEL_PRODUCT_NAME = "Product Name:";
	public static String STRING_LABEL_DESCRIPTION = "Description";
	public static String STRING_LABEL_QUANTITY_AVAILABLE = "Quantity Available";
	public static String STRING_LABEL_PRICE = "Price";
	public static String STRING_LABEL_BAR_CODE = "Bar Code Number";
	public static String STRING_LABEL_REORDER_QUANTYTY = "Reorder Quantity";
	public static String STRING_LABEL_ORDER_QUANTITY = "Order Quantity";
	public static String STRING_LABEL_THRESHOLD = "Threshold";
	public static String STRING_MESSAGE_ANOTHER_PRODUCT = "Do you want to enter information about another product?";
	public static String STRING_TITLE_ANOTHER_PRODUCT = "Confirmation";
	public static String STRING_MESSAGE_DELETE_PRODUCT = "Would you like to delete this product?";
	
	public static String STRING_LABEL_TAB_CATEGORY = "Category";
	public static String STRING_LABEL_CATEGORY_CODE = "Category Code"; 
	public static String STRING_LABEL_CATEGORY_NAME = "Category Name";
	public static String STRING_INSERT_ERROR_Title="Cannot insert Categor";
	public static String STRING_MESSAGE_DELETE_CATEGORY = "All the products related to this category will be deleted. \n Would you like to delete this category? ";
	
	public static String STRING_LABEL_TAB_VENDOR = "Vendor";
	public static String STRING_LABEL_VENDOR_NAME = "Vendor Name";
	public static String STRING_LABEL_VENDOR_DESCRIPTION = "Description";
	
	public static String STRING_LABEL_TAB_DISCOUNT = "Discount";
	public static String STRING_LABEL_DISCOUNT_CODE = "Discount Code";
	public static String STRING_LABEL_DISCOUNT_DESCRIPTION = "Description";
	public static String STRING_LABEL_DISCOUNT_START_DATE = "Start Date";
	public static String STRING_LABEL_DISCOUNT_PERIOD = "Period of Discount";
	public static String STRING_LABEL_DISCOUNT_PERCENT = "Percent of Discount";
	public static String STRING_LABEL_DISCOUNT_MEMBER_ALL = "Member or All";
	public static String STRING_LABEL_DISCOUNT_MEMBER = "Member";
	public static String STRING_LABEL_DISCOUNT_ALL = "All Customers";
	

}
