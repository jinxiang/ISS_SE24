package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.service.MemberService;
import sg.edu.nus.iss.se24.pt1.service.MemberServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.MemberListTable;

/**
 * @author Nguwar
 * @author jinxiang
 *
 */
public class MemberPanel extends JPanel {
	private AdminstrationDialog parentDialog;
	private MemberServiceI memberService = new MemberService();
	private JTextField txtMemberID;
	private JTextField txtMemberName;
	private JTextField txtLoyaltyPoint;
	private JTable table;
	private JButton btnAdd;

	public MemberPanel() {
		this(null);
	}

	public MemberPanel(AdminstrationDialog dialog) {
		parentDialog = dialog;
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(Constants.APP_DIMENSION);
		// Content Panel
		JPanel ContentPanel = new JPanel();
		add(ContentPanel, BorderLayout.CENTER);
		ContentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		JPanel ListPanel = new JPanel();
		ContentPanel.add(ListPanel);
		ListPanel.setLayout(new BorderLayout(0, 0));
		MemberListTable tableModel = new MemberListTable(listMember());
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		ListPanel.add(scrollPane, BorderLayout.CENTER);

		JPanel DetailPanel = new JPanel();
		ContentPanel.add(DetailPanel);
		GridBagLayout gbl_DetailPanel = new GridBagLayout();
		gbl_DetailPanel.columnWidths = new int[] { 123, 240, 0 };
		gbl_DetailPanel.rowHeights = new int[] { 33, 33, 33, 0 };
		gbl_DetailPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_DetailPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		DetailPanel.setLayout(gbl_DetailPanel);
		JLabel lblMemberID = new JLabel(Constants.STRING_LABEL_MEMBER_ID);
		GridBagConstraints gbc_lblMemberID = new GridBagConstraints();
		gbc_lblMemberID.fill = GridBagConstraints.BOTH;
		gbc_lblMemberID.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberID.gridx = 0;
		gbc_lblMemberID.gridy = 0;
		DetailPanel.add(lblMemberID, gbc_lblMemberID);

		txtMemberID = new JTextField();
		GridBagConstraints gbc_txtMemberID = new GridBagConstraints();
		gbc_txtMemberID.fill = GridBagConstraints.BOTH;
		gbc_txtMemberID.insets = new Insets(0, 0, 5, 0);
		gbc_txtMemberID.gridx = 1;
		gbc_txtMemberID.gridy = 0;
		DetailPanel.add(txtMemberID, gbc_txtMemberID);
		txtMemberID.setColumns(10);

		JLabel lblMemberName = new JLabel(Constants.STRING_LABEL_NAME);
		GridBagConstraints gbc_lblMemberName = new GridBagConstraints();
		gbc_lblMemberName.fill = GridBagConstraints.BOTH;
		gbc_lblMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberName.gridx = 0;
		gbc_lblMemberName.gridy = 1;
		DetailPanel.add(lblMemberName, gbc_lblMemberName);

		txtMemberName = new JTextField();
		GridBagConstraints gbc_txtMemberName = new GridBagConstraints();
		gbc_txtMemberName.fill = GridBagConstraints.BOTH;
		gbc_txtMemberName.insets = new Insets(0, 0, 5, 0);
		gbc_txtMemberName.gridx = 1;
		gbc_txtMemberName.gridy = 1;
		DetailPanel.add(txtMemberName, gbc_txtMemberName);
		txtMemberName.setColumns(10);
		JLabel lblLoyaltyPoint = new JLabel(Constants.STRING_LABEL_POINT);
		GridBagConstraints gbc_lblLoyaltyPoint = new GridBagConstraints();
		gbc_lblLoyaltyPoint.fill = GridBagConstraints.BOTH;
		gbc_lblLoyaltyPoint.insets = new Insets(0, 0, 0, 5);
		gbc_lblLoyaltyPoint.gridx = 0;
		gbc_lblLoyaltyPoint.gridy = 2;
		DetailPanel.add(lblLoyaltyPoint, gbc_lblLoyaltyPoint);
		txtLoyaltyPoint = new JTextField();
		GridBagConstraints gbc_txtLoyaltyPoint = new GridBagConstraints();
		gbc_txtLoyaltyPoint.fill = GridBagConstraints.BOTH;
		gbc_txtLoyaltyPoint.gridx = 1;
		gbc_txtLoyaltyPoint.gridy = 2;
		DetailPanel.add(txtLoyaltyPoint, gbc_txtLoyaltyPoint);
		txtLoyaltyPoint.setColumns(10);

		// Button Panel
		JPanel ButtonPanel = new JPanel();
		add(ButtonPanel, BorderLayout.SOUTH);
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		btnAdd = new JButton(Constants.BUTTON_ADD_LABEL);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addMember(tableModel);
				//clearMember();
			}
		});
		ButtonPanel.add(btnAdd);
		JButton btnEdit = new JButton(Constants.BUTTON_EDIT_LABEL);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateMember(tableModel);
			}
		});
		ButtonPanel.add(btnEdit);

		// JButton btnRemove = new JButton(Constants.BUTTON_DELETE_LABEL);
		// btnRemove.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// deleteMember(tableModel);
		// }
		// });
		// ButtonPanel.add(btnRemove);

		JButton btnClear = new JButton(Constants.BUTTON_CLEAR_LABEL);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearMember();
			}
		});
		ButtonPanel.add(btnClear);

		JButton btnClose = new JButton(Constants.BUTTON_CLOSE_LABEL);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentDialog.close();
			}
		});
		ButtonPanel.add(btnClose);

		ListSelectionModel selectionModel = table.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectRow = table.getSelectedRow();
				if (selectRow != -1) {
					txtMemberID.setText(tableModel.getList().get(selectRow).getMemberID());
					txtMemberName.setText(tableModel.getList().get(selectRow).getMemberName());
					txtLoyaltyPoint
							.setText(tableModel.getList().get(selectRow).GetLoyaltyPointAccumulated().toString());
					txtMemberID.setEnabled(false);
					btnAdd.setEnabled(false);
				} else {
					clearMember();
				}
			}

		});
	}

	private List<Member> listMember() {
		return memberService.getAll();
	}

	private void addMember(MemberListTable tableModel) {
		//if (isValidate(true)) {
			try {
				//before adding, need to get alert box
				if(txtLoyaltyPoint.getText().trim().length()>0 && !(txtLoyaltyPoint.getText().equals("-1")))
				{
					JOptionPane.showMessageDialog(this, Constants.STRING_MESSAGE_NEW_MEMBER,
							Constants.STRING_TITLE_NEW_MEMBER, JOptionPane.INFORMATION_MESSAGE);
				}
				
				memberService.add(this.txtMemberID.getText().trim(), this.txtMemberName.getText().trim(),
						this.txtLoyaltyPoint.getText().trim());
				refreshMemberList();
				clearMember();
			} catch (ValidationException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
			}
			// tableModel.addRow(table.getSelectedRow(), member);
			
		//}
	}

	private void updateMember(MemberListTable tableModel) {
		//if (isValidate(false)) {
			try {
				memberService.edit(this.txtMemberID.getText().trim(), this.txtMemberName.getText().trim(),
						this.txtLoyaltyPoint.getText().trim());
				refreshMemberList();
			} catch (ValidationException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
			}
			// tableModel.updateRow(table.getSelectedRow(), member);
			
		//}
	}

	private void clearMember() {
		txtMemberID.setText(null);
		txtMemberName.setText(null);
		txtLoyaltyPoint.setText(null);
		txtMemberID.requestFocus(true);
		txtMemberID.setEnabled(true);
		btnAdd.setEnabled(true);
	}

	private void deleteMember(MemberListTable tableModel) {
		int dialogResult = JOptionPane.showConfirmDialog(null, Constants.STRING_MESSAGE_DELETE_MEMEBER);
		if (dialogResult == JOptionPane.YES_OPTION) {
			// tableModel.deleteRow(table.getSelectedRow());
			memberService.delete(txtMemberID.getText());
		}
	}

	private void refreshMemberList() {
		((MemberListTable) table.getModel()).fireTableDataChanged();
	}

	/*
	private Boolean isValidate(boolean isNewMember) {
		if (isNewMember && memberService.isDuplicate(this.txtMemberID.getText().trim())) {
			return false;
		}
		if (txtMemberID.getText() == null || txtMemberID.getText().trim().equals("")) {
			return false;
		}
		if (txtMemberName.getText() == null || txtMemberName.getText().trim().equals("")) {
			return false;
		}
		if (txtLoyaltyPoint.getText() == null || txtLoyaltyPoint.getText().trim().equals("")) {
			return false;
		} else {
			try {
				Integer.parseInt(txtLoyaltyPoint.getText());
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}
	*/

}
