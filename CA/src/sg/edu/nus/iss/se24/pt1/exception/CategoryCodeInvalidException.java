package sg.edu.nus.iss.se24.pt1.exception;
/**
 * @author Nilar
 *
 */

public class CategoryCodeInvalidException extends Exception{
	private static final String ERROR_MESSAGE ="Invalid category code.Please enter 3 non-numeric character for category.";
    public CategoryCodeInvalidException() {
        super(ERROR_MESSAGE);
    }
}
