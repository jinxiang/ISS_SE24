package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sg.edu.nus.iss.se24.pt1.exception.ProductCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.service.CategoryService;
import sg.edu.nus.iss.se24.pt1.service.CategoryServiceI;
import sg.edu.nus.iss.se24.pt1.service.ProductService;
import sg.edu.nus.iss.se24.pt1.service.ProductServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.ProductTable;

/**
 * created by
 * 
 * @author Nilar
 * @author jinxiang
 *
 */

public class ProductPanel extends JPanel {
	private ProductServiceI productService = new ProductService();
	private CategoryServiceI categoryService = new CategoryService();
	private AdminstrationDialog parentDialog;
	private JTextField txtProductID;
	private JTextField txtProductName;
	private JTextField txtDescription;
	private JTextField txtQuantityAvailable;
	private JTextField txtPrice;
	private JTextField txtBarCodeNo;
	private JTextField txtThreshold;
	private JTextField txtOrderQty;
	private JTable table;
	private JComboBox<String> cboCategory;
	private JComboBox<String> cboFilterCategory;
	private boolean generateProdutID=true;
	private JButton btnAdd;

	public ProductPanel() {
		this(null);
	}

	/**
	 * Create the panel.
	 * 
	 * @param adminstrationDialog
	 */
	public ProductPanel(AdminstrationDialog adminstrationDialog) {
		this.parentDialog = adminstrationDialog;
		setPreferredSize(Constants.APP_DIMENSION);
		setLayout(new BorderLayout(0, 0));

		JPanel titlePanel = new JPanel();
		add(titlePanel, BorderLayout.NORTH);
		titlePanel.setLayout(new GridLayout(1, 1, 0, 0));
		{
			JPanel FilterPanel = new JPanel();
			FlowLayout fl_FilterPanel = (FlowLayout) FilterPanel.getLayout();
			fl_FilterPanel.setAlignment(FlowLayout.LEFT);
			titlePanel.add(FilterPanel);
			{
				JLabel lblCategory1 = new JLabel(Constants.STRING_LABEL_TAB_CATEGORY);
				FilterPanel.add(lblCategory1);
			}
			{
				cboFilterCategory = new JComboBox(categoryService.listCategoryCodeWithDefault());
				cboFilterCategory.addItemListener(new ItemListener() {

					@Override
					public void itemStateChanged(ItemEvent e) {
						generateSelectedProduct(e.getItem().toString());
						cboCategory.setSelectedItem(cboFilterCategory.getSelectedItem().toString());
					}
				});
				FilterPanel.add(cboFilterCategory);
			}
		}
		

		JPanel contentPanel = new JPanel();
		add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(1, 2, 0, 0));

		JPanel panel_3 = new JPanel();
		contentPanel.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		ProductTable tableModel = new ProductTable(listProduct());
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);

		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel_3.add(scrollPane, BorderLayout.CENTER);

		JPanel panel_4 = new JPanel();
		contentPanel.add(panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 135, 243, 0 };
		gbl_panel_4.rowHeights = new int[] {33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33};
		gbl_panel_4.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		JLabel lblCategory = new JLabel(Constants.STRING_LABEL_CATEGORY);
		GridBagConstraints gbc_lblCategory = new GridBagConstraints();
		gbc_lblCategory.fill = GridBagConstraints.BOTH;
		gbc_lblCategory.insets = new Insets(0, 0, 5, 5);
		gbc_lblCategory.gridx = 0;
		gbc_lblCategory.gridy = 0;
		panel_4.add(lblCategory, gbc_lblCategory);

		cboCategory = new JComboBox<String>(listCategoryCode());
		GridBagConstraints gbc_cboCategory = new GridBagConstraints();
		gbc_cboCategory.fill = GridBagConstraints.BOTH;
		gbc_cboCategory.insets = new Insets(0, 0, 5, 0);
		gbc_cboCategory.gridx = 1;
		gbc_cboCategory.gridy = 0;
		panel_4.add(cboCategory, gbc_cboCategory);
		cboCategory.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(generateProdutID){
					txtProductID.setText(getProductID());
					
				}
				
			}
		});
		
		

		JLabel lblProductid = new JLabel(Constants.STRING_LABEL_PRODUCT_ID);
		GridBagConstraints gbc_lblProductid = new GridBagConstraints();
		gbc_lblProductid.fill = GridBagConstraints.BOTH;
		gbc_lblProductid.insets = new Insets(0, 0, 5, 5);
		gbc_lblProductid.gridx = 0;
		gbc_lblProductid.gridy = 1;
		panel_4.add(lblProductid, gbc_lblProductid);

		txtProductID = new JTextField();
		txtProductID.setEnabled(false);
		GridBagConstraints gbc_txtProductID = new GridBagConstraints();
		gbc_txtProductID.fill = GridBagConstraints.BOTH;
		gbc_txtProductID.insets = new Insets(0, 0, 5, 0);
		gbc_txtProductID.gridx = 1;
		gbc_txtProductID.gridy = 1;
		panel_4.add(txtProductID, gbc_txtProductID);
		txtProductID.setColumns(10);

		JLabel lblProductname = new JLabel(Constants.STRING_LABEL_PRODUCT_NAME);
		GridBagConstraints gbc_lblProductname = new GridBagConstraints();
		gbc_lblProductname.fill = GridBagConstraints.BOTH;
		gbc_lblProductname.insets = new Insets(0, 0, 5, 5);
		gbc_lblProductname.gridx = 0;
		gbc_lblProductname.gridy = 2;
		panel_4.add(lblProductname, gbc_lblProductname);

		txtProductName = new JTextField();
		GridBagConstraints gbc_txtProductName = new GridBagConstraints();
		gbc_txtProductName.fill = GridBagConstraints.BOTH;
		gbc_txtProductName.insets = new Insets(0, 0, 5, 0);
		gbc_txtProductName.gridx = 1;
		gbc_txtProductName.gridy = 2;
		panel_4.add(txtProductName, gbc_txtProductName);
		txtProductName.setColumns(10);

		JLabel lblDescription = new JLabel(Constants.STRING_LABEL_DESCRIPTION);
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.fill = GridBagConstraints.BOTH;
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 0;
		gbc_lblDescription.gridy = 3;
		panel_4.add(lblDescription, gbc_lblDescription);

		txtDescription = new JTextField();
		GridBagConstraints gbc_txtDescription = new GridBagConstraints();
		gbc_txtDescription.fill = GridBagConstraints.BOTH;
		gbc_txtDescription.insets = new Insets(0, 0, 5, 0);
		gbc_txtDescription.gridx = 1;
		gbc_txtDescription.gridy = 3;
		panel_4.add(txtDescription, gbc_txtDescription);
		txtDescription.setColumns(10);

		JLabel lblQuantityAvailable = new JLabel(Constants.STRING_LABEL_QUANTITY_AVAILABLE);
		GridBagConstraints gbc_lblQuantityAvailable = new GridBagConstraints();
		gbc_lblQuantityAvailable.fill = GridBagConstraints.BOTH;
		gbc_lblQuantityAvailable.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantityAvailable.gridx = 0;
		gbc_lblQuantityAvailable.gridy = 4;
		panel_4.add(lblQuantityAvailable, gbc_lblQuantityAvailable);

		txtQuantityAvailable = new JTextField();
		GridBagConstraints gbc_txtQuantityAvailable = new GridBagConstraints();
		gbc_txtQuantityAvailable.fill = GridBagConstraints.BOTH;
		gbc_txtQuantityAvailable.insets = new Insets(0, 0, 5, 0);
		gbc_txtQuantityAvailable.gridx = 1;
		gbc_txtQuantityAvailable.gridy = 4;
		panel_4.add(txtQuantityAvailable, gbc_txtQuantityAvailable);
		txtQuantityAvailable.setColumns(10);

		JLabel lblPrice = new JLabel(Constants.STRING_LABEL_PRICE);
		GridBagConstraints gbc_lblPrice = new GridBagConstraints();
		gbc_lblPrice.fill = GridBagConstraints.BOTH;
		gbc_lblPrice.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrice.gridx = 0;
		gbc_lblPrice.gridy = 5;
		panel_4.add(lblPrice, gbc_lblPrice);

		txtPrice = new JTextField();
		GridBagConstraints gbc_txtPrice = new GridBagConstraints();
		gbc_txtPrice.fill = GridBagConstraints.BOTH;
		gbc_txtPrice.insets = new Insets(0, 0, 5, 0);
		gbc_txtPrice.gridx = 1;
		gbc_txtPrice.gridy = 5;
		panel_4.add(txtPrice, gbc_txtPrice);
		txtPrice.setColumns(10);

		JLabel lblBarCodeNumber = new JLabel(Constants.STRING_LABEL_BAR_CODE);
		GridBagConstraints gbc_lblBarCodeNumber = new GridBagConstraints();
		gbc_lblBarCodeNumber.fill = GridBagConstraints.BOTH;
		gbc_lblBarCodeNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblBarCodeNumber.gridx = 0;
		gbc_lblBarCodeNumber.gridy = 6;
		panel_4.add(lblBarCodeNumber, gbc_lblBarCodeNumber);

		txtBarCodeNo = new JTextField();
		GridBagConstraints gbc_txtBarCodeNo = new GridBagConstraints();
		gbc_txtBarCodeNo.fill = GridBagConstraints.BOTH;
		gbc_txtBarCodeNo.insets = new Insets(0, 0, 5, 0);
		gbc_txtBarCodeNo.gridx = 1;
		gbc_txtBarCodeNo.gridy = 6;
		panel_4.add(txtBarCodeNo, gbc_txtBarCodeNo);
		txtBarCodeNo.setColumns(10);

		JLabel lblThreshold = new JLabel(Constants.STRING_LABEL_THRESHOLD);
		GridBagConstraints gbc_lblThreshold = new GridBagConstraints();
		gbc_lblThreshold.fill = GridBagConstraints.BOTH;
		gbc_lblThreshold.insets = new Insets(0, 0, 5, 5);
		gbc_lblThreshold.gridx = 0;
		gbc_lblThreshold.gridy = 7;
		panel_4.add(lblThreshold, gbc_lblThreshold);

		txtThreshold = new JTextField();
		GridBagConstraints gbc_txtThreshold = new GridBagConstraints();
		gbc_txtThreshold.fill = GridBagConstraints.BOTH;
		gbc_txtThreshold.insets = new Insets(0, 0, 5, 0);
		gbc_txtThreshold.gridx = 1;
		gbc_txtThreshold.gridy = 7;
		panel_4.add(txtThreshold, gbc_txtThreshold);
		txtThreshold.setColumns(10);

		JLabel lblOrderQuantity = new JLabel(Constants.STRING_LABEL_ORDER_QUANTITY);
		GridBagConstraints gbc_lblOrderQuantity = new GridBagConstraints();
		gbc_lblOrderQuantity.fill = GridBagConstraints.BOTH;
		gbc_lblOrderQuantity.insets = new Insets(0, 0, 0, 5);
		gbc_lblOrderQuantity.gridx = 0;
		gbc_lblOrderQuantity.gridy = 8;
		panel_4.add(lblOrderQuantity, gbc_lblOrderQuantity);

		txtOrderQty = new JTextField();
		GridBagConstraints gbc_txtOrderQty = new GridBagConstraints();
		gbc_txtOrderQty.fill = GridBagConstraints.BOTH;
		gbc_txtOrderQty.gridx = 1;
		gbc_txtOrderQty.gridy = 8;
		panel_4.add(txtOrderQty, gbc_txtOrderQty);
		txtOrderQty.setColumns(10);

		JPanel buttonPanel = new JPanel();
		add(buttonPanel, BorderLayout.SOUTH);

		btnAdd = new JButton(Constants.BUTTON_ADD_LABEL);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				add();
			}
		});
		buttonPanel.add(btnAdd);

		JButton btnEdit = new JButton(Constants.BUTTON_EDIT_LABEL);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				edit();
			}
		});
		buttonPanel.add(btnEdit);

		// JButton btnDelete = new JButton(Constants.BUTTON_DELETE_LABEL);
		// btnDelete.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// int dialogResult = JOptionPane.showConfirmDialog(null,
		// Constants.STRING_MESSAGE_DELETE_PRODUCT);
		// if(dialogResult == JOptionPane.YES_OPTION){
		// delete(tableModel);
		// }
		//
		// }
		// });
		// buttonPanel.add(btnDelete);

		JButton btnClear = new JButton(Constants.BUTTON_CLEAR_LABEL);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearProduct();
			}
		});
		buttonPanel.add(btnClear);

		JButton btnClose = new JButton(Constants.BUTTON_CLOSE_LABEL);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentDialog.close();
			}
		});
		buttonPanel.add(btnClose);

		ListSelectionModel selectionModel = table.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectRow = table.getSelectedRow();
				if (selectRow != -1) {
					setProduct(((ProductTable) table.getModel()).getList().get(selectRow));
					btnAdd.setEnabled(false);
					
					/*
					txtProductID.setText(tableModel.getList().get(selectRow).getProductID());
					txtProductName.setText(tableModel.getList().get(selectRow).getProductName());
					txtDescription.setText(tableModel.getList().get(selectRow).getDescription());
					txtQuantityAvailable.setText(tableModel.getList().get(selectRow).getAvailableQty().toString());
					txtPrice.setText(tableModel.getList().get(selectRow).getPrice().toString());
					txtBarCodeNo.setText(tableModel.getList().get(selectRow).getBarCodeNo());
					txtThreshold.setText(tableModel.getList().get(selectRow).getReOrderQty().toString());
					txtOrderQty.setText(tableModel.getList().get(selectRow).getOrderQty().toString());
					cboCategory.setSelectedItem(getCategoryCode(txtProductID.getText().trim()));
				*/} else {
					clearProduct();
				}
			}

		});
		generateSelectedProduct(cboFilterCategory.getSelectedItem().toString());
	}

	private void clearProduct() {
		txtProductID.setText(getProductID());
		txtProductName.setText(null);
		txtDescription.setText(null);
		txtQuantityAvailable.setText(null);
		txtPrice.setText(null);
		txtBarCodeNo.setText(null);
		txtThreshold.setText(null);
		txtOrderQty.setText(null);
		cboCategory.setEnabled(true);
		btnAdd.setEnabled(true);
		
	}

	private String[] listCategoryCode() {
		return categoryService.listCategoryCode();
	}

	private List<Product> listProduct() {
		return productService.getAll();
	}

	private static String getCategoryCode(String productID) {
		if (productID != null) {
			return productID.substring(0, 3);
		}
		return null;
	}

	private void add() {
		boolean result = false;

		// check about the duplicate records for same product and same category
		try {
			result = productService.isDuplicate(txtProductName.getText(), cboCategory.getSelectedItem().toString());
			if (result) {
				JOptionPane.showMessageDialog(null, "Product Name for this category is already existed.",
						"Duplicated of product", JOptionPane.ERROR_MESSAGE);
			} else {
				// add the product
				try {
					result = productService.add(txtProductID.getText(), txtProductName.getText(),
							txtDescription.getText(), txtQuantityAvailable.getText(), txtPrice.getText(),
							txtBarCodeNo.getText(), txtThreshold.getText(), txtOrderQty.getText());
					if (!result) {
						JOptionPane.showMessageDialog(null, "Cannot insert new product.", "Error in adding new product",
								JOptionPane.ERROR_MESSAGE);
					} else {
//						generateSelectedProduct(cboFilterCategory.getSelectedItem().toString());
						refreshProductList();
						clearProduct();
						txtProductID.setText(getProductID());
					}
				}catch (ValidationException e) {
					JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
				} catch (ProductCodeInvalidException e) {
					JOptionPane.showMessageDialog(null, "Cannot insert new product.", e.getMessage(),
							JOptionPane.ERROR_MESSAGE);
					} catch (ProductCode_BarCodeExistException e) {
					JOptionPane.showMessageDialog(this, e.getMessage(), "Error in inserting prodcut", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (ValidationException e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage(), e1.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

	private void edit() {
		boolean result = false;
		try {
			result = productService.edit(txtProductID.getText(), txtProductName.getText(), txtDescription.getText(),
					txtQuantityAvailable.getText(), txtPrice.getText(), txtBarCodeNo.getText(), txtThreshold.getText(),
					txtOrderQty.getText());
			if (!result) {
				JOptionPane.showMessageDialog(null, "Cannot update product.", "Error in updating product",
						JOptionPane.ERROR_MESSAGE);
			} else {
//				generateSelectedProduct(cboFilterCategory.getSelectedItem().toString());
				refreshProductList();
				clearProduct();
				txtProductID.setText(getProductID());
			}
		} catch (ProductCodeInvalidException e) {
			e.printStackTrace();
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}

//	private void delete(ProductTable tableModel) {
//		boolean result = false;
//		result = productService.remove(txtProductID.getText().trim());
//		if (!result) {
//			JOptionPane.showMessageDialog(null, "Cannot delete product.", "Error in deleting product",
//					JOptionPane.ERROR_MESSAGE);
//		} else {
//			tableModel.deleteRow(table.getSelectedRow());
//		}
//
//	}



	private Product getProduct() {
		return new Product(txtProductID.getText().trim(), txtProductName.getText().trim(),
				txtDescription.getText().trim(), Integer.parseInt(txtQuantityAvailable.getText().trim()),
				Double.parseDouble(txtPrice.getText().trim()), txtBarCodeNo.getText().trim(),
				Integer.parseInt(txtThreshold.getText().trim()), Integer.parseInt(txtOrderQty.getText().trim()));
	}
	
	protected void generateSelectedProduct(String string) {
		List<Product> list=new ArrayList<Product>();
		if(cboFilterCategory.getSelectedItem().toString().equals("All")){
			list=productService.getAll();
		}
		else{
			
		list = productService.getProductByCategory(cboFilterCategory.getSelectedItem().toString());
		}
		
		ProductTable tableModel = new ProductTable(list);
		table.setModel(tableModel);
		//clearVendor();
		//lblCategoryName.setText(string);
		//validate();
	}
	
	private void setProduct(Product product) {
		txtProductID.setText(product.getProductID());
		txtProductName.setText(product.getProductName());
		txtDescription.setText(product.getDescription());
		txtQuantityAvailable.setText(product.getAvailableQty().toString());
		txtPrice.setText(product.getPrice().toString());
		txtBarCodeNo.setText(product.getBarCodeNo());
		txtThreshold.setText(product.getReOrderQty().toString());
		txtOrderQty.setText(product.getOrderQty().toString());
		cboCategory.setSelectedItem(getCategoryCode(product.getProductID()));
		cboCategory.setEnabled(false);
	}
	
	private String getProductID(){
		return productService.getProductID(cboCategory.getSelectedItem().toString());
	}

	private void refreshProductList() {
		((ProductTable) table.getModel()).fireTableDataChanged();
	}
}
