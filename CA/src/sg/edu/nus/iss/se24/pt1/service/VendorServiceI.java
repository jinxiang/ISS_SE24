package sg.edu.nus.iss.se24.pt1.service;

import java.util.*;

import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import sg.edu.nus.iss.se24.pt1.model.Vendor;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Category;

//Created by Nguwar

public interface VendorServiceI {
	
	public List<Category> getAllCategory();
	
	public List<Vendor> getAllByCategoryCode(String categoryCode);

	public boolean add(String categoryCode, String vendorName, String vendorDescription) throws ValidationException, VendorExistedException;

	public boolean edit(String categoryCode,String vendorName, String vendorDescription) throws ValidationException, VendorNotExistException;
	
	public boolean delete(Category category, Vendor vendor) throws VendorNotExistException;
	
}
