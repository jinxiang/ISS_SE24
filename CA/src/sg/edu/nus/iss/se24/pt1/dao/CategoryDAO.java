package sg.edu.nus.iss.se24.pt1.dao;

import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.model.Category;

/**
 * created by Nilar 
 *
 */
public interface CategoryDAO {
	List<Category> findAll();
    Category findByName(String name);
    boolean insertCategory(Category category) throws CategoryCodeExistedException;
    boolean updateCategory(Category category);
    boolean deleteCategory(String categoryCode);
    void commit();
	
}
