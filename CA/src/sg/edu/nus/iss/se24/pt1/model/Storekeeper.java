package sg.edu.nus.iss.se24.pt1.model;
import java.util.Comparator;

/**
 * Created by Myo Wai Yan Kyaw on 3/13/2016.
 */
public class Storekeeper implements Comparator<Storekeeper>{

	private String storeKeeperName;
    private String storeKeeperPassword;

	public Storekeeper(){	}

	/**
	 * @param storeKeeperName
	 * @param storeKeeperPassword
	 */
	public Storekeeper(String storeKeeperName, String storeKeeperPassword) {
		this.storeKeeperName = storeKeeperName;
		this.storeKeeperPassword = storeKeeperPassword;
	}

	/**
	 * @return the storeKeeperName
	 */
	public String getStoreKeeperName() {
		return storeKeeperName;
	}

	/**
	 * @return the storeKeeperPassword
	 */
	public String getStoreKeeperPassword() {
		return storeKeeperPassword;
	}

	public String toString(){
		return storeKeeperName+","+storeKeeperPassword;
	}

	@Override
	public int compare(Storekeeper s1, Storekeeper s2) {
		if(s1.storeKeeperPassword.equalsIgnoreCase(s2.storeKeeperPassword)
		&& s1.storeKeeperName.equalsIgnoreCase(s2.storeKeeperPassword) ){
			return 0;
		}else {
			return -1;
		}
	}
}