package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Storekeeper;

import java.util.List;

/**
 * Created by xinqu on 3/19/2016.
 */
public interface StorekeeperDAO {

    List<Storekeeper> findAll();
    Storekeeper findByName(String name);
    boolean insertStorekeeper(Storekeeper storekeeper);
    boolean updateStorekeeper(Storekeeper storekeeper);
    boolean deleteStorekeeper(String storekeeperName);
    void commit();
}
