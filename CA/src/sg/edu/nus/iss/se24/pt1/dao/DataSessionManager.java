package sg.edu.nus.iss.se24.pt1.dao;

/**
 * Created by xinqu on 4/2/2016.
 */
public class DataSessionManager {
    private CategoryDAO categoryDAO;
    private DiscountDAO discountDAO;
    private MemberDAO memberDAO;
    private ProductDAO productDAO;
    private StorekeeperDAO storekeeperDAO;
    private TransactionDAO transactionDAO;
    private VendorDAO vendorDAO;
    private static DataSessionManager instance = null;

    private DataSessionManager() {
        categoryDAO = CategoryDAOImpl.getInstance();
        discountDAO = DiscountDAOImpl.getInstance();
        memberDAO = MemberDAOImpl.getInstance();
        productDAO = ProductDAOImpl.getInstance();
        storekeeperDAO = StorekeeperDAOImpl.getInstance();
        transactionDAO = TransactionDAOImpl.getInstance();
        vendorDAO = VendorDAOImpl.getInstance();
    }

    public static DataSessionManager getInstance() {
        if (instance == null) {
            synchronized (DataSessionManager.class) {
                if (instance == null) {
                    instance = new DataSessionManager();
                    return instance;
                }
            }
        }
        return instance;
    }

    public void commit() {
        categoryDAO.commit();
        discountDAO.commit();
        memberDAO.commit();
        productDAO.commit();
        storekeeperDAO.commit();
        transactionDAO.commit();
        vendorDAO.commit();
    }

    public CategoryDAO getCategoryDAO() {
        return categoryDAO;
    }

    public DiscountDAO getDiscountDAO() {
        return discountDAO;
    }

    public MemberDAO getMemberDAO() {
        return memberDAO;
    }

    public ProductDAO getProductDAO() {
        return productDAO;
    }

    public StorekeeperDAO getStorekeeperDAO() {
        return storekeeperDAO;
    }

    public TransactionDAO getTransactionDAO() {
        return transactionDAO;
    }

    public VendorDAO getVendorDAO() {
        return vendorDAO;
    }
}