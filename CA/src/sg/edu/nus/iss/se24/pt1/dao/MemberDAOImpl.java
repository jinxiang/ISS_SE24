package sg.edu.nus.iss.se24.pt1.dao;

import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.model.Member;

//Created by Nguwar

public class MemberDAOImpl extends GenericFileDAO<Member> implements MemberDAO {
	private static MemberDAOImpl instance = null;
	private ArrayList<Member> data;

	public static MemberDAOImpl getInstance(){
		if(instance == null){
			synchronized (MemberDAOImpl.class){
				if(instance == null){
					instance = new MemberDAOImpl();
					return instance;
				}
			}
		}
		return instance;
	}
	private MemberDAOImpl() {
		super(Member.class);
		data = load();
	}

	@Override
	public List<Member> findAll() {
		return data;
	}

	@Override
	public Member findByID(String memberID) {
        for(Member member:data){
            if(member.getMemberID().equalsIgnoreCase(memberID)){
                return member;
            }
        }
		return null;
	}

	@Override
	public synchronized boolean insertMember(Member memberToInsert) {
        boolean toInsert=true;
        for(Member member:data){
            if(member.getMemberID().equalsIgnoreCase(memberToInsert.getMemberID())){
                toInsert = false;
                break;
            }
        }
        if(toInsert) {
        	data.add(memberToInsert);
        }
        return toInsert;
	}

	@Override
	public synchronized boolean updateMember(Member memberToUpdate) {
	        for(Member member:data){
	            if(member.getMemberID().equalsIgnoreCase(memberToUpdate.getMemberID())){
	            	data.remove(member);
	            	data.add(memberToUpdate);
	                return true;
	            }
	        }
	        return false;
	}

	@Override
	public synchronized boolean deleteMember(String memberID) {
        for(Member member:data){
            if(member.getMemberID().equalsIgnoreCase(memberID)){
            	data.remove(member);
                return true;
            }
        }
        return false;
	}

	@Override
	public synchronized void commit() {
		this.commit(data);
	}
}
