/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.dao.DiscountDAOImpl;
import sg.edu.nus.iss.se24.pt1.dao.Transformer;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Discount;

/**
 * @author Myo Wai Yan Kyaw
 * @author jinxiang
 *
 */
public class DiscountService implements DiscountServiceI {

	private DiscountDAOImpl database;

	public DiscountService() {
		database = DiscountDAOImpl.getInstance();
	}

	@Override
	public List<Discount> getAll() {
		return database.findAll();
	}

	@Override
	public Discount getByDiscountID(String discountCode) {
		return database.findByCode(discountCode);
	}

	@Override
	public boolean add(String discountCode, String description, String startDate, String periodOfDiscount,
			String percentageDiscount, char memberFlg) throws ValidationException {
		validateDiscount(discountCode, description, startDate, periodOfDiscount, percentageDiscount, memberFlg, false);
		Discount discountToInsert = new Discount(discountCode.trim(), description.trim(), startDate.trim(),
				periodOfDiscount.trim(), Integer.valueOf(percentageDiscount.trim()), memberFlg);
		boolean getResult = database.insertDiscount(discountToInsert);
		if (getResult) {
			database.commit();
		}
		return getResult;
	}

	@Override
	public boolean edit(String discountCode, String description, String startDate, String periodOfDiscount,
			String percentageDiscount, char memberFlg) throws ValidationException {
		validateDiscount(discountCode, description, startDate, periodOfDiscount, percentageDiscount, memberFlg, true);
		Discount discountToUpdate = new Discount(discountCode.trim(), description.trim(), startDate.trim(),
				periodOfDiscount.trim(), Integer.valueOf(percentageDiscount.trim()), memberFlg);
		boolean getResult = database.updateDiscount(discountToUpdate);
		if (getResult) {
			database.commit();
		}
		return getResult;
	}

	@Override
	public boolean remove(String discountCode) {
		boolean getResult = database.deleteDiscount(discountCode);
		if (getResult) {
			database.commit();
		}
		return getResult;
	}

	public void validateDiscount(String discountCode, String description, String startDate, String periodOfDiscount,
			String strPercentageDiscount, char strMemberFlg, boolean editMode) throws ValidationException {
		if (discountCode.isEmpty() || discountCode.trim().length() == 0)
			throw new ValidationException("Discount", "Discount Code Is Empty.");
		if (getByDiscountID(discountCode) != null && editMode == false)
			throw new ValidationException("Discount", "Discount Code Exist.");
		if (description.isEmpty() || description.trim().length() == 0)
			throw new ValidationException("Discount", "Discount Description Is Empty.");
		if (startDate.isEmpty() || startDate.trim().length() == 0)
			throw new ValidationException("Discount", "Start Date Is Empty.");
		try {
			Transformer.stringToDate(startDate);
			if (!isValidDate(startDate))
				throw new ValidationException("Discount", "Start Date Is Invalid.");
		} catch (ParseException e) {
			if (!"ALWAYS".equalsIgnoreCase(startDate)) {
				throw new ValidationException("Discount",
						"Start Date of Discount must be either 'ALWAYS' or in 'yyyy-MM-dd' format.");
			}
		}
		if (periodOfDiscount.isEmpty() || periodOfDiscount.trim().length() == 0)
			throw new ValidationException("Discount", "Period of Discount Is Empty.");
		else {
			try {
				Integer.parseInt(periodOfDiscount);
				if ("ALWAYS".equalsIgnoreCase(startDate)) {
					throw new ValidationException("Discount", "Period of Discount must be 'ALWAYS'.");
				}
			} catch (NumberFormatException e) {
				if (!"ALWAYS".equalsIgnoreCase(periodOfDiscount)) {
					throw new ValidationException("Discount",
							"Period of Discount must be either 'ALWAYS' or in Number.");
				}
			}
		}
		if (strPercentageDiscount.isEmpty() || strPercentageDiscount.trim().length() == 0)
			throw new ValidationException("Discount", "Discount Percentage Is Empty.");
		try {
			Integer.parseInt(strPercentageDiscount);
			if (Integer.parseInt(strPercentageDiscount) > 100 || Integer.parseInt(strPercentageDiscount) < 0) {
				throw new ValidationException("Discount", "Discount Percentage Must Greater Than 0 And Less Than 100.");
			}
		} catch (NumberFormatException e) {
			throw new ValidationException("Discount", "Discount Percentage Is Invalid Number.");
		}
		if (strMemberFlg != 'A' && strMemberFlg != 'M')
			throw new ValidationException("Discount", "Member Flag must be either 'A' or 'M'.");
	}

	public boolean isValidDate(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public boolean isTodayValidDiscountDay(Discount d) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date todayDate = new Date();
		String today = dateFormat.format(todayDate);
		String lastDay = null;
		Calendar c = Calendar.getInstance();

		if (d.getStartDate().equalsIgnoreCase("ALWAYS") && d.getPeriodOfDiscount().equalsIgnoreCase("ALWAYS")) {
			return true;
		}
		if (!d.getStartDate().equalsIgnoreCase("ALWAYS")) {
			if (dateFormat.parse(d.getStartDate()).compareTo(dateFormat.parse(today)) < 1
					&& d.getPeriodOfDiscount().equalsIgnoreCase("ALWAYS")) {
				return true;
			}
			if (dateFormat.parse(d.getStartDate()).compareTo(dateFormat.parse(today)) > 0) {
				return false;
			}
			if (!d.getPeriodOfDiscount().equalsIgnoreCase("ALWAYS")) {
				c.setTime(dateFormat.parse(d.getStartDate()));
				c.add(Calendar.DATE, Integer.parseInt(d.getPeriodOfDiscount()));
				lastDay = dateFormat.format(c.getTime()).toString();
				if (dateFormat.parse(today).compareTo(dateFormat.parse(lastDay)) < 1) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int getDiscountPercent(String memberID) {
		int discountPercentage = 0;
		List<Discount> discounts = database.findAll();

		for (Discount d : discounts) {
			try {
				if (isTodayValidDiscountDay(d)) {
					if (memberID.equalsIgnoreCase("PUBLIC") == false) {
						if (discountPercentage < d.getPercentageDiscount()) {
							discountPercentage = d.getPercentageDiscount();
						}
					} else {
						if (d.getMemberFlg() == 'A') {
							if (discountPercentage < d.getPercentageDiscount()) {
								discountPercentage = d.getPercentageDiscount();
							}
						}
					}
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return discountPercentage;
	}
}
