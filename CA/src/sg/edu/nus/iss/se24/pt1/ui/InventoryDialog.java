package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import sg.edu.nus.iss.se24.pt1.model.Inventory;
import sg.edu.nus.iss.se24.pt1.service.ProductService;
import sg.edu.nus.iss.se24.pt1.service.ProductServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.InventoryTable;

/**
 * @author jinxiang
 *
 */
public class InventoryDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ProductServiceI productService = new ProductService();
	private JTable table;
	private StoreFrame parent;

	/**
	 * Create the dialog.
	 */
	public InventoryDialog() {
		/**
		 * Create the dialog.
		 */
		this(null, null);
	}

	public InventoryDialog(final StoreFrame frame, String title) {
		this(frame, title, true);
	}

	public InventoryDialog(final StoreFrame frame, String title, boolean flgFrame) {
		super(frame, title, true);
		this.parent = frame;
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			table = new JTable(new InventoryTable(listInventory()));
			JScrollPane scrollPane = new JScrollPane(table);
			scrollPane.setPreferredSize(Constants.APP_DIMENSION);
			contentPanel.add(scrollPane);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnNewButton_3 = new JButton(Constants.STRING_LABEL_PO);
				btnNewButton_3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generatePurchasedOrder();
					}
				});
				buttonPane.add(btnNewButton_3);
			}
			{
				JButton cancelButton = new JButton(Constants.BUTTON_CANCEL_LABEL);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						parent.closeInventoryScreen(flgFrame);
					}
				});
				buttonPane.add(cancelButton);
			}
		}
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeInventoryScreen(flgFrame);
			}
		});
		pack();
	}

	protected void generatePurchasedOrder() {
		productService.genereateInventoryList();
		JOptionPane.showMessageDialog(this, "PO Generated");
	}

	private List<Inventory> listInventory() {
		return productService.getInventoryList();
	}

}
