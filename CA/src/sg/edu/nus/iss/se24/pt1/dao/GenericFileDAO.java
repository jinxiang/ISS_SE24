package sg.edu.nus.iss.se24.pt1.dao;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xinqu on 3/19/2016.
 */

public class GenericFileDAO<T> {

    private Class<T> type ;

    public GenericFileDAO(Class<T> cls)
    {
        this.type = cls;
    }

    private static final String DATA_PATH = "data";
    ArrayList<T> load(){
        String dataFilePath = DATA_PATH+File.separator+  (  type.getSimpleName().equals("Category")?  
        		type.getSimpleName() +  ".dat"  : type.getSimpleName()  + "s.dat" );
        ArrayList<T> dataList = new ArrayList<T>();
        try {
            if(!new File(dataFilePath).exists() ) {
                if(!new File(DATA_PATH).exists()){
                    new File(DATA_PATH).mkdirs();
                }
                new File(dataFilePath).createNewFile();
            }
            BufferedReader bufferedReader = new BufferedReader(new FileReader(dataFilePath));
            while (bufferedReader.ready()) {
                String[] cell = bufferedReader.readLine().split(",");
                T instance = type.newInstance();
                for (int index = 0; index < cell.length; index++) {

                    Field field = type.getDeclaredFields()[index];
                    String fieldTypeStr = field.getAnnotatedType().getType().getTypeName();
                    Method[] methods = Transformer.class.getDeclaredMethods();
                    for (Method method : methods) {
                        if (method.getParameterTypes().length == 1) {
                            // Transformer should accept 1 argument. This skips the Transformer() method.
                            field.setAccessible(true);
                            if (method.getName().compareToIgnoreCase("stringTo" + fieldTypeStr.substring( fieldTypeStr.lastIndexOf('.')+1,fieldTypeStr.length())) == 0) {
                                field.set(instance, method.invoke(new Transformer(), cell[index].trim()));
                            }else if(fieldTypeStr.endsWith( "String" )){
                                field.set(instance, cell[index].trim());
                            }
                        }
                    }
                }
                dataList.add(instance);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public  void commit( List<T> modelList ){
      
    	String dataFilePath = DATA_PATH+File.separator+ (type.getSimpleName().equals("Category")?
    			type.getSimpleName() +  ".dat"  : type.getSimpleName()  +"s.dat");
        
        String backupDataFilePath = DATA_PATH+File.separator+ (type.getSimpleName().equals("Category")?
        		type.getSimpleName() +  ".dat.bk"  :type.getSimpleName()  +"s.dat.bk"); 
        try {

            //backup the dat file before commit
            if (new File(dataFilePath).exists()) {
                if(new File(backupDataFilePath).exists()){
                    new File(backupDataFilePath).delete();
                }
                new File(dataFilePath).renameTo(new File(backupDataFilePath));
                new File(dataFilePath).delete();
            }
            PrintWriter printWriter = new PrintWriter(new File(dataFilePath));
            for (T model : modelList) {
                printWriter.println(model.toString());
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //if commit fails, restore from backup
            if(!new File(dataFilePath).exists() && new File(backupDataFilePath).exists()){
                new File(backupDataFilePath).renameTo(new File(dataFilePath));
            }
        }
    }

}
