/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.exception;

/**
 * An DuplicatedMemberIDException is thrown when there is existing Member ID during registration.
 * 
 * @author jinxiang
 *
 */
public class DuplicatedMemberIDException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2831636126886208587L;
	
	private static final String STRING_ERROR_FORMAT = "Member ID : %s is duplicated";
	
	public DuplicatedMemberIDException(String memberID) {
		super(String.format(STRING_ERROR_FORMAT, memberID));
	}
	
}
