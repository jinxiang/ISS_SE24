package sg.edu.nus.iss.se24.pt1.dao;
import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.model.Category;

/**
 * created by Nilar 
 *
 */
public class CategoryDAOImpl extends  GenericFileDAO<Category> implements CategoryDAO  {

    private static CategoryDAOImpl instance = null;
    private ArrayList<Category> data;

    public static CategoryDAOImpl getInstance(){
        if(instance == null){
            synchronized (CategoryDAOImpl.class){
                if(instance == null){
                    instance = new CategoryDAOImpl();
                    return instance;
                }
            }
        }
        return instance;
    }

	private CategoryDAOImpl() {
        super(Category.class);
        data = load();
    }


	
	@Override
    public List<Category> findAll() {
        return  data;
    }

    @Override
    public Category findByName(String name) {
        for(Category category:data){
            if(category.getCategoryName().equalsIgnoreCase(name)){
                return category;
            }
        }
        return  null;
    }

    @Override
    public synchronized boolean insertCategory(Category categoryToInsert) throws CategoryCodeExistedException {
        Boolean toInsert=true;
        for(Category category:data){
            if(category.getCategoryCode().equalsIgnoreCase(categoryToInsert.getCategoryCode())){
                toInsert = false;
            }
        }
        if(toInsert) {
        	data.add(categoryToInsert);
        }
        else
        {
        	throw new CategoryCodeExistedException();
        }
        return toInsert;
    }

    @Override
    public synchronized boolean updateCategory(Category categoryToUpdate) {
        for(Category category:data){
            if(category.getCategoryCode().equalsIgnoreCase(categoryToUpdate.getCategoryCode())){
                data.remove(category);
                data.add(categoryToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean deleteCategory(String categoryToDelete) {
        for(Category category:data){
            if(category.getCategoryCode().equalsIgnoreCase(categoryToDelete)){
                data.remove(category);
                return true;
            }
        }
        return false;
    }

    public synchronized void commit(){
        this.commit(data);
    }
	

}
