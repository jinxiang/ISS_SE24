package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import sg.edu.nus.iss.se24.pt1.Application;
import sg.edu.nus.iss.se24.pt1.ui.bean.Menu;

import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author jinxiang
 *
 */
public class StoreFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2090966370041581555L;

	JDialog loginDialog;
	JDialog inventoryDialog;
	JDialog reportingDialog;
	JDialog adminDialog;
	JDialog checkoutDialog;

	private JPanel contentPane;

	private JLabel lblUserLabel = new JLabel();
	
	private Application app;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StoreFrame frame = new StoreFrame(null, null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param sTRING_TITLE_APPLICATION 
	 * @param application 
	 */
	public StoreFrame(Application application, String title) {
		this.app = application;
		setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setPreferredSize(new Dimension(600,400));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel userPanel = new JPanel();
		contentPane.add(userPanel, BorderLayout.NORTH);
		
		userPanel.add(lblUserLabel);
		
		JPanel menuPanel = new JPanel();
		contentPane.add(menuPanel, BorderLayout.CENTER);
		initMenuPanel(menuPanel);
		showLoginScreen();
	}
	
	private void initMenuPanel(JPanel menuPanel) {
		menuPanel.setLayout(new GridLayout(Menu.values().length, 1, 0, 0));
		JButton btnNewMember = new JButton(Menu.ADMIN.getLabel());
		btnNewMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAdminScreen();
			}
		});
		menuPanel.add(btnNewMember);
		JButton btnCheckout = new JButton(Menu.CHECK_OUT.getLabel());
		btnCheckout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showCheckoutScreen();
			}
		});
		menuPanel.add(btnCheckout);
		JButton btnInventoryList = new JButton(Menu.INVENTORY_LIST.getLabel());
		btnInventoryList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showInventoryScreen();
			}
		});
		menuPanel.add(btnInventoryList);
		JButton btnReporting = new JButton(Menu.REPORTING.getLabel());
		btnReporting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showReportingScreen();
			}
		});
		menuPanel.add(btnReporting);
		JButton btnLogoff = new JButton(Menu.LOGOUT.getLabel());
		btnLogoff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shutdown();
			}
		});
		menuPanel.add(btnLogoff);
	}
	
	public void setUser(String username) {
		lblUserLabel.setText(String.format(Constants.STRING_TITLE_USER_INFO, username));
	}
	
	private void showLoginScreen() {
		loginDialog = new LoginDialog(this,Constants.STRING_TITLE_LOGIN_SCREEN);
		loginDialog.setVisible(true);
	}
	
	public void closeLoginScreen() {
		JOptionPane.showMessageDialog(this,
			    Constants.STRING_MESSAGE_LOGOFF);
		shutdown();
	}
	
	private void shutdown() {
		if (loginDialog != null) {
			loginDialog.dispose();
			loginDialog = null;
		}
		closeAdminScreen();
		closeReportingScreen();
		app.close();
	}
	
	private void showAdminScreen() {
		setVisible(false);
		adminDialog = new AdminstrationDialog(this, Constants.STRING_TITLE_ADMINISTEATION_SETUP_SCREEN);
		adminDialog.setLocationRelativeTo(null);
		adminDialog.setVisible(true);
	}
	
	public void closeAdminScreen() {
		if (adminDialog != null ) {
			adminDialog.dispose();
			adminDialog = null;
		}
		setVisible(true);
	}
	
	private void showReportingScreen() {
		setVisible(false);
		reportingDialog = new ReportingDialog(this, Constants.STRING_TITLE_REPORTING_SCREEN);
		reportingDialog.setLocationRelativeTo(null);
		reportingDialog.setVisible(true);
	}

	public void closeReportingScreen() {
		if (reportingDialog != null ) {
			reportingDialog.dispose();
			reportingDialog = null;
		}
		setVisible(true);
	}
	
	private void showInventoryScreen() {
		setVisible(false);
		inventoryDialog = new InventoryDialog(this, Constants.STRING_TITLE_INVENTORY_SCREEN);
		inventoryDialog.setLocationRelativeTo(null);
		inventoryDialog.setVisible(true);
	}

	public void closeInventoryScreen(boolean flgFrame) {
		if (inventoryDialog != null ) {
			inventoryDialog.dispose();
			inventoryDialog = null;
		}
		if(flgFrame) {
			setVisible(true);
		}
	}
	
	public void setInventoryDialog(InventoryDialog inventoryDialog) {
		this.inventoryDialog = inventoryDialog;
	}
	
	private void showCheckoutScreen() {
		setVisible(false);
		checkoutDialog = new CheckOutScreenDialog(this, Constants.STRING_TITLE_CHECKOUT_SCREEN);
		checkoutDialog.setLocationRelativeTo(null);
		checkoutDialog.setVisible(true);
	}
	
	public void closeCheckoutScreen() {
		if (checkoutDialog != null ) {
			checkoutDialog.dispose();
			checkoutDialog = null;
		}
		setVisible(true);
	}
}
