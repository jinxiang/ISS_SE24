package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import sg.edu.nus.iss.se24.pt1.model.Vendor;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xinqu on 3/19/2016.
 */
public class VendorDAOImpl implements VendorDAO {

    private static final String DATA_PATH = "data";

    private static VendorDAOImpl instance = null;
    private HashMap<String,ArrayList<Vendor>> data;

    private VendorDAOImpl(){
        data = load();
    }

    public static VendorDAOImpl getInstance(){
        if(instance == null){
            synchronized (VendorDAOImpl.class){
                if(instance == null){
                    instance = new VendorDAOImpl();
                    return instance;
                }
            }
        }
        return instance;
    }

    private HashMap<String,ArrayList<Vendor>> load(){
        HashMap<String,ArrayList<Vendor>> vendorHashMap = new HashMap<>();
        if(new File(DATA_PATH).exists()){
            for(File file:new File(DATA_PATH).listFiles()){
                ArrayList<Vendor> vendorArrayList = new ArrayList<>();
                if(file.getName().matches("Vendors(\\w{3})\\.dat")){
                    String category="";
                    Pattern pattern = Pattern.compile("Vendors(\\w{3})\\.dat");
                    Matcher matcher = pattern.matcher(file.getName());

                    while (matcher.find()){
                        category=matcher.group(1);
                    }

                    try {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                        while (bufferedReader.ready()) {
                            String[] cell = bufferedReader.readLine().split(",");
                            vendorArrayList.add(new Vendor(cell[0],cell[1]));
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    vendorHashMap.put(category,vendorArrayList);
                }
            }
        }
        return vendorHashMap;
    }

    public synchronized void commit(){
        for(String category:data.keySet()){
            String vendorFilePath = DATA_PATH+File.separator+"Vendors"+category+".dat";
            String backupVendorFilePath = DATA_PATH+File.separator+"Vendors"+category+".dat.bk";
            try {
                //backup the dat file before commit
                if (new File(vendorFilePath).exists()) {
                    if(new File(backupVendorFilePath).exists()){
                        new File(backupVendorFilePath).delete();
                    }
                    new File(vendorFilePath).renameTo(new File(backupVendorFilePath));
                    new File(vendorFilePath).delete();
                }

                PrintWriter printWriter = new PrintWriter(new File(vendorFilePath));
                for(Vendor vendor:data.get(category)){
                    printWriter.println(vendor.toString());
                }
                printWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                //if commit fails, restore from backup
                if(!new File(vendorFilePath).exists() && new File(backupVendorFilePath).exists()){
                    new File(backupVendorFilePath).renameTo(new File(vendorFilePath));
                }
            }
        }
    }

    @Override
    public HashMap<String, ArrayList<Vendor>> findAll() {
        return data;
    }

    @Override
    public List<Vendor> findByCategory(String category) {
    	for (Map.Entry<String, ArrayList<Vendor>> entry : data.entrySet()) {
    		if(entry.getKey().equalsIgnoreCase(category))
    			return entry.getValue();
    	}
        return null;
    }

    @Override
    public HashMap<String,Vendor> findByName(String name) {
        return null;
    }

    @Override
    public synchronized boolean insertVendor(String category, Vendor vendor) throws VendorExistedException {
        if(category.length()==3){
            boolean toInsert = true;
            if(data.containsKey(category)) {
                for (Vendor v : data.get(category)) {
                    if (v.getVendorName().equalsIgnoreCase(vendor.getVendorName())) {
                        throw new VendorExistedException("Vendor",v.getVendorName());
                    }
                }
                data.get(category).add(vendor);
            }else{
                ArrayList<Vendor> vendorArrayList = new ArrayList<Vendor>();
                vendorArrayList.add(vendor);
                data.put(category,vendorArrayList);
                return true;
            }
            return true;
        }
        return false;
    }

    @Override
    public synchronized boolean updateVendor(String category, Vendor vendor) throws VendorNotExistException {
        if(category.length()==3){
            if(data.containsKey(category)){
                for(Vendor v:data.get(category)){
                    if(v.getVendorName().equalsIgnoreCase(vendor.getVendorName())){
                        data.get(category).remove(v);
                        data.get(category).add(vendor);
                        return true;
                    }
                }
                throw new VendorNotExistException("Vendor",vendor.getVendorName());
            }
        }
        return false;
    }

    @Override
    public synchronized boolean deleteVendor(String category,String vendorName) throws VendorNotExistException {
        if(category.length()==3){
            if(data.containsKey(category)){
                for(Vendor v:data.get(category)){
                    if(v.getVendorName().equalsIgnoreCase(vendorName)){
                        data.get(category).remove(v);
                        return true;
                    }
                }
                throw new VendorNotExistException("Vendor",vendorName);
            }
        }
        return false;
    }
}
