package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Storekeeper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xinqu on 3/19/2016.
 */
public class StorekeeperDAOImpl extends GenericFileDAO<Storekeeper> implements StorekeeperDAO {

    private StorekeeperDAOImpl(){
        super(Storekeeper.class);
        data = load();
    }

    private static StorekeeperDAOImpl instance = null;
    private ArrayList<Storekeeper> data;

    public static StorekeeperDAOImpl getInstance(){
        if(instance == null){
            synchronized (StorekeeperDAOImpl.class){
                if(instance == null){
                    instance = new StorekeeperDAOImpl();
                    return instance;
                }
            }
        }
        return instance;
    }

    @Override
    public ArrayList<Storekeeper> findAll() {
        return  data;
    }

    @Override
    public Storekeeper findByName(String name) {
        for(Storekeeper storekeeper:data){
            if(storekeeper.getStoreKeeperName().equalsIgnoreCase(name)){
                return storekeeper;
            }
        }
        return  null;
    }

    @Override
    public synchronized boolean insertStorekeeper(Storekeeper storekeeperToInsert) {
        Boolean toInsert=true;
        for(Storekeeper storekeeper:data){
            if(storekeeper.getStoreKeeperName().equalsIgnoreCase(storekeeperToInsert.getStoreKeeperName())){
                toInsert = false;
            }
        }
        if(toInsert) {
            data.add(storekeeperToInsert);
        }
        return toInsert;
    }

    @Override
    public synchronized boolean updateStorekeeper(Storekeeper storekeeperToUpdate) {
        for(Storekeeper storekeeper:data){
            if(storekeeper.getStoreKeeperName().equalsIgnoreCase(storekeeperToUpdate.getStoreKeeperName())
               && !storekeeper.getStoreKeeperPassword().equalsIgnoreCase(storekeeperToUpdate.getStoreKeeperPassword())){
                data.remove(storekeeper);
                data.add(storekeeperToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean deleteStorekeeper(String storekeeperToDelete) {
        for(Storekeeper storekeeper:data){
            if(storekeeper.getStoreKeeperName().equalsIgnoreCase(storekeeperToDelete)){
                data.remove(storekeeper);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized void commit() {
        this.commit(data);
    }
}
