package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.service.CheckOutServiceI;
import sg.edu.nus.iss.se24.pt1.service.MemberService;
import sg.edu.nus.iss.se24.pt1.service.MemberServiceI;
import sg.edu.nus.iss.se24.pt1.ui.Constants;
import sg.edu.nus.iss.se24.pt1.ui.StoreFrame;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.security.Provider.Service;
import java.awt.event.ActionEvent;

public class ReadMemberIDDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final StoreFrame parent;
	private JTextField txtMemberID;
	private CheckOutServiceI service;
	private MemberServiceI memberService;

	/**
	 * Create the dialog.
	 */
	public ReadMemberIDDialog(StoreFrame frame, String title, CheckOutServiceI service) {
		super(frame, title, true);
		parent = frame;
		this.service = service;
		this.memberService = new MemberService();
		
		setPreferredSize(Constants.APP_DIMENSION);
		setBounds(100, 100, 400, 156);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(2, 2, 0, 0));
		{
			JLabel lblNewLabel_1 = new JLabel("Member ID");
			contentPanel.add(lblNewLabel_1);
		}
		{
			txtMemberID = new JTextField();
			contentPanel.add(txtMemberID);
			txtMemberID.setColumns(10);
		}
		{
			
		}
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					add();
				}
			});
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancel();
				}
			});
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}
		
	}
	
	private void add(){
		if(isValidAdding()){
			service.updateMemberID(this.txtMemberID.getText().trim());
			this.setVisible(false);
			dispose();
		}
	}
	
	private void cancel(){
		this.setVisible(false);
		dispose();
	}
	
	public void showDialog(){
		this.setVisible(true);
	}
	
	private boolean isValidAdding(){
		
		Member member = this.memberService.getByMemberID(this.txtMemberID.getText().trim());
		if(member == null)
			return false;
		
		return true;
	}

}
