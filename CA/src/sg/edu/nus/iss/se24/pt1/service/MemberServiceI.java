/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

import java.util.*;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Member;

/**
 * @author jinxiang
 *
 */
public interface MemberServiceI {
	
	public List<Member> getAll();
	
	public Member getByMemberID(String memberID);
	
	public boolean add(String memberID, String memberName, String loyaltyPoint) throws ValidationException;
	
	public boolean edit(String memberID, String memberName, String loyaltyPoint) throws ValidationException;
	
	public boolean delete(String memberID);
	
	public boolean isDuplicate(String MemberID);

}
