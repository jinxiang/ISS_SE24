package sg.edu.nus.iss.se24.pt1.exception;

/**
 * created by Nilar
 *
 */

public class ProductCodeInvalidException extends Exception {
	private static final String ERROR_MESSAGE ="Invalid product code.";
    public ProductCodeInvalidException() {
        super(ERROR_MESSAGE);
    }

}


