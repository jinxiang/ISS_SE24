/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.model;

/**
 * @author jinxiang
 *
 */
public class Inventory {
	private String productId;
	private String productName;
	private Double price;
	private Integer quantityAvailable;
	private Integer threshold;
	private Integer orderQuantity;
	
	private Product product;

	public Inventory() {
	}

	/**
	 * @param productId
	 * @param productName
	 * @param price
	 * @param quantityAvailable
	 * @param threshold
	 * @param orderQuantity
	 */
	public Inventory(String productId, String productName, Double price, Integer quantityAvailable, Integer threshold,
			Integer orderQuantity) {
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.quantityAvailable = quantityAvailable;
		this.threshold = threshold;
		this.orderQuantity = orderQuantity;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the quantityAvailable
	 */
	public Integer getQuantityAvailable() {
		return quantityAvailable;
	}

	/**
	 * @param quantityAvailable
	 *            the quantityAvailable to set
	 */
	public void setQuantityAvailable(Integer quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	/**
	 * @return the threshold
	 */
	public Integer getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	/**
	 * @return the orderQuantity
	 */
	public Integer getOrderQuantity() {
		return orderQuantity;
	}

	/**
	 * @param orderQuantity
	 *            the orderQuantity to set
	 */
	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return productId + "," + productName + "," + orderQuantity;
//		return product.getProductID() + "," + productName + "," + orderQuantity;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

}
