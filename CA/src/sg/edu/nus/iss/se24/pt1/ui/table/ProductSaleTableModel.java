/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Product;

/**
 * @author jinxiang
 *
 */
public class ProductSaleTableModel extends AbstractTableModel {
	
	private List<Product> list;
	
	private final String[] columnNames = new String[] { "Product ID", "Bar Code", "Product Name", "Price", "Quantity Available",
			"Threshold", "Order Quantity", "Description" };
	

	
	/**
	 * @param list
	 */
	public ProductSaleTableModel(List<Product> list) {
		this.list = list;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7381680171810406151L;
	
	

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return list.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return 8;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return list.get(0).getProductID().getClass();
			
		case 1:
			return list.get(0).getBarCodeNo().getClass();
			
		case 2:
			return list.get(0).getProductName().getClass();
			
		case 3:
//			list.get(0).getPrice();
			return Double.class;
		
		case 4:
//			list.get(0).getAvailableQty();
			return Integer.class;
			
		case 5:
//			list.get(0).getReOrderQty();
			return Integer.class;
			
		case 6:
//			list.get(0).getReOrderQty()
			return Integer.class;
			
		case 7:
			return list.get(0).getDescription().getClass();

		default:
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Product product = list.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return product.getProductID();
			
		case 1:
			return product.getBarCodeNo();
			
		case 2:
			return product.getProductName();
			
		case 3:
			return product.getPrice();
		
		case 4:
			return product.getAvailableQty();
			
		case 5:
			return product.getReOrderQty();
			
		case 6:
			product.getReOrderQty();
			
		case 7:
			return product.getDescription();

		default:
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

}
