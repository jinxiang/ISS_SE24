package sg.edu.nus.iss.se24.pt1.exception;
/**
 * created by Nilar
 *
 */
public class ProductCode_BarCodeExistException extends Exception {
	private static final String ERROR_MESSAGE ="Entered product code or Bar code is already existed in system.";
    public ProductCode_BarCodeExistException() {
        super(ERROR_MESSAGE);
    }
}
