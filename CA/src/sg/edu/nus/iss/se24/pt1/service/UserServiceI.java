/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.service;

/**
 * @author jinxiang
 *
 */
public interface UserServiceI {
	
	public boolean authenticate(String username, String password);

}
