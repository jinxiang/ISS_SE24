package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Discount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Myo Wai Yan Kyaw on 3/19/2016.
 */
public class DiscountDAOImpl extends GenericFileDAO<Discount> implements DiscountDAO {

    private static DiscountDAOImpl instance = null;
    private ArrayList<Discount> data;

    public static DiscountDAOImpl getInstance(){
        if(instance == null){
            synchronized (DiscountDAOImpl.class){
                if(instance == null){
                    instance = new DiscountDAOImpl();
                    return instance;
                }
            }
        }
        return instance;
    }

    private DiscountDAOImpl(){
        super(Discount.class);
        data = load();
    }

    @Override
    public List<Discount> findAll() {
        return  data;
    }

    @Override
    public Discount findByCode(String discountCode) {
        for(Discount discount:data){
            if(discount.getDiscountCode().equalsIgnoreCase(discountCode)){
                return discount;
            }
        }
        return  null;
    }
    
    @Override
    public Discount findDiscountForToday(boolean isMember) {
        return  null;
    }

    @Override
    public synchronized boolean insertDiscount(Discount discountToInsert) {
        Boolean toInsert=true;
        for(Discount discount:data){
            if(discount.getDiscountCode().equalsIgnoreCase(discountToInsert.getDiscountCode())){
                toInsert = false;
            }
        }
        if(toInsert) {
            data.add(discountToInsert);
        }
        return toInsert;
    }

    @Override
    public synchronized boolean updateDiscount(Discount discountToUpdate) {
        for(Discount discount:data){
            if(discount.getDiscountCode().equalsIgnoreCase(discountToUpdate.getDiscountCode())){
                data.remove(discount);
                data.add(discountToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean deleteDiscount(String discountToDelete) {
        for(Discount discount:data){
            if(discount.getDiscountCode().equalsIgnoreCase(discountToDelete)){
                data.remove(discount);
                return true;
            }
        }
        return false;
    }

    public synchronized void commit(){
        this.commit(data);
    }
}
