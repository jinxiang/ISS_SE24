package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Discount;

import java.util.List;

/**
 * Created by Myo Wai Yan Kyaw on 3/19/2016.
 */
public interface DiscountDAO {

    List<Discount> findAll();
    Discount findByCode(String code);
    Discount findDiscountForToday(boolean isMember);
    boolean insertDiscount(Discount discount);
    boolean updateDiscount(Discount discount);
    boolean deleteDiscount(String discountCode);
    void commit();

}
