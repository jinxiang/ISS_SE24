package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import sg.edu.nus.iss.se24.pt1.service.UserService;
import sg.edu.nus.iss.se24.pt1.service.UserServiceI;

/**
 * @author jinxiang
 *
 */
public class LoginDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1989532294236501714L;
	private final JPanel contentPanel = new JPanel();
	private JTextField usernameField;
	private JPasswordField passwordField;
	private StoreFrame parent;
	private UserServiceI userService;

	/**
	 * Create the dialog.
	 */
	public LoginDialog() {
		this(null, null);
	}

	/**
	 * Create the dialog.
	 */
	public LoginDialog(final StoreFrame frame, final String title) {
		super(frame, title, true);
		this.userService = new UserService();
		this.parent = frame;
		setResizable(false);
		setBounds(100, 100, 402, 163);
		setPreferredSize(new Dimension(400,150));
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel userNamePanel = new JPanel();
			contentPanel.add(userNamePanel);
			{
				JLabel lblNewLabel = new JLabel(Constants.STRING_LABEL_USERNAME);
				userNamePanel.add(lblNewLabel);
			}
			{
				usernameField = new JTextField();
				userNamePanel.add(usernameField);
				usernameField.setColumns(20);
			}
			contentPanel.add(userNamePanel);
		}
		{
			JPanel passwordPanel = new JPanel();
			contentPanel.add(passwordPanel);
			{
				JLabel lblNewLabel_1 = new JLabel(Constants.STRING_LABEL_PASSWORD);
				passwordPanel.add(lblNewLabel_1);
			}
			{
				passwordField = new JPasswordField();
				passwordField.setColumns(20);
				passwordPanel.add(passwordField);
			}
			contentPanel.add(passwordPanel);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton(Constants.BUTTON_LOGIN_LABEL);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						login();
					}
				});
				okButton.setActionCommand(Constants.BUTTON_LOGIN_LABEL);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton(Constants.BUTTON_CANCEL_LABEL);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						parent.closeLoginScreen();
					}
				});
				{
					JButton btnReset = new JButton(Constants.BUTTON_RESET_LABEL);
					btnReset.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							reset();
						}
					});
					buttonPane.add(btnReset);
				}
				cancelButton.setActionCommand(Constants.BUTTON_CANCEL_LABEL);
				buttonPane.add(cancelButton);
			}
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeLoginScreen();
			}
		});
		pack();
	}
	
	private void login() {
		boolean authenticated = userService.authenticate(usernameField.getText(),String.valueOf(passwordField.getPassword()));
		if (authenticated) {
			parent.setUser(usernameField.getText());
			this.dispose();
			parent.pack();
			parent.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(this,
				    Constants.MESSAGE_AUTHENTICATED_FAILED);
			reset();
		}
	}
	
	private void reset() {
		usernameField.setText(null);
		passwordField.setText(null);
		usernameField.requestFocus();
	}
}
