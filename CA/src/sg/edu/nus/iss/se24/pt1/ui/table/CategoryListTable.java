/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Category;

/**
 * @author jinxiang
 *
 */
public class CategoryListTable extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3111602267312702310L;

	private final List<Category> list;

	private final String[] columnNames = new String[] { "Category Code", "Category Name" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class };

	/**
	 * @param inventoryList
	 */
	public CategoryListTable(List<Category> list) {
		this.list = list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return list.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Category row = list.get(rowIndex);
		if (0 == columnIndex) {
			return row.getCategoryCode();
		} else if (1 == columnIndex) {
			return row.getCategoryName();
		}
		return null;
	}

	/**
	 * @return the list
	 */
	public List<Category> getList() {
		return list;
	}
}
