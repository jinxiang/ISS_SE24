/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Discount;
import sg.edu.nus.iss.se24.pt1.ui.Constants;

/**
 * @author Myo Wai Yan Kyaw
 * @author jinxiang
 *
 */
public class DiscountListTable extends AbstractTableModel {
	private final List<Discount> discountList;

	/**
	 * @param discountList
	 */
	public DiscountListTable(List<Discount> discountList) {
		this.discountList = discountList;
	}

	private final String[] columnNames = new String[] { "Discount Code", "Description", "Start Date",
			"Period of Discount", "Percentage of Discount", "Member or All" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class, String.class, String.class, Integer.class,
			String.class };

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111602267312702310L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return discountList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Discount row = discountList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getDiscountCode();
		} else if (1 == columnIndex) {
			return row.getDescription();
		} else if (2 == columnIndex) {
			return row.getStartDate();
		} else if (3 == columnIndex) {
			return row.getPeriodOfDiscount();
		} else if (4 == columnIndex) {
			return row.getPercentageDiscount();
		} else if (5 == columnIndex) {
			if (row.getMemberFlg() == 'M')
				return Constants.STRING_LABEL_DISCOUNT_MEMBER;
			else
				return Constants.STRING_LABEL_DISCOUNT_ALL;
		}
		return null;
	}

	/**
	 * @return the discountList
	 */
	public List<Discount> getList() {
		return discountList;
	}
}
