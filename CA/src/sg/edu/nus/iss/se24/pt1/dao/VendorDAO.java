package sg.edu.nus.iss.se24.pt1.dao;
//Created by Nilar 19.March.2016

import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import sg.edu.nus.iss.se24.pt1.model.Vendor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface VendorDAO  {

	HashMap<String, ArrayList<Vendor>> findAll();
	List<Vendor> findByCategory(String category);
	HashMap<String,Vendor> findByName(String name);
	boolean insertVendor(String category, Vendor vendor) throws VendorExistedException;
	boolean updateVendor(String category, Vendor vendor) throws VendorNotExistException;
	boolean deleteVendor(String category, String vendorName) throws VendorNotExistException;
	void commit();
}
