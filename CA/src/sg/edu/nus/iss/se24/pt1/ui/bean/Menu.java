/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.bean;

/**
 * @author jinxiang
 *
 */
public enum Menu {
	ADMIN("Administartion Setup"), CHECK_OUT("Check Out"), INVENTORY_LIST("Inventory List"), REPORTING("Reporting"), LOGOUT("Log Off");

	private String label;

	private Menu(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
}