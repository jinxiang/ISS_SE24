package sg.edu.nus.iss.se24.pt1.dao;

/**
 * Created by Shin on 3/12/2016.
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Killu on 3/12/2016.
 */
public class Transformer {

    public static Integer stringToInteger(String value) {
        return Integer.parseInt(value.trim());
    }

    public static Double stringToDouble(String value) {
        return Double.valueOf(value.trim());
    }

    public static Float stringToFloat(String value) {
        return Float.valueOf(value.trim());
    }
    
    public static Character stringToCharacter(String value) {
    	return Character.valueOf(value.trim().charAt(0));
    }

    public static Long stringToLong(String value) {
        return Long.valueOf(value.trim());
    }

    public static Date stringToDate(String value) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(value.trim());
    }

    public static String dateToString(Date value) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(value);
    }

}

