package sg.edu.nus.iss.se24.pt1.service;

import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.dao.CategoryDAO;
import sg.edu.nus.iss.se24.pt1.dao.CategoryDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Category;

/**
 * created by Nilar
 * @author jinxiang
 *
 */
public class CategoryService implements CategoryServiceI {

	CategoryDAO categoryDAO;

	public CategoryService() {
		categoryDAO = CategoryDAOImpl.getInstance();
	}

	@Override
	public boolean add(String categoryCode, String categoryName) throws ValidationException,CategoryCodeExistedException {
		validateCategory(categoryCode, categoryName);
		boolean getResult=categoryDAO.insertCategory(new Category(categoryCode, categoryName));
		if (getResult){
			categoryDAO.commit();
		}
		return getResult;
	}

	private void validateCategory(String categoryCode, String categoryName) throws ValidationException {
		if (categoryCode == null || categoryCode.trim().length() == 0)
			throw new ValidationException("Category", "Category Code Is Empty.");
		if (categoryName == null || categoryName.trim().length() == 0)
			throw new ValidationException("Category", "Category Name Is Empty.");
		if (categoryCode.trim().length() != 3) {
			throw new ValidationException("Category", "Category Code must be 3 letters");
		}
		//if(txtCategoryCode.getText().matches("\\d+"))
		if(categoryCode.matches("\\d+")){
			throw new ValidationException("Category", "Category Code must not be numeric value");
		}
		if(categoryName.matches("\\d+")){
			throw new ValidationException("Category", "Category name must not be numeric value");
		}
	}

	@Override
	public boolean edit(String categoryCode, String categoryName) {
		
		boolean getResult= categoryDAO.updateCategory(new Category(categoryCode, categoryName));
		if(getResult){
			categoryDAO.commit();
		}
		return getResult;
	}

	@Override
	public boolean remove(String categoryCode) {
		categoryDAO.deleteCategory(categoryCode);
		// if category is deleted, all the products related with that category
		// will be deleted
		ProductServiceI productservice = new ProductService();
		if (!productservice.removeProductByCategory(categoryCode)) {
			return false;
		}

		return true;
	}

	@Override
	public ArrayList<Category> getAll() {
		ArrayList<Category> ttest = (ArrayList<Category>) categoryDAO.findAll();
		for (Category tt : ttest) {
			// tt.addProductToCategory(p);
			// here need to call product service to get the products
		}
		return (ArrayList<Category>) categoryDAO.findAll();
	}

	@Override
	public String[] listCategoryCode() {
		List<Category> categoryList = getAll();
		String[] categoryCodeArr = new String[categoryList.size()];
		int count = 0;
		for (Category category : categoryList) {
			categoryCodeArr[count] = category.getCategoryCode();
			count++;
		}
		return categoryCodeArr;
	}

	@Override
	public boolean isValidCategoryCode(String categoryCode, String categoryName)  {
		if (categoryName.substring(0, 3).equals(categoryCode)) {
			return true;
		}
		
		return false;

	}
	
	@Override
	public String[] listCategoryCodeWithDefault(){
		List<Category> categoryList = getAll();
		String[] categoryCodeArr = new String[categoryList.size()+1];
		categoryCodeArr[0]="All";
		int count = 1;
		for (Category category : categoryList) {
			categoryCodeArr[count] = category.getCategoryCode();
			count++;
		}
		return categoryCodeArr;
	}
	
	
}
