package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.model.Transaction;
import sg.edu.nus.iss.se24.pt1.service.CheckOutServiceI;
import sg.edu.nus.iss.se24.pt1.service.ProductService;
import sg.edu.nus.iss.se24.pt1.service.ProductServiceI;
import sg.edu.nus.iss.se24.pt1.ui.Constants;
import sg.edu.nus.iss.se24.pt1.ui.StoreFrame;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.security.Provider.Service;
import java.text.NumberFormat;
import java.awt.event.ActionEvent;
import javax.swing.event.*;

public class NewTransactionDialog extends JDialog  {

	private final JPanel contentPanel = new JPanel();
	private final StoreFrame parent;
	private JTextField txtProductID;
	//private JFormattedTextField txtQty;
	private JTextField txtQty;
	private CheckOutServiceI service;
	private Transaction transaction;
	JLabel lblError;
	private ProductService productService;

	/**
	 * Create the dialog.
	 */
	public NewTransactionDialog(StoreFrame frame, String title, CheckOutServiceI service) {
		super(frame, title, true);
		parent = frame;
		this.service = service;
		this.productService = new ProductService();
		this.transaction = null;
		
		setPreferredSize(Constants.APP_DIMENSION);
		setBounds(100, 100, 400, 170);
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(2, 2, 0, 0));
		{
			JLabel lblNewLabel_1 = new JLabel("Product Barcode");
			contentPanel.add(lblNewLabel_1);
		}
		{
			txtProductID = new JTextField();
			contentPanel.add(txtProductID);
			txtProductID.setColumns(10);
		}
		{
			JLabel lblNewLabel_2 = new JLabel("Qty");
			contentPanel.add(lblNewLabel_2);
		}
		{
			NumberFormat numberFormat = NumberFormat.getNumberInstance();
			//txtQty = new JFormattedTextField(numberFormat);
			txtQty=new JTextField();
			txtQty.setColumns(10);
			txtQty.setText("0");
			contentPanel.add(txtQty);
			txtQty.setColumns(10);
		}
		{
			
		}
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					add();
				}
			});
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancel();
				}
			});
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.NORTH);
			{
				lblError = new JLabel("");
				lblError.setForeground(Color.red);
				panel.add(lblError);
			}
		} 
		
	}
	
	private void add(){
		lblError.setText("");
		if(isValidAdding()){
						
			transaction = service.addLine(this.txtProductID.getText().trim(), Integer.parseInt(this.txtQty.getText().trim()));
			this.setVisible(false);
		}
	}
	
	private void cancel(){
		this.setVisible(false);
		dispose();
	}
	
	public Transaction showDialog(){
		this.setVisible(true);
		return transaction;
	}
	
	private boolean isValidAdding(){
		
		//will update validation here
		if (txtProductID.getText().equals("")|| txtProductID.getText().trim().length()==0 ){
			lblError.setText("Please enter bar code for the product");
			return false;
		}
			
		
		Product product = productService.findByBarCode(this.txtProductID.getText().trim());
		if(product == null){
			lblError.setText("Entered product code does not exist in the system");
			return false;
		}
		
		if (txtQty.getText().equals("")){
			lblError.setText("Please enter qty for product");
			return false;
		}
		if(!txtQty.getText().matches("\\d+")){
			lblError.setText("Please enter numeric value for Qty");
			return false;
		}
		
		if (Integer.parseInt(txtQty.getText())<=0){
			lblError.setText("Please enter valid quantity for Qty");
			return false;
		}
		
		if(Integer.parseInt(txtQty.getText())>product.getAvailableQty()){
			lblError.setText("<html>Not enough in stock for this prodcut.<br> Current available Qty is " + product.getAvailableQty().toString() + "</html> ");
			return false;
		}
		
		if (service.isProductAlreadyAdded(txtProductID.getText().trim())){
			lblError.setText("<html>Product is already added.<br> if you want to update, you can directly update in grid</html>");
			return false;
		}
		return true;
	}
	
	
}
