package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import sg.edu.nus.iss.se24.pt1.model.Transaction;
import sg.edu.nus.iss.se24.pt1.service.CheckOutService;
import sg.edu.nus.iss.se24.pt1.service.CheckOutServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.CheckOutTable;

import javax.swing.JTable;
import java.awt.GridLayout;   
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

//Created by Nguwar

public class CheckOutDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -509919958605787622L;
	private final JPanel contentPanel = new JPanel();
	private final StoreFrame parent;
	private JTextField txtTenderedAmount;
	private JTable table;
	private JLabel lblMemberName;
	private CheckOutTable tableModel;
	private JLabel lblPointToRedeem;
	private JButton btnRedeem;
	private JLabel lblDiscountAmount;
	private JLabel lblTotal;
	private JLabel lblTendered;
	private JLabel lblDiscount;
	private JLabel lblRedeemed;
	private JLabel lblequivalent;
	private JLabel lblBalance; 
	private JLabel lblChange;
	
	private CheckOutServiceI service;
	
	/**
	 * Create the dialog.
	 */
	
	public CheckOutDialog() {
		this(null, null);
	}
	public CheckOutDialog(StoreFrame frame, String title) {
		super(frame, title, true);
		parent = frame;
		service = new CheckOutService();
		setPreferredSize(Constants.APP_DIMENSION);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(1, 2, 0, 0));
		
		JPanel panel = new JPanel();
		contentPanel.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {0};
		gbl_panel.rowHeights = new int[] {30, 150, 150};
		gbl_panel.columnWeights = new double[]{1.0};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0};
		panel.setLayout(gbl_panel);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.EAST;
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.VERTICAL;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel.add(panel_2, gbc_panel_2);
		
		JButton btnAdd = new JButton("+");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addLine();
			}
		});
		panel_2.add(btnAdd);
		
		JButton btnRemove = new JButton("-");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteLine();
			}
		});
		panel_2.add(btnRemove);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 1;
		panel.add(panel_3, gbc_panel_3);
		
		tableModel = new CheckOutTable(listTransaction());
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel_3.add(scrollPane);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 2;
		panel.add(panel_4, gbc_panel_4);
		panel_4.setLayout(new GridLayout(5, 4, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Total");
		panel_4.add(lblNewLabel);
		
		lblTotal = new JLabel("0.00");
		panel_4.add(lblTotal);
		
		JLabel lblNewLabel_13 = new JLabel("Tendered");
		panel_4.add(lblNewLabel_13);
		
		lblTendered = new JLabel("0.00");
		panel_4.add(lblTendered);
		
		JLabel lblNewLabel_2 = new JLabel("Discount");
		panel_4.add(lblNewLabel_2);
		
		lblDiscount = new JLabel("0 %");
		panel_4.add(lblDiscount);
		
		JLabel lblNewLabel_3 = new JLabel("Redeemed");
		panel_4.add(lblNewLabel_3);
		
		lblRedeemed = new JLabel("0 Pts");
		panel_4.add(lblRedeemed);
		
		JLabel lblNewLabel_5 = new JLabel("");
		panel_4.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("");
		panel_4.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("(equivalent)");
		panel_4.add(lblNewLabel_7);
		
		lblequivalent = new JLabel("0.00");
		panel_4.add(lblequivalent);
		
		JLabel lblNewLabel_9 = new JLabel("Balance");
		panel_4.add(lblNewLabel_9);
		
		lblBalance = new JLabel("0.00");
		panel_4.add(lblBalance);
		
		JLabel lblNewLabel_11 = new JLabel("Change");
		panel_4.add(lblNewLabel_11);
		
		lblChange = new JLabel("0.00");
		panel_4.add(lblChange);
		
		JLabel lblNewLabel_14 = new JLabel("");
		panel_4.add(lblNewLabel_14);
		
		JLabel lblNewLabel_17 = new JLabel("");
		panel_4.add(lblNewLabel_17);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panel_4.add(btnCancel);
		
		JButton btnPay = new JButton("Pay");
		btnPay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pay();
			}
		});
		panel_4.add(btnPay);
		
		JPanel panel_1 = new JPanel();
		contentPanel.add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] {0};
		gbl_panel_1.rowHeights = new int[] {50, 230, 40, 80};
		gbl_panel_1.columnWeights = new double[]{1.0};
		gbl_panel_1.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0};
		panel_1.setLayout(gbl_panel_1);
		
		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.anchor = GridBagConstraints.WEST;
		gbc_panel_6.insets = new Insets(0, 0, 5, 0);
		gbc_panel_6.fill = GridBagConstraints.VERTICAL;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 0;
		panel_1.add(panel_6, gbc_panel_6);
		
		JButton btnMember = new JButton("Member");
		btnMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateMember();
			}
		});
		panel_6.add(btnMember);
		
		lblMemberName = new JLabel("PUBLIC");
		panel_6.add(lblMemberName);
		
		JPanel panel_7 = new JPanel();
		
		JPanel panel_9 = new JPanel();
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.insets = new Insets(0, 0, 5, 0);
		gbc_panel_9.fill = GridBagConstraints.BOTH;
		gbc_panel_9.gridx = 0;
		gbc_panel_9.gridy = 1;
		panel_1.add(panel_9, gbc_panel_9);
		GridBagLayout gbl_panel_9 = new GridBagLayout();
		gbl_panel_9.columnWidths = new int[]{0, 0};
		gbl_panel_9.rowHeights = new int[] {70, 160};
		gbl_panel_9.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_9.rowWeights = new double[]{1.0, 1.0};
		panel_9.setLayout(gbl_panel_9);
		
		JPanel panel_11 = new JPanel();
		GridBagConstraints gbc_panel_11 = new GridBagConstraints();
		gbc_panel_11.insets = new Insets(0, 0, 5, 0);
		gbc_panel_11.fill = GridBagConstraints.BOTH;
		gbc_panel_11.gridx = 0;
		gbc_panel_11.gridy = 0;
		panel_9.add(panel_11, gbc_panel_11);
		
		JLabel lblNewLabel_4 = new JLabel("Tendered Amount");
		panel_11.add(lblNewLabel_4);
		
		txtTenderedAmount = new JTextField();
		txtTenderedAmount.setText("0");
		panel_11.add(txtTenderedAmount);
		txtTenderedAmount.setColumns(10);
		
		JPanel panel_10 = new JPanel();
		GridBagConstraints gbc_panel_10 = new GridBagConstraints();
		gbc_panel_10.insets = new Insets(0, 0, 5, 0);
		gbc_panel_10.fill = GridBagConstraints.BOTH;
		gbc_panel_10.gridx = 0;
		gbc_panel_10.gridy = 1;
		panel_9.add(panel_10, gbc_panel_10);
		panel_10.setLayout(new GridLayout(5, 0, 0, 0));
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("7");
			}
		});
		panel_10.add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("8");
			}
		});
		panel_10.add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("9");
			}
		});
		panel_10.add(btn9);
		
		JButton btn10 = new JButton("10");
		btn10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("10");
			}
		});
		panel_10.add(btn10);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("4");
			}
		});
		panel_10.add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("5");
			}
		});
		panel_10.add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("6");
			}
		});
		panel_10.add(btn6);
		
		JButton btn20 = new JButton("20");
		btn20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("20");
			}
		});
		panel_10.add(btn20);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("1");
			}
		});
		panel_10.add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("2");
			}
		});
		panel_10.add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("3");
			}
		});
		panel_10.add(btn3);
		
		JButton btn50 = new JButton("50");
		btn50.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("50");
			}
		});
		panel_10.add(btn50);
		
		JButton btnClear = new JButton("C");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("C");
			}
		});
		panel_10.add(btnClear);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("0");
			}
		});
		panel_10.add(btn0);
		
		JButton btnDecimalPoint = new JButton(".");
		btnDecimalPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad(".");
			}
		});
		panel_10.add(btnDecimalPoint);
		
		JButton btnlessThan = new JButton("<");
		btnlessThan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("<");
			}
		});
		panel_10.add(btnlessThan);
		
		
		JPanel panel_8 = new JPanel();
		GridBagConstraints gbc_panel_8 = new GridBagConstraints();
		gbc_panel_8.anchor = GridBagConstraints.EAST;
		gbc_panel_8.insets = new Insets(0, 0, 5, 0);
		gbc_panel_8.fill = GridBagConstraints.VERTICAL;
		gbc_panel_8.gridx = 0;
		gbc_panel_8.gridy = 2;
		panel_1.add(panel_8, gbc_panel_8);
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				done();
			}
		});
		panel_8.add(btnDone);
		
		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 3;
		panel_1.add(panel_5, gbc_panel_5);
		panel_5.setLayout(new GridLayout(2, 0, 0, 0));
		
		JPanel panel_12 = new JPanel();
		panel_5.add(panel_12);
		
		JLabel lblNewLabel_10 = new JLabel("Point To Redeem: ");
		panel_12.add(lblNewLabel_10);
		
		lblPointToRedeem = new JLabel("0 Pts");
		
		lblPointToRedeem.setSize(40, 20);
		panel_12.add(lblPointToRedeem);
		
		btnRedeem = new JButton("Redeem");
		btnRedeem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				redeemPoint();
			}
		});
		panel_12.add(btnRedeem);
		
		lblDiscountAmount = new JLabel("");
		panel_5.add(lblDiscountAmount);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeCheckoutScreen();
			}
		});
		pack();
	}
	
	protected void close() {
		parent.closeCheckoutScreen();
	}
	private List<Transaction> listTransaction() {
		return new ArrayList<Transaction>();
	}
	
	private void addLine(){
		//Call dialog to enter New Transaction
		NewTransactionDialog dialog = new NewTransactionDialog(parent, "Enter Product ID", service);
		Transaction transaction = dialog.showDialog();
		
		if(transaction != null){
			int selectedRow = (table.getSelectedRow() < 0)? 0 : table.getSelectedRow();
			tableModel.addRow(selectedRow, transaction);
		}
		
		refresh();
	}
	
	private void editLine(Transaction t, Integer Quantity){
		if(table.getSelectedRow() != -1){
			Transaction transaction = service.editLine(t, Quantity);
			tableModel.updateRow(table.getSelectedRow(), transaction);
			refresh();
		}
	}
	
	private void deleteLine(){
		if(table.getSelectedRow() != -1){
			int dialogResult = JOptionPane.showConfirmDialog(null, Constants.STRING_MESSAGE_DELETE_MEMEBER);
			if(dialogResult == JOptionPane.YES_OPTION){
				
				tableModel.deleteRow(table.getSelectedRow());
				service.deleteLine((Transaction) tableModel.getRow(table.getSelectedRow()));
				refresh();
			}
		}
	}
	
	private void updateMember(){
		//Call dialog to enter member ID
		ReadMemberIDDialog dialog = new ReadMemberIDDialog(parent, "Enter Member ID", service);
		dialog.showDialog();
		
		refresh();
	}
	
	private void redeemPoint(){
		if(service.getAvailablePointToRedeem() > 0){
			service.redeemPoint();
			this.refresh();
		}
	}
	
	private void done(){
		
		if(this.txtTenderedAmount.getText().charAt(this.txtTenderedAmount.getText().length() - 1) == '.')
			this.txtTenderedAmount.setText(this.txtTenderedAmount.getText() + "0");
		
		if(isValidAmount()){
			service.setTenderedAmount(Double.parseDouble(this.txtTenderedAmount.getText()));
			this.refresh();
		}
	}
	
	
	private boolean isValidAmount(){
		return true;
	}
	
	private void KeyPad(String key){
		String value = this.txtTenderedAmount.getText();
		
		switch(key){
			case "0":  
			case "1":
			case "2": 
			case "3": 
			case "4": 
			case "5": 
			case "6": 
			case "7":
			case "8": 
			case "9": 
				if(value.indexOf(".") != -1){
					value += key;
				}else if(Double.parseDouble(value) == 0){
					value = key;
				}
				else{
					value += key;
				}
				break;
			case "10": 
			case "20": 
			case "50": 
				if(value.indexOf(".") != -1){
					String[] arr = value.split("\\.");
					Integer tempvalue =  Integer.parseInt(arr[0]) + Integer.parseInt(key);
					arr[0] = tempvalue.toString();
					
					value = arr[0];
					if(arr.length == 2)
						value += "."+arr[1];
				}else{
					Integer tempvalue =  Integer.parseInt(value) + Integer.parseInt(key);
					value = tempvalue.toString();
				}
				
				break;
			case ".":
				if(value.indexOf(".") == -1){
					value += key;
				}
				break;
			case "C":
				value = "0";
				break;
			case "<":
				if(value.indexOf(".") != -1){
					value = value.substring(0, value.length() - 1);
				}else if(Double.parseDouble(value) != 0){
					if(value.length() == 1)
						value = "0";
					else
						value = value.substring(0, value.length() - 1);
				}
				break;
			default:break;
		}
		
		this.txtTenderedAmount.setText(value);
	}
	
	private void pay(){
		//service.pay();
		this.showInventoryBelowThresholdLevel();
		this.clear();
	}
	
	private void refresh(){
		String memberID = (service.getMember() == null)? "PUBLIC" : service.getMember().getMemberName();
		
		this.lblMemberName.setText(memberID);
		this.lblPointToRedeem.setText(service.getAvailablePointToRedeem().toString() + " Pts");
		
		this.lblTotal.setText(service.getTotalAmount().toString());
		this.lblDiscount.setText(service.getDiscountPercentage().toString() + " %");
		this.lblBalance.setText(service.getBalance().toString());
		this.lblTendered.setText(service.getTenderedAmount().toString());
		this.lblRedeemed.setText(service.getRedeemedPoint().toString() + " Pts");
		this.lblequivalent.setText(service.getRedeemAmount().toString());
		this.lblChange.setText(service.getChange().toString());
	}

	private void showInventoryBelowThresholdLevel(){
		
	}
	
	private void clear(){
		service = new CheckOutService();
		tableModel.clear();
		this.txtTenderedAmount.setText("0");
		
		this.refresh();
	}
}
