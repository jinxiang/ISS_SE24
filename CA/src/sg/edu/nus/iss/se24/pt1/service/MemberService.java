package sg.edu.nus.iss.se24.pt1.service;

import java.util.*;

import sg.edu.nus.iss.se24.pt1.dao.MemberDAO;
import sg.edu.nus.iss.se24.pt1.dao.MemberDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Member;

//Created by Nguwar

public class MemberService implements MemberServiceI {
	
	private MemberDAO database;

	public MemberService() {
		database = MemberDAOImpl.getInstance();
	}

	@Override
	public List<Member> getAll() {
		return database.findAll();
	}

	@Override
	public Member getByMemberID(String memberID) {
		return database.findByID(memberID);
	}

	@Override
	public boolean add(String memberID, String memberName, String loyaltyPoint) throws ValidationException {
		validateMember(memberID, memberName, loyaltyPoint);
		Member memberToInsert = new Member(memberName, memberID);
		boolean getResult = database.insertMember(memberToInsert);
		if(getResult)
			database.commit();
		return getResult;
	}

	@Override
	public boolean edit(String memberID, String memberName, String loyaltyPoint) throws ValidationException {
		validateMember(memberID, memberName, loyaltyPoint);
		Member memberToUpdate = new Member(memberName, memberID, Integer.parseInt(loyaltyPoint));
		boolean getResult = database.updateMember(memberToUpdate);
		if(getResult)
			database.commit();
		return getResult;
	}

	@Override
	public boolean delete(String memberID) {
		boolean getResult = database.deleteMember(memberID);
		if(getResult)
			database.commit();
		return getResult;
	}
	
	@Override
	public boolean isDuplicate(String MemberID){
		Member member = database.findByID(MemberID);
		return (member == null)? false : true;
	}
	
	private void validateMember(String memberID, String memberName, String point) throws ValidationException {
		if (memberID == null || memberID.trim().length() == 0)
			throw new ValidationException("Member", "Member ID Is Empty.");
		if (memberName == null || memberName.trim().length() == 0)
			throw new ValidationException("Member", "Member Name Is Empty.");
		if (point == null || point.trim().length() == 0)
			throw new ValidationException("Member", "Point Is Empty.");
		
		try {
			Integer.parseInt(point);
		} catch (NumberFormatException e) {
			throw new ValidationException("Member", "Point must be valid numeric value");
		}
		
		//assumption : that memberID should be FIN number or student card ID, and always includes at least one Char
		if(memberID.matches("\\d+")){
			throw new ValidationException("Member", "Member ID must not be only numeric value");
		}
		if(memberName.matches("\\d+")){
			throw new ValidationException("Member", "Member name must not be numeric value");
		}
		
	}

}
