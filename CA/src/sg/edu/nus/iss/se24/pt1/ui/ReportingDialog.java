package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

import sg.edu.nus.iss.se24.pt1.dao.Transformer;
import sg.edu.nus.iss.se24.pt1.model.Category;
import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.model.Product;
import sg.edu.nus.iss.se24.pt1.model.Transaction;
import sg.edu.nus.iss.se24.pt1.service.CategoryService;
import sg.edu.nus.iss.se24.pt1.service.MemberService;
import sg.edu.nus.iss.se24.pt1.service.ProductService;
import sg.edu.nus.iss.se24.pt1.ui.table.CategoryListTable;
import sg.edu.nus.iss.se24.pt1.ui.table.MemberListTable;
import sg.edu.nus.iss.se24.pt1.ui.table.ProductTable;
import sg.edu.nus.iss.se24.pt1.ui.table.TransactionReportTable;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

/**
 * @author jinxiang
 *
 */
public class ReportingDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable table;

	private StoreFrame parent;
	private JLabel lblReportHeader;

	/**
	 * Create the dialog.
	 */
	public ReportingDialog() {
		/**
		 * Create the dialog.
		 */
		this(null, null);
	}

	public ReportingDialog(final StoreFrame frame, String title) {
		super(frame, title, true);
		this.parent = frame;
		setResizable(false);
		setSize(Constants.APP_DIMENSION);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			lblReportHeader = new JLabel();
			lblReportHeader.setFont(lblReportHeader.getFont().deriveFont(lblReportHeader.getFont().getStyle() | Font.BOLD, 16f));
			lblReportHeader.setForeground(new Color(0, 0, 204));
			lblReportHeader.setHorizontalAlignment(SwingConstants.CENTER);
			contentPanel.add(lblReportHeader, BorderLayout.NORTH);
		}
		{
			table = new JTable();
			JScrollPane scrollPane = new JScrollPane(table);
			scrollPane.setPreferredSize(Constants.APP_DIMENSION);
			contentPanel.add(scrollPane);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnNewButton = new JButton(Constants.STRING_LABEL_LIST_MEMBERS);
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generateMemberReport();
					}
				});
				buttonPane.add(btnNewButton);
			}
			{
				JButton btnNewButton_1 = new JButton(Constants.STRING_LABEL_LIST_TRANSACTION);
				btnNewButton_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generateTransactionReport();
					}
				});
				buttonPane.add(btnNewButton_1);
			}
			{
				JButton btnNewButton_2 = new JButton(Constants.STRING_LABEL_LIST_PRODUCT);
				btnNewButton_2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generateProductReport();
					}
				});
				buttonPane.add(btnNewButton_2);
			}
			{
				JButton btnNewButton_3 = new JButton(Constants.STRING_LABEL_LIST_CATEGORY);
				btnNewButton_3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generateCategoryReport();
					}
				});
				buttonPane.add(btnNewButton_3);
			}
			{
				JButton cancelButton = new JButton(Constants.BUTTON_CANCEL_LABEL);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						parent.closeReportingScreen();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeReportingScreen();
			}
		});
		pack();
		generateMemberReport();
	}

	protected void generateCategoryReport() {
		lblReportHeader.setText(Constants.STRING_LABEL_LIST_CATEGORY);
		List<Category> list = new CategoryService().getAll();
		CategoryListTable tableModel = new CategoryListTable(list);
		table.setModel(tableModel);
		validate();
	}

	protected void generateProductReport() {
		lblReportHeader.setText(Constants.STRING_LABEL_LIST_PRODUCT);
		List<Product> list = new ProductService().getAll();
		ProductTable tableModel = new ProductTable(list);
		table.setModel(tableModel);
		DefaultTableCellRenderer centreRenderer = new DefaultTableCellRenderer();
		centreRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(5).setCellRenderer(centreRenderer);
		validate();
	}

	protected void generateMemberReport() {
		lblReportHeader.setText(Constants.STRING_LABEL_LIST_MEMBERS);
		List<Member> list = new MemberService().getAll();
		MemberListTable tableModel = new MemberListTable(list);
		table.setModel(tableModel);
		validate();
	}

	protected void generateTransactionReport() {
		lblReportHeader.setText(Constants.STRING_LABEL_LIST_TRANSACTION);
		String transactionDateInput = JOptionPane
				.showInputDialog("Please input the transaction date in (yyyy-mm-dd) format");
		Date transactionDate = null;
		if (transactionDateInput != null) {
			try {
				transactionDate = Transformer.stringToDate(transactionDateInput);
			} catch (ParseException e) {
				JOptionPane.showMessageDialog(this, "Wrong Date Format", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		List<Transaction> list = new ProductService().getTransactionList(transactionDate);
		TransactionReportTable tableModel = new TransactionReportTable(list);
		table.setModel(tableModel);
		DefaultTableCellRenderer centreRenderer = new DefaultTableCellRenderer();
		centreRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(centreRenderer);
		table.getColumnModel().getColumn(5).setCellRenderer(centreRenderer);
		validate();
	}

}
