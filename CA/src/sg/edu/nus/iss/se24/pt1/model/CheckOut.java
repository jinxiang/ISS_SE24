package sg.edu.nus.iss.se24.pt1.model;

import java.util.ArrayList;

public class CheckOut {

	public ArrayList<Transaction> list;
	public Member member;
	public String memberID;
	public Double totalAmount;
	public Integer discountPercentage;
	public Double discountAmount;
	public Double redeemAmount;
	public Integer pointToRedeem;
	public Integer redeemedPoint;
	public Double tenderedAmount;
	public Double balance;
	public Double change;
	
	public CheckOut() {
		list = new ArrayList<Transaction>();
		member = null;
		memberID = "PUBLIC";
		totalAmount = 0.00;
		redeemAmount = 0.00;
		pointToRedeem = 0;
		redeemedPoint = 0;
		discountPercentage = 0;
		discountAmount = 0.00;
		tenderedAmount = 0.00;
		balance = 0.00;
		change = 0.00;
	}

}
