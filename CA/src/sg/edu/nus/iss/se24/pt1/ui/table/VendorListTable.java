/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Vendor;

//Created by Nguwar

public class VendorListTable extends AbstractTableModel {
	private final List<Vendor> vendorList;

	/**
	 * @param vendorList
	 */
	public VendorListTable(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}

	private final String[] columnNames = new String[] { "Vendor Name", "Description" };

	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] { String.class, String.class };

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return vendorList == null ? 0 : vendorList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Vendor row = vendorList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getVendorName();
		} else if (1 == columnIndex) {
			return row.getDescription();
		}
		return null;
	}

	/**
	 * @return the vendorList
	 */
	public List<Vendor> getList() {
		return vendorList;
	}
}
