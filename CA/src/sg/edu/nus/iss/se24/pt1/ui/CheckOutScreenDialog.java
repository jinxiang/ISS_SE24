package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Transaction;
import sg.edu.nus.iss.se24.pt1.service.CheckOutService;
import sg.edu.nus.iss.se24.pt1.service.CheckOutServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.CheckOutTable;

import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.event.*;
import java.awt.SystemColor;

//Created by Nguwar

public class CheckOutScreenDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -509919958605787622L;
	private final JPanel contentPanel = new JPanel();
	private final StoreFrame parent;
	private JTextField txtTenderedAmount;
	private JTable table;
	private JLabel lblMemberName;
	private CheckOutTable tableModel;
	private JLabel lblPointToRedeem;
	private JButton btnRedeem;
	private JLabel lblDiscountAmount;
	private JLabel lblTotal;
	private JLabel lblTendered;
	private JLabel lblDiscount;
	private JLabel lblRedeemed;
	private JLabel lblequivalent;
	private JLabel lblBalance; 
	private JLabel lblChange;
	
	private CheckOutServiceI service;
	
	/**
	 * Create the dialog.
	 */
	
	public CheckOutScreenDialog() {
		this(null, null);
	}
	public CheckOutScreenDialog(StoreFrame frame, String title) {
		super(frame, title, true);
		parent = frame;
		service = new CheckOutService();
		setPreferredSize(Constants.APP_DIMENSION);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(1, 2, 0, 0));
		
		JPanel panel = new JPanel();
		contentPanel.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {0};
		gbl_panel.rowHeights = new int[] {30, 150, 130, 20};
		gbl_panel.columnWeights = new double[]{1.0};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0};
		panel.setLayout(gbl_panel);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.anchor = GridBagConstraints.EAST;
		gbc_panel_2.fill = GridBagConstraints.VERTICAL;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel.add(panel_2, gbc_panel_2);
		
		JButton btnAdd = new JButton("+");
		btnAdd.setOpaque(true);
		btnAdd.setBackground(SystemColor.controlShadow);
		btnAdd.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addLine();
			}
		});
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_2.add(btnAdd);
		
		JButton btnRemove = new JButton("-");
		btnRemove.setOpaque(true);
		btnRemove.setBackground(SystemColor.controlShadow);
		btnRemove.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteLine();
			}
		});
		panel_2.add(btnRemove);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 1;
		panel.add(panel_3, gbc_panel_3);
		
		tableModel = new CheckOutTable(listTransaction());
		tableModel.addTableModelListener(new TableModelListener() {
			
		      public void tableChanged(TableModelEvent e) {
		        editLine();
		      }
		 });
		
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		table = new JTable(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		panel_3.add(scrollPane);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 2;
		panel.add(panel_4, gbc_panel_4);
		panel_4.setLayout(new GridLayout(5, 4, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Total");
		panel_4.add(lblNewLabel);
		
		lblTotal = new JLabel("$0.00");
		panel_4.add(lblTotal);
		
		JLabel lblNewLabel_13 = new JLabel("Tendered");
		panel_4.add(lblNewLabel_13);
		
		lblTendered = new JLabel("$0.00");
		panel_4.add(lblTendered);
		
		JLabel lblNewLabel_2 = new JLabel("Discount");
		panel_4.add(lblNewLabel_2);
		
		lblDiscount = new JLabel("0 %");
		panel_4.add(lblDiscount);
		
		JLabel lblNewLabel_3 = new JLabel("Redeemed");
		panel_4.add(lblNewLabel_3);
		
		lblRedeemed = new JLabel("0 Pts");
		panel_4.add(lblRedeemed);
		
		JLabel lblNewLabel_5 = new JLabel("");
		panel_4.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("");
		panel_4.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("(equivalent)");
		panel_4.add(lblNewLabel_7);
		
		lblequivalent = new JLabel("$0.00");
		panel_4.add(lblequivalent);
		
		JLabel lblNewLabel_9 = new JLabel("Balance");
		lblNewLabel_9.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		panel_4.add(lblNewLabel_9);
		
		lblBalance = new JLabel("$0.00");
		lblBalance.setForeground(Color.RED);
		lblBalance.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		panel_4.add(lblBalance);
		
		JLabel lblNewLabel_11 = new JLabel("Change");
		lblNewLabel_11.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		panel_4.add(lblNewLabel_11);
		
		lblChange = new JLabel("$0.00");
		lblChange.setForeground(Color.BLUE);
		lblChange.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		panel_4.add(lblChange);
		
		JLabel lblNewLabel_14 = new JLabel("");
		panel_4.add(lblNewLabel_14);
		
		JLabel lblNewLabel_17 = new JLabel("");
		panel_4.add(lblNewLabel_17);
		
		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.anchor = GridBagConstraints.EAST;
		gbc_panel_7.fill = GridBagConstraints.VERTICAL;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 3;
		panel.add(panel_7, gbc_panel_7);
		
		JButton btnCancel = new JButton("Cancel");
		panel_7.add(btnCancel);
		btnCancel.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		
		JButton btnPay = new JButton("  Pay  ");
		panel_7.add(btnPay);
		btnPay.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnPay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				makePayment();
			}
		});
		
		JPanel panel_1 = new JPanel();
		contentPanel.add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] {0};
		gbl_panel_1.rowHeights = new int[] {30, 150, 150};
		gbl_panel_1.columnWeights = new double[]{1.0};
		gbl_panel_1.rowWeights = new double[]{0.0, 1.0, 0.0};
		panel_1.setLayout(gbl_panel_1);
		
		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.anchor = GridBagConstraints.WEST;
		gbc_panel_6.fill = GridBagConstraints.VERTICAL;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 0;
		panel_1.add(panel_6, gbc_panel_6);
		
		JButton btnMember = new JButton("Member");
		btnMember.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateMember();
			}
		});
		panel_6.add(btnMember);
		
		lblMemberName = new JLabel("PUBLIC");
		lblMemberName.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblMemberName.setForeground(new Color(0, 0, 255));
		panel_6.add(lblMemberName);
		
		JPanel panel_9 = new JPanel();
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.insets = new Insets(0, 0, 5, 0);
		gbc_panel_9.fill = GridBagConstraints.BOTH;
		gbc_panel_9.gridx = 0;
		gbc_panel_9.gridy = 1;
		panel_1.add(panel_9, gbc_panel_9);
		
		GridBagLayout gbl_panel_9 = new GridBagLayout();
		gbl_panel_9.columnWidths = new int[]{0, 0};
		gbl_panel_9.rowHeights = new int[] {70, 160};
		gbl_panel_9.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_9.rowWeights = new double[]{0.0, 1.0};
		panel_9.setLayout(gbl_panel_9);
		
		JPanel panel_11 = new JPanel();
		GridBagConstraints gbc_panel_11 = new GridBagConstraints();
		gbc_panel_11.anchor = GridBagConstraints.SOUTHEAST;
		gbc_panel_11.insets = new Insets(0, 0, 5, 0);
		gbc_panel_11.gridx = 0;
		gbc_panel_11.gridy = 0;
		panel_9.add(panel_11, gbc_panel_11);
		panel_11.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblNewLabel_4 = new JLabel("Tendered Amount");
		panel_11.add(lblNewLabel_4);
		
		txtTenderedAmount = new JTextField();
		txtTenderedAmount.setBackground(SystemColor.textHighlight);
		txtTenderedAmount.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		txtTenderedAmount.setText("0");
		txtTenderedAmount.setEnabled(false);
		panel_11.add(txtTenderedAmount);
		txtTenderedAmount.setColumns(10);
		
		JPanel panel_10 = new JPanel();
		GridBagConstraints gbc_panel_10 = new GridBagConstraints();
		gbc_panel_10.insets = new Insets(0, 0, 5, 0);
		gbc_panel_10.fill = GridBagConstraints.BOTH;
		gbc_panel_10.gridx = 0;
		gbc_panel_10.gridy = 1;
		panel_9.add(panel_10, gbc_panel_10);
		panel_10.setLayout(new GridLayout(5, 0, 0, 0));
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("7");
			}
		});
		panel_10.add(btn7);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("8");
			}
		});
		panel_10.add(btn8);
		
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("9");
			}
		});
		panel_10.add(btn9);
		
		JButton btn10 = new JButton("10");
		btn10.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		btn10.setOpaque(true);
		btn10.setBackground(Color.BLUE);
		btn10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("10");
			}
		});
		panel_10.add(btn10);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("4");
			}
		});
		panel_10.add(btn4);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("5");
			}
		});
		panel_10.add(btn5);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("6");
			}
		});
		panel_10.add(btn6);
		
		JButton btn20 = new JButton("20");
		btn20.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		btn20.setOpaque(true);
		btn20.setBackground(Color.BLUE);
		btn20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("20");
			}
		});
		panel_10.add(btn20);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("1");
			}
		});
		panel_10.add(btn1);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("2");
			}
		});
		panel_10.add(btn2);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("3");
			}
		});
		panel_10.add(btn3);
		
		JButton btn50 = new JButton("50");
		btn50.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		btn50.setOpaque(true);
		btn50.setBackground(Color.BLUE);
		btn50.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("50");
			}
		});
		panel_10.add(btn50);
		
		JButton btnClear = new JButton("C");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("C");
			}
		});
		panel_10.add(btnClear);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("0");
			}
		});
		panel_10.add(btn0);
		
		JButton btnDecimalPoint = new JButton(".");
		btnDecimalPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad(".");
			}
		});
		panel_10.add(btnDecimalPoint);
		
		JButton btnlessThan = new JButton("<");
		btnlessThan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KeyPad("<");
			}
		});
		panel_10.add(btnlessThan);
		
		JPanel panel_13 = new JPanel();
		panel_10.add(panel_13);
		
		JPanel panel_14 = new JPanel();
		panel_10.add(panel_14);
		
		JPanel panel_15 = new JPanel();
		panel_10.add(panel_15);
		
		JButton btnDone = new JButton("Done");
		btnDone.setBackground(new Color(0, 128, 128));
		panel_10.add(btnDone);
		btnDone.setFont(new Font("Lucida Grande", Font.BOLD, 20));
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				done();
			}
		});
		
		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 2;
		panel_1.add(panel_5, gbc_panel_5);
		panel_5.setLayout(new GridLayout(2, 0, 0, 0));
		
		JPanel panel_12 = new JPanel();
		panel_12.setBackground(Color.LIGHT_GRAY);
		FlowLayout flowLayout = (FlowLayout) panel_12.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel_5.add(panel_12);
		
		JLabel lblNewLabel_10 = new JLabel("Point To Redeem: ");
		panel_12.add(lblNewLabel_10);
		
		lblPointToRedeem = new JLabel("0 Pts");
		lblPointToRedeem.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblPointToRedeem.setForeground(Color.BLUE);
		
		lblPointToRedeem.setSize(40, 20);
		panel_12.add(lblPointToRedeem);
		
		btnRedeem = new JButton("Redeem");
		btnRedeem.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnRedeem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				redeemPoint();
			}
		});
		panel_12.add(btnRedeem);
		
		lblDiscountAmount = new JLabel("");
		panel_5.add(lblDiscountAmount);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				parent.closeCheckoutScreen();
			}
		});
		pack();
	}
	
	protected void close() {
		parent.closeCheckoutScreen();
	}
	private List<Transaction> listTransaction() {
		return new ArrayList<Transaction>();
	}
	
	private void addLine(){
		//Call dialog to enter New Transaction
		NewTransactionDialog dialog = new NewTransactionDialog(parent, "Enter Product Barcode", service);
		Transaction transaction = dialog.showDialog();
		
		if(transaction != null){
			int selectedRow = (table.getSelectedRow() < 0)? 0 : table.getSelectedRow();
			tableModel.addRow(selectedRow, transaction);
		}
		
		refresh();
	}
	
	private void editLine(){
		if(tableModel.getEditedRow() != -1){
			Transaction transactionToEdit = (Transaction) tableModel.getRow(tableModel.getEditedRow());
			Transaction transaction = service.editLine(transactionToEdit, transactionToEdit.getPurchasedQuantity());
			//tableModel.updateRow(table.getSelectedRow(), transaction);
		}
		
		refresh();
	}
	
	private void deleteLine(){
		if(table.getSelectedRow() != -1){
			int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to delete this product?");
			if(dialogResult == JOptionPane.YES_OPTION){
				
				service.deleteLine((Transaction) tableModel.getRow(table.getSelectedRow()));
				tableModel.deleteRow(table.getSelectedRow());
			}
		}
		
		refresh();
	}
	
	private void updateMember(){
		//Call dialog to enter member ID
		ReadMemberIDDialog dialog = new ReadMemberIDDialog(parent, "Enter Member ID", service);
		dialog.showDialog();
		refresh();
	}
	
	private void redeemPoint(){
		if(service.getAvailablePointToRedeem() > 0){
			service.redeemPoint();
		}
		
		this.refresh();
	}
	
	private void done(){
		
		if(this.txtTenderedAmount.getText().charAt(this.txtTenderedAmount.getText().length() - 1) == '.')
			this.txtTenderedAmount.setText(this.txtTenderedAmount.getText() + "0");
		
		if(isValidAmount()){
			service.setTenderedAmount(Double.parseDouble(this.txtTenderedAmount.getText()));
			this.txtTenderedAmount.setText("0");
		}
		
		this.refresh();
	}
	
	
	private boolean isValidAmount(){
		return true;
	}
	
	private void KeyPad(String key){
		String value = this.txtTenderedAmount.getText();
		
		switch(key){
			case "0":  
			case "1":
			case "2": 
			case "3": 
			case "4": 
			case "5": 
			case "6": 
			case "7":
			case "8": 
			case "9": 
				if(value.indexOf(".") != -1){
					value += key;
				}else if(Double.parseDouble(value) == 0){
					value = key;
				}
				else{
					value += key;
				}
				break;
			case "10": 
			case "20": 
			case "50": 
				if(value.indexOf(".") != -1){
					String[] arr = value.split("\\.");
					Integer tempvalue =  Integer.parseInt(arr[0]) + Integer.parseInt(key);
					arr[0] = tempvalue.toString();
					
					value = arr[0];
					if(arr.length == 2)
						value += "."+arr[1];
				}else{
					Integer tempvalue =  Integer.parseInt(value) + Integer.parseInt(key);
					value = tempvalue.toString();
				}
				
				break;
			case ".":
				if(value.indexOf(".") == -1){
					value += key;
				}
				break;
			case "C":
				value = "0";
				break;
			case "<":
				if(value.indexOf(".") != -1){
					value = value.substring(0, value.length() - 1);
				}else if(Double.parseDouble(value) != 0){
					if(value.length() == 1)
						value = "0";
					else
						value = value.substring(0, value.length() - 1);
				}
				break;
			default:break;
		}
		
		this.txtTenderedAmount.setText(value);
	}
	
	private void makePayment(){
		try {
			if(service.isValidateCheckOut()){
				
				int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to check out this transaction?");
				if(dialogResult == JOptionPane.YES_OPTION){
					
					if(service.makePayment()){
						
						int dialogResult2 = JOptionPane.showConfirmDialog(null, "Do you want to print out the receipt(console)?");
						if(dialogResult2 == JOptionPane.YES_OPTION){
							this.printReceipt();
						} 
						
						if(service.hasBelowThresholdLevel()){
							this.showInventoryBelowThresholdLevel();
						}
						
						this.clear();
					}
					
				}
			}
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void printReceipt(){
		service.printReceipt();
	}

	private void showInventoryBelowThresholdLevel(){
		InventoryDialog inventoryDialog = new InventoryDialog(parent, Constants.STRING_TITLE_INVENTORY_SCREEN, false);
		parent.setInventoryDialog(inventoryDialog);
		inventoryDialog.setLocationRelativeTo(null);
		inventoryDialog.setVisible(true);
	}
	
	private void clear(){
		service = new CheckOutService();
		tableModel.clear();
		this.txtTenderedAmount.setText("0");
		
		this.refresh();
	}
	
	private void refresh(){
		String memberID = (service.getMember() == null)? "PUBLIC" : service.getMember().getMemberName();
		
		this.lblMemberName.setText(memberID);
		this.lblPointToRedeem.setText(service.getAvailablePointToRedeem().toString() + " Pts");
		
		this.lblTotal.setText("$" + String.format("%.2f", service.getTotalAmount()));
		this.lblDiscount.setText("$" + service.getDiscountPercentage().toString() + " %");
		this.lblBalance.setText("$" + String.format("%.2f", service.getBalance()));
		this.lblTendered.setText("$" + String.format("%.2f", service.getTenderedAmount()));
		this.lblRedeemed.setText(service.getRedeemedPoint().toString() + " Pts");
		this.lblequivalent.setText("$" + String.format("%.2f", service.getRedeemAmount()));
		this.lblChange.setText("$" + String.format("%.2f", service.getChange()));
	}
}
