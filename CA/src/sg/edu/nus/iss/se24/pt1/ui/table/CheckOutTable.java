/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.ui.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import sg.edu.nus.iss.se24.pt1.model.Member;
import sg.edu.nus.iss.se24.pt1.model.Transaction;

//Created by Nguwar   

public class CheckOutTable extends AbstractTableModel {
	private final List<Transaction> transactionList;
	private Integer editedRowIndex;

	/**
	 * @param inventoryList
	 */
	public CheckOutTable(List<Transaction> transactionList) {
		this.transactionList = transactionList;
		editedRowIndex = -1;
	}

	private final String[] columnNames = new String[] { "Item", "Qty", "Unit Price", "Total"};
	
	@SuppressWarnings("rawtypes")
	final Class[] columnClass = new Class[] {String.class, Integer.class, Double.class, Double.class};

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111602267312702310L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return transactionList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	public Class<?> getColumnClass(int columnIndex)
    {
        return columnClass[columnIndex];
    }
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int col) {
        return columnNames[col];
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Transaction row = transactionList.get(rowIndex);
		if (0 == columnIndex) {
			return row.getProductID();
		} else if (1 == columnIndex) {
			return row.getPurchasedQuantity();
		} else if (2 == columnIndex) {
			if(row.getProduct() != null)
				return row.getProduct().getPrice();
			else 
				return 0.00;
		} else if (3 == columnIndex) {
			if(row.getProduct() != null)
				return row.getPurchasedQuantity() * row.getProduct().getPrice();
			else 
				return 0.00;
		}
		return null;
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
	     switch (col) {
	         case 0:
	         case 1:
	             return true;
	         case 2:
	             return false;
	         case 3:
	             return false;
	         default:
	             return false;
	      }
	}
	
	
	@Override
	public void setValueAt(Object value, int row, int col)
	{
	    Transaction transaction = transactionList.get(row);
	    
	    switch (col) {
	    	case 1:
	    		transaction.setPurchaseQuantity((Integer)value);
	    		this.editedRowIndex = row;
	    		break;
	    	default:
	    		break;
	    }
	    
	    this.fireTableDataChanged();
	}
	
	
	public Object getRow(int rowIndex){
		return transactionList.get(rowIndex);
	}
	
	public Integer getEditedRow(){
		return this.editedRowIndex;
	}
	
	public void addRow(int row, Transaction rowData) {
		this.editedRowIndex = -1;
		
		transactionList.add(row, rowData);
		fireTableRowsInserted(row, row);
	}

	public void deleteRow(int row) {
		this.editedRowIndex = -1;
		
		transactionList.remove(row);
		fireTableRowsDeleted(row, row);
	}

	public void updateRow(int row, Transaction rowData) {
		transactionList.set(row, rowData);
		fireTableRowsUpdated(row, row);
	}
	
	public void clear() {
		this.editedRowIndex = -1;
		
		transactionList.clear();
		this.fireTableDataChanged();
	}

    
}
