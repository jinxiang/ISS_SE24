package sg.edu.nus.iss.se24.pt1.model;

import java.util.List;

/**
 * created by Nilar
 *
 */

public class Vendor {
	
	private String vendorName;
	private String description;
	
	private List<Category> categoryList;
	
	public Vendor(){}
	
	public Vendor(String vendorName, String description){
		this.vendorName=vendorName;
		this.description=description;
	}
	
	public String getVendorName(){
		return this.vendorName;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public String toString(){
		return (this.getVendorName() + "," + this.getDescription());
	}

	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the categoryList
	 */
	public List<Category> getCategoryList() {
		return categoryList;
	}

	/**
	 * @param categoryList the categoryList to set
	 */
	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

}
