package sg.edu.nus.iss.se24.pt1.service;

import java.util.ArrayList;

import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Category;;

/**
 * created by Nilar
 * 
 * @author jinxiang
 *
 */
public interface CategoryServiceI {
	public boolean add(String categoryCode, String categoryName) throws ValidationException, CategoryCodeExistedException;

	public boolean edit(String categoryCode, String categoryName) throws ValidationException;

	public boolean remove(String categoryCode);

	// public boolean isDuplicate(String categoryCode);
	public ArrayList<Category> getAll();

	public boolean isValidCategoryCode(String categoryCode, String categoryName);

	public String[] listCategoryCode();
	
	public String[] listCategoryCodeWithDefault();


}
