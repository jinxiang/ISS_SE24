/**
 * 
 */
package sg.edu.nus.iss.se24.pt1.exception;

/**
 * @author jinxiang
 *
 */
public class CategoryCodeExistedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5186289332977741382L;
	private static final String ERROR_MESSAGE ="Category is alerady existed in system.";
    public CategoryCodeExistedException() {
        super(ERROR_MESSAGE);
    }

}
