package sg.edu.nus.iss.se24.pt1.model;

import java.util.ArrayList;
import java.util.List;

/**
 * created by Nilar 
 *
 */

public class Category {

	private String categoryCode;
	private String categoryName;
	
	private ArrayList<Product>listProduct;
	
	private List<Vendor> vendorList;
	
	public Category(){}
	
	public Category(String categoryCode, String categoryName){
		this.categoryCode=categoryCode;
		this.categoryName=categoryName;
	}
	
	public String getCategoryCode(){
		return this.categoryCode;
	}
	
	public String getCategoryName(){
		return this.categoryName;
	}
	
	public ArrayList<Product> getProductList(){
		return this.listProduct;
	}
	
	public void addProductToCategory(Product p){
		this.listProduct.add(p);
	}
	
	public String toString(){
		return (this.getCategoryCode() + "," + this.getCategoryName());
	}

	/**
	 * @return the vendorList
	 */
	public List<Vendor> getVendorList() {
		return vendorList;
	}

	/**
	 * @param vendorList the vendorList to set
	 */
	public void setVendorList(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}

}
