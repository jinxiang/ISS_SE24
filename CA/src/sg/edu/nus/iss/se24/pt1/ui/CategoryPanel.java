package sg.edu.nus.iss.se24.pt1.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Category;
import sg.edu.nus.iss.se24.pt1.service.CategoryService;
import sg.edu.nus.iss.se24.pt1.service.CategoryServiceI;
import sg.edu.nus.iss.se24.pt1.ui.table.CategoryListTable;

/**
 * 
 * @author jinxiang, Nilar
 *
 */
public class CategoryPanel extends JPanel {
	CategoryServiceI categoryService = new CategoryService();
	private AdminstrationDialog parentDialog;
	private JTextField txnCategoryCode;
	private JTextField txtCategoryName;
	private JTable table;
	private JButton btnAdd;
	private JButton btnClear;

	public CategoryPanel() {
		this(null);
	}

	public CategoryPanel(AdminstrationDialog dialog) {
		parentDialog = dialog;
		setLayout(new BorderLayout(0, 0));

		// Content Panel
		JPanel ContentPanel = new JPanel();
		add(ContentPanel, BorderLayout.CENTER);
		ContentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		JPanel ListPanel = new JPanel();
		ContentPanel.add(ListPanel);
		ListPanel.setLayout(new BorderLayout(0, 0));
		CategoryListTable tableModel = new CategoryListTable(listCategories());
		table = new JTable(tableModel);
		// table.setFillsViewportHeight(true);
		// table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane = new JScrollPane(table);
		// scrollPane.setViewportView(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		ListPanel.add(scrollPane, BorderLayout.CENTER);

		JPanel DetailPanel = new JPanel();
		ContentPanel.add(DetailPanel);
		GridBagLayout gbl_DetailPanel = new GridBagLayout();
		gbl_DetailPanel.columnWidths = new int[] {120, 250, 0};
		gbl_DetailPanel.rowHeights = new int[] {33, 33, 33, 0};
		gbl_DetailPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_DetailPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		DetailPanel.setLayout(gbl_DetailPanel);
		JLabel lblCategoryCode = new JLabel(Constants.STRING_LABEL_CATEGORY_CODE);
		GridBagConstraints gbc_lblMemberID = new GridBagConstraints();
		gbc_lblMemberID.fill = GridBagConstraints.BOTH;
		gbc_lblMemberID.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberID.gridx = 0;
		gbc_lblMemberID.gridy = 0;
		DetailPanel.add(lblCategoryCode, gbc_lblMemberID);

		txnCategoryCode = new JTextField();
		GridBagConstraints gbc_txtMemberID = new GridBagConstraints();
		gbc_txtMemberID.fill = GridBagConstraints.BOTH;
		gbc_txtMemberID.insets = new Insets(0, 0, 5, 0);
		gbc_txtMemberID.gridx = 1;
		gbc_txtMemberID.gridy = 0;
		DetailPanel.add(txnCategoryCode, gbc_txtMemberID);
		txnCategoryCode.setColumns(10);

		JLabel lblCategoryName = new JLabel(Constants.STRING_LABEL_CATEGORY_NAME);
		GridBagConstraints gbc_lblMemberName = new GridBagConstraints();
		gbc_lblMemberName.fill = GridBagConstraints.BOTH;
		gbc_lblMemberName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMemberName.gridx = 0;
		gbc_lblMemberName.gridy = 1;
		DetailPanel.add(lblCategoryName, gbc_lblMemberName);

		txtCategoryName = new JTextField();
		GridBagConstraints gbc_txtMemberName = new GridBagConstraints();
		gbc_txtMemberName.fill = GridBagConstraints.BOTH;
		gbc_txtMemberName.insets = new Insets(0, 0, 5, 0);
		gbc_txtMemberName.gridx = 1;
		gbc_txtMemberName.gridy = 1;
		DetailPanel.add(txtCategoryName, gbc_txtMemberName);
		txtCategoryName.setColumns(10);

		// Button Panel
		JPanel ButtonPanel = new JPanel();
		add(ButtonPanel, BorderLayout.SOUTH);
		ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		btnAdd = new JButton(Constants.BUTTON_ADD_LABEL);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCategory(tableModel);
			}
		});
		ButtonPanel.add(btnAdd);
		JButton btnEdit = new JButton(Constants.BUTTON_EDIT_LABEL);
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateCategory(tableModel);
			}
		});
		ButtonPanel.add(btnEdit);
		// JButton btnRemove = new JButton(Constants.BUTTON_DELETE_LABEL);
		// btnRemove.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// int dialogResult = JOptionPane.showConfirmDialog(null,
		// Constants.STRING_MESSAGE_DELETE_CATEGORY);
		// if(dialogResult == JOptionPane.YES_OPTION){
		// deleteCategory(tableModel);
		// }
		// }
		// });
		// ButtonPanel.add(btnRemove);

		btnClear = new JButton(Constants.BUTTON_CLEAR_LABEL);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearCategory();
			}
		});
		ButtonPanel.add(btnClear);

		JButton btnClose = new JButton(Constants.BUTTON_CLOSE_LABEL);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentDialog.close();
			}
		});
		ButtonPanel.add(btnClose);

		ListSelectionModel selectionModel = table.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectRow = table.getSelectedRow();
				if (selectRow != -1) {
					txnCategoryCode.setEnabled(false);
					btnAdd.setEnabled(false);
					setCategory(tableModel.getList().get(selectRow));
				} else {
					clearCategory();
				}
			}

		});
	}

	private List<Category> listCategories() {
		return categoryService.getAll();
	}

	private void setCategory(Category category) {
		txnCategoryCode.setText(category.getCategoryCode());
		txtCategoryName.setText(category.getCategoryName());
	}

	private Category getCategory() {
		return new Category(txnCategoryCode.getText().trim(), txtCategoryName.getText().trim());
	}

	private void addCategory(CategoryListTable tableModel) {
		boolean result = false;
		/*
		result = categoryService.isValidCategoryCode(txnCategoryCode.getText().trim(),
				txtCategoryName.getText().trim());
		if (!result) {
			JOptionPane.showMessageDialog(null,
					"Invalid category Code. Category code must be the first 3 character of categoryName.",
					"Error in adding new catgory", JOptionPane.ERROR_MESSAGE);
		} else {*/
		
			try {
				result = categoryService.add(txnCategoryCode.getText(), txtCategoryName.getText());
				if (result) {
					refreshCategoryList();
				} else {
					JOptionPane.showMessageDialog(null,"Error in adding new catgory into list view", Constants.STRING_INSERT_ERROR_Title, 
							JOptionPane.ERROR_MESSAGE);
				}
			} catch (ValidationException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(),e.getClazz(),JOptionPane.ERROR_MESSAGE);
			}catch(CategoryCodeExistedException e){
				JOptionPane.showMessageDialog(this,e.getMessage().toString(),Constants.STRING_INSERT_ERROR_Title,JOptionPane.ERROR_MESSAGE);
			//}
			
		}
	}

	private void updateCategory(CategoryListTable tableModel) {
		boolean result = false;
		try {
			result = categoryService.edit(txnCategoryCode.getText(), txtCategoryName.getText());
		} catch (ValidationException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), e.getClazz(), JOptionPane.ERROR_MESSAGE);
		}
		if (result) {
			refreshCategoryList();
		} else {
			JOptionPane.showMessageDialog(null, "Cannot update category.", "Error in updating catgory",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private void clearCategory() {
		txnCategoryCode.setEnabled(true);
		txnCategoryCode.setText("");
		txtCategoryName.setText("");
		txnCategoryCode.requestFocus(true);
		btnAdd.setEnabled(true);
	}

	/*
	private void deleteCategory(CategoryListTable tableModel) {

		boolean result = false;
		result = categoryService.remove(txnCategoryCode.getText().trim());
		if (result) {
			refreshCategoryList();
		} else {
			JOptionPane.showMessageDialog(null, "Cannot delete category.", "Error in deleting catgory",
					JOptionPane.ERROR_MESSAGE);
		}
	}
*/
/*
	private boolean ValidateTextFields() {
		
		 * will commit after doing unittest String message="";
		 * if(txtCategoryCode.getText().isEmpty()){ message=
		 * "Please enter the category code"; } else
		 * if(txtCategoryCode.getText().matches("\\d+")){ message=
		 * "Please enter valid category code"; } else
		 * if(txtCategoryName.getText().isEmpty()){ message=
		 * "Please enter category name"; } else
		 * if(txtCategoryName.getText().matches("\\d+")){ message=
		 * "Please enter valid category name"; }
		 * 
		 * if(!message.equals("")){ JOptionPane.showMessageDialog(null,message,
		 * "Error in checking input", JOptionPane.ERROR_MESSAGE); return false;
		 * }
		 
		return true;
	}
*/	
	private void refreshCategoryList() {
		((CategoryListTable) table.getModel()).fireTableDataChanged();
	}

}
