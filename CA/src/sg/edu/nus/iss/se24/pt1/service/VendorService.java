package sg.edu.nus.iss.se24.pt1.service;

import java.util.List;

import sg.edu.nus.iss.se24.pt1.dao.CategoryDAO;
import sg.edu.nus.iss.se24.pt1.dao.CategoryDAOImpl;
import sg.edu.nus.iss.se24.pt1.dao.VendorDAO;
import sg.edu.nus.iss.se24.pt1.dao.VendorDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import sg.edu.nus.iss.se24.pt1.model.Category;
import sg.edu.nus.iss.se24.pt1.model.Vendor;

/**
 * @author jinxiang
 *
 */
public class VendorService implements VendorServiceI {

	private VendorDAO vendorDAO;
	private CategoryDAO categoryDAO;

	public VendorService() {
		vendorDAO = VendorDAOImpl.getInstance();
		categoryDAO = CategoryDAOImpl.getInstance();
	}
	
	@Override
	public List<Category> getAllCategory() {
		return categoryDAO.findAll();
	}

	@Override
	public List<Vendor> getAllByCategoryCode(String categoryCode) {
		return vendorDAO.findByCategory(categoryCode);
	}

	@Override
	public boolean add(String categoryCode, String vendorName, String vendorDescription) throws ValidationException, VendorExistedException {

		validateVendor(vendorName, vendorDescription);
		vendorDAO.insertVendor(categoryCode, new Vendor(vendorName.trim(), vendorDescription.trim()));
		vendorDAO.commit();
		return true;
	}

	@Override
	public boolean edit(String categoryCode, String vendorName, String vendorDescription) throws ValidationException, VendorNotExistException {
		validateVendor(vendorName, vendorDescription);
		vendorDAO.updateVendor(categoryCode, new Vendor(vendorName.trim(), vendorDescription.trim()));
		vendorDAO.commit();
		return true;
	}

	@Override
	public boolean delete(Category category, Vendor vendor) throws VendorNotExistException {
		vendorDAO.deleteVendor(category.getCategoryCode(), vendor.getVendorName());
		vendorDAO.commit();
		return true;
	}

	private void validateVendor(String vendorName, String vendorDescription) throws ValidationException {
		if (vendorName == null || vendorName.trim().length() == 0)
			throw new ValidationException("Vendor", "Vendor Name Is Empty.");
		if (vendorDescription == null || vendorDescription.trim().length() == 0)
			throw new ValidationException("Vendor", "Description Is Empty.");
		
		//vendor Name can be numeric value
		
		if(vendorDescription.matches("\\d+")){
			throw new ValidationException("Vendor", "Vender description must not be numeric value.");
		}
			
	}

}
