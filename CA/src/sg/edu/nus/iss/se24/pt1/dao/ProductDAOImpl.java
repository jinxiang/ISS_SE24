
package sg.edu.nus.iss.se24.pt1.dao;

import java.util.ArrayList;
import java.util.List;

import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.model.Inventory;
import sg.edu.nus.iss.se24.pt1.model.Product;

/**
 * created by Nilar 
 *
 */
public class ProductDAOImpl extends  GenericFileDAO<Product> implements ProductDAO {

    private static ProductDAOImpl instance = null;
    private ArrayList<Product> data;

    public static ProductDAOImpl getInstance(){
        if(instance == null){
            synchronized (ProductDAOImpl.class){
                if(instance == null){
                    instance = new ProductDAOImpl();
                    return instance;
                }
            }
        }
        return instance;
    }

	private ProductDAOImpl() {
		super(Product.class);
		data = load();
	}
	
	@Override
    public List<Product> findAll() {
        return  data;
    }

    @Override
    public Product findByName(String name) {
        for(Product product:data){
            if(product.getProductName().equalsIgnoreCase(name)){
                return product;
            }
        }
        return  null;
    }

    @Override
    public synchronized boolean insertProduct(Product productToInsert) throws ProductCode_BarCodeExistException{
        Boolean toInsert=true;
        for(Product product:data){
            if(product.getProductID().equalsIgnoreCase(productToInsert.getProductID()) ||
            		(product.getBarCodeNo().equalsIgnoreCase(productToInsert.getBarCodeNo()))){
                toInsert = false;
            }
        }
        if(toInsert) {
        	data.add(productToInsert);
        }
        else
        {
        	throw new ProductCode_BarCodeExistException();
        }
        return toInsert;
    }

    @Override
    public synchronized boolean updateProduct(Product productToUpdate) {
        for(Product product:data){
            if(product.getProductID().equalsIgnoreCase(productToUpdate.getProductID())){
                data.remove(product);
                data.add(productToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized boolean deleteProduct(String productToDelete) {
        for(Product product:data){
            if(product.getProductID().equalsIgnoreCase(productToDelete)){
            	data.remove(product);
                return true;
            }
        }
        return false;
    }
    
    @Override
    public List<Product> findByThresholdQty(){
    	ArrayList<Product> resultProductList= new ArrayList<Product>();
    	for(Product product:data){
    		if(product.getAvailableQty()<product.getReOrderQty()){
    			resultProductList.add(product);
    		}
    	}
    	return resultProductList;
    	
    }
    
    @Override
    public List<Product>findByCategory(String categoryCode){
    	ArrayList<Product> resultProductList= new ArrayList<Product>();
    	if (data.size()>0){
    		for (Product product:data){
    		String productID=product.getProductID();
    		int indexofSperator= productID.indexOf('/');
    		String getCategoryCode= productID.substring(0, indexofSperator);
    		if(getCategoryCode.equals(categoryCode)){
    			resultProductList.add(product);
    			}
    		}
    	}
    	return resultProductList;
    }

	@Override
	public Product findByProductID(String productID) {
        for(Product product:data){
            if(product.getProductID().equalsIgnoreCase(productID)){
                return product;
            }
        }
        return  null;
	}

    @Override
    public synchronized void commit() {
        this.commit(data);
    }

	@Override
	public void insertPurchasedOrder(List<Inventory> inventoryList) {
		GenericFileDAO dao = new GenericFileDAO<>(Inventory.class);
		dao.commit(inventoryList);
	}
	
	@Override
	public Product findByBarCode(String barCode){
		List<Product> listProduct=this.findAll();
		for(Product p: listProduct){
			if(p.getBarCodeNo().equalsIgnoreCase(barCode)){
				return p;
			}
		}
		return null;
		
	}
}
