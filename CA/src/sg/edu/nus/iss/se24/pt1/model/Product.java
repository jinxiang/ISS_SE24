package sg.edu.nus.iss.se24.pt1.model;
/**
 * created by Nilar 
 *
 */
public class Product  {
	private String productID;
	private String productName;
	private String description;
	private Integer availableQty;
	private Double price;
	private String barCodeNo;
	private Integer reOrderQty;
	private Integer orderQty;
	
	private Category category;
	
	public Product() {	}
	
	public Product(String productID, String productName, String description, Integer availableQty, Double price, String barCodeNo, Integer reOrderQty,Integer orderQty){
		this.productID=productID;
		this.productName=productName;
		this.description=description;
		this.availableQty=availableQty;
		this.price=price;
		this.barCodeNo=barCodeNo;
		this.reOrderQty=reOrderQty;
		this.orderQty=orderQty;
	}
	
	public String getProductID(){
		return productID;
	}
	
	public String getProductName(){
		return productName;
	}
	
	public String getDescription(){
		return description;
	}
	
	public Integer getAvailableQty(){
		return availableQty;
	}
	
	public Double getPrice(){
		return price;
	}
	
	public String getBarCodeNo(){
		return barCodeNo;
	}
	
	public Integer getReOrderQty(){
		return reOrderQty;
	}
	
	public Integer getOrderQty(){
		return orderQty;
	}
	
	public String toString(){
		return (productID + "," + productName + "," + description+ "," + availableQty + 
				"," + price + "," + barCodeNo+ "," + reOrderQty + "," + orderQty);
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
}
