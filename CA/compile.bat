set APP_PATH=%~dp0

call %APP_PATH%setenv.bat

set SRC_PATH=%APP_PATH%src
set BIN_PATH=%APP_PATH%classes

dir %SRC_PATH%\*.java /B/S > %APP_PATH%JavaFileList.txt

if exist %BIN_PATH% rmdir /S /Q %BIN_PATH%

PAUSE

mkdir %BIN_PATH% 

"%JAVA_HOME%\bin\javac.exe" -Xlint:deprecation -classpath %SRC_PATH%  -d %BIN_PATH% @%APP_PATH%JavaFileList.txt

del %APP_PATH%JavaFileList.txt
