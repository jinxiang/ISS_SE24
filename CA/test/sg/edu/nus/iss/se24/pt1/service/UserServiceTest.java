package sg.edu.nus.iss.se24.pt1.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author jinxiang
 *
 */
public class UserServiceTest {
	UserServiceI userService;
	
	@Before
	public void init() {
		userService = new UserService();
	}

	@Test
	public void testAuthenticate() {
		assertEquals(userService.authenticate("admin", "admin"), true);
		assertEquals(userService.authenticate(" admin", "admin"), true);
		assertEquals(userService.authenticate("admin ", "admin"), true);
		assertEquals(userService.authenticate("admin", " admin"), true);
		assertEquals(userService.authenticate("admin", "admin "), true);
		assertNotEquals(userService.authenticate("admin", "password"), true);
	}

}
