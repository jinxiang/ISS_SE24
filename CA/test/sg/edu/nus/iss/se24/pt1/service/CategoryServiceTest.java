package sg.edu.nus.iss.se24.pt1.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * created by Nilar 
 *
 */

public class CategoryServiceTest {
	CategoryServiceI categorySerice;
	
	
	
	@Before
	public void setup() throws Exception{ 
		categorySerice= new CategoryService();
		
	}	
	
	
	@Test
	public void isValidCateogryCodeTest(){
		assertTrue(categorySerice.isValidCategoryCode("MOU", "MOUSE"));
		assertFalse(categorySerice.isValidCategoryCode("ALB", "RIBBON"));
	}
	
	
	
	
}
