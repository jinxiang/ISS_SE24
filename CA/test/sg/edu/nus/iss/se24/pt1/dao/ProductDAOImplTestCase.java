package sg.edu.nus.iss.se24.pt1.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import sg.edu.nus.iss.se24.pt1.dao.ProductDAO;
import sg.edu.nus.iss.se24.pt1.dao.ProductDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.model.Product;

/**
 * created by Nilar 
 *
 */
public class ProductDAOImplTestCase {
	ProductDAO productDAO;
	@Before
	public void Setup(){
		productDAO = ProductDAOImpl.getInstance();
	}
	
	@Test
	public void testFindAll(){
		ArrayList<Product> productList= (ArrayList<Product>) productDAO.findAll();
		int count=productList.size();
		assertNotEquals(count, 0);
		//assertNotNull(productList);
	}
	
	@Test
	public void positiveTestFindByName(){
		Product p=productDAO.findByName("NUS Notepad");
		assertNotNull(p);
	}
	
	@Test
	public void negativeTestFindByName(){
		Product p=productDAO.findByName("Free Star");
		assertNull(p);
	}
	
	@Test
	public void positiveTestInsertProduct(){
		try {
			assertTrue(productDAO.insertProduct(new Product("CLO/99","Jupiter Run T-shirt","A nice T-shirt",40,20.40,"11111",20,20)));
			productDAO.commit();
		} catch (ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void negativeTestInsertProduct(){
		try {
			//result should print out the error with exception
			assertFalse(productDAO.insertProduct(new Product("CLO/1","Jupiter Run T-shirt","A nice T-shirt",40,20.40,"22222",20,20)));
			productDAO.commit();
		} catch (ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void positiveTestUpdateProduct(){
		//update Name and retrieve it by Name
		assertTrue(productDAO.updateProduct(new Product("CLO/1","Centenary Jumper11","Moderate nice momento",40,33.5,"7777",20,20)));
		productDAO.commit();
	}
	
	@Test
	public void negativeTestUpdateProduct(){
		assertFalse(productDAO.updateProduct(new Product("CLO/100","Centenary Jumper99","Moderate nice momento",40,33.5,"7777",20,20)));
		productDAO.commit();
	}
	
	
	
	
	@Test
	public void testfindByThresholdQty(){
		//precondition : some of the product must have qty less then threshold
		ArrayList<Product> productList= (ArrayList<Product>) productDAO.findByThresholdQty();
		int count=productList.size();
		assertNotEquals(count, 0);
	}
	
	@Test
	public void testfindByThresholdQty_1(){
		//precondition : some of the product must have qty equal or more then threshold
		ArrayList<Product> productList= (ArrayList<Product>) productDAO.findByThresholdQty();
		int count=productList.size();
		assertEquals(count, 0);
	}
	
	@Test
	public void positiveTestFindByCategory(){
		ArrayList<Product> productList= (ArrayList<Product>) productDAO.findByCategory("CLO");
		int count=productList.size();
		assertNotEquals(count, 0);
		
	}
	
	@Test
	public void negativeTestFindByCategory(){
		ArrayList<Product> productList= (ArrayList<Product>) productDAO.findByCategory("TOY");
		int count=productList.size();
		assertEquals(count, 0);
	}
	
	@Test
	public void findByProductID(){
		assertNull(productDAO.findByProductID("STA/5"));
	}
	
	

}
