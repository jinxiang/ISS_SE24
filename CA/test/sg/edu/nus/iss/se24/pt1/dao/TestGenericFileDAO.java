package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Storekeeper;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

/**
 * Created by Killu on 3/12/2016.
 */
public class TestGenericFileDAO {
    public static void main(String[] args){

        StorekeeperDAO storekeeperDAO = StorekeeperDAOImpl.getInstance();

        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user1","password1")));
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user2","password1")));
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user3","password1")));
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user4","password1")));

        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user1","password1")));
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user2","password1")));
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user3","password1")));
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user4","password1")));

        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user1","password1")));
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user2","password1")));
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user3","password1")));
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user4","password1")));

        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user1","password2")));
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user2","password2")));
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user3","password2")));
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user4","password2")));

        assertTrue(storekeeperDAO.deleteStorekeeper("user1"));
        assertTrue(storekeeperDAO.deleteStorekeeper("user2"));
        assertTrue(storekeeperDAO.deleteStorekeeper("user3"));
        assertTrue(storekeeperDAO.deleteStorekeeper("user4"));

        assertFalse(storekeeperDAO.deleteStorekeeper("user1"));
        assertFalse(storekeeperDAO.deleteStorekeeper("user2"));
        assertFalse(storekeeperDAO.deleteStorekeeper("user3"));
        assertFalse(storekeeperDAO.deleteStorekeeper("user4"));

        ArrayList<Storekeeper> arrayList1 = (ArrayList<Storekeeper>) storekeeperDAO.findAll();


    }
}
