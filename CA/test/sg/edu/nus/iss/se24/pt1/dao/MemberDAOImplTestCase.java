package sg.edu.nus.iss.se24.pt1.dao;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.model.Member;
/**
 * created by Nilar 
 *
 */
public class MemberDAOImplTestCase {

	MemberDAO memberDAO;
	@Before
	public void Setup(){
		memberDAO = MemberDAOImpl.getInstance();
	}
	
	@Test
	public void findAllTest(){
		assertNotNull(memberDAO.findAll());
	}
	
	@Test
	public void findByIDTest() {
		//test with memberID that is already in dat file
		assertNotNull(memberDAO.findByID("F42563743156"));
		
		//test with memberID that i not existing dat file
		assertNull(memberDAO.findByID("HELLO12345"));
	}
	
	
	@Test
	public void insertMemberTest(){
		//test with member that is not exist in dat file
		assertTrue(memberDAO.insertMember(new Member("Joe", "789012360NNN",-1)));
		memberDAO.commit();
		//test with member ID that is already exist in dat file
		assertFalse(memberDAO.insertMember(new Member("Bryan","F42563743156",90)));
		memberDAO.commit();
	}
	
	
	
	@Test
	public void updateMemberTest(){
		//test with member that is existed in system
		assertTrue(memberDAO.updateMember(new Member("Ang Lee1", "R64565FG4",90)));
		memberDAO.commit();
		//test with member that is not existed in system
		assertFalse(memberDAO.updateMember(new Member("Ang Lee1", "9999999999",90)));
		memberDAO.commit();
	}
	
	

}
