package sg.edu.nus.iss.se24.pt1.dao;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import sg.edu.nus.iss.se24.pt1.dao.CategoryDAO;
import sg.edu.nus.iss.se24.pt1.dao.CategoryDAOImpl;
import sg.edu.nus.iss.se24.pt1.exception.CategoryCodeExistedException;
import sg.edu.nus.iss.se24.pt1.model.Category;
/**
 * created by Nilar 
 *
 */
public class CategoryDAOImplTestCase {

	CategoryDAO categoryDAO;
	
	@Before
	public void setup() throws Exception{
	  categoryDAO =CategoryDAOImpl.getInstance();
	}
	
	@Test
	public void testfindAll() {
		ArrayList<Category> categoryList = (ArrayList<Category>) categoryDAO.findAll();
		int count=categoryList.size();
		assertNotEquals(count, 0);
		
	}
	
	@Test
	public void testfindByName(){
		Category c= (Category)categoryDAO.findByName("Bags");
		assertNotNull(c);
	}
	
	@Test
	public void positiveTestInsertCategory(){
		//only run one time otherwise will return false since already inserted in second time run
		try {
			assertTrue(categoryDAO.insertCategory(new Category("OAT22","Bottle22")));
			categoryDAO.commit();
		} catch (CategoryCodeExistedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	@Test
	public void negativeTestInsertCategory(){
		try {
			//result should throw error...
			assertFalse(categoryDAO.insertCategory(new Category("MUG","Mugs")));
			categoryDAO.commit();
		} catch (CategoryCodeExistedException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
	}
	
	@Test
	public void positiveTestUpdateCategory(){
		assertTrue(categoryDAO.updateCategory(new Category("MUG","Pretty Mugs222")));
		categoryDAO.commit();
	}
	
	@Test
	public void negativeTestUpdateCategory(){
		assertFalse(categoryDAO.updateCategory(new Category("PENS","Pen")));
		
	}
	
	@Test
	public void negativeTestDeleteCategory(){
		assertFalse(categoryDAO.deleteCategory("PIN"));
	}
	
	
}
