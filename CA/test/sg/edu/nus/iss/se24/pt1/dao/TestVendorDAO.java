package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.exception.VendorExistedException;
import sg.edu.nus.iss.se24.pt1.exception.VendorNotExistException;
import org.junit.Test;
import sg.edu.nus.iss.se24.pt1.model.Vendor;

/**
 * Created by xinqu on 4/2/2016.
 */

public class TestVendorDAO {
    @Test
    public void testVendorDAO() throws VendorExistedException, VendorNotExistException {

        VendorDAOImpl vendorDAO = VendorDAOImpl.getInstance();

        vendorDAO.insertVendor("TST",new Vendor("vendor1","descr1"));
        vendorDAO.commit();

        vendorDAO.updateVendor("TST",new Vendor("vendor1", "descr2"));
        vendorDAO.commit();

        vendorDAO.deleteVendor("TST","vendor1");
        vendorDAO.commit();
        return;

    }
}
