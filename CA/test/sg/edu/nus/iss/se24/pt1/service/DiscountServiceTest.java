package sg.edu.nus.iss.se24.pt1.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.exception.ValidationException;

/**
 * @author Myo Wai Yan Kyaw
 *
 */

public class DiscountServiceTest {
	DiscountService discountSerice;
	
	
	
	@Before
	public void setup() throws Exception{ 
		discountSerice= new DiscountService();
		
	}
	
	@Test
	public void removeTest(){
		assertTrue(discountSerice.remove("MEMBER_FIRST"));
		assertFalse(discountSerice.remove("PRESIDNT_BDAY"));
	}
	
	@Test
	public void isValidDiscountCodeTest() throws ValidationException{
		assertTrue(discountSerice.add("NATIONAL_DAY", "National Day","2016-08-09","1","20",'A'));
		assertFalse(discountSerice.add("ORIENTATION_DAY", "Orientation Day","2014-02-02","3","50",'A'));
	}
	
	
	
	
}
