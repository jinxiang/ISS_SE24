package sg.edu.nus.iss.se24.pt1.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.model.Transaction;


/**
 * created by Nilar 
 *
 */
public class TransactionDAOTest {

	TransactionDAO transactionDAO;
	
	@Before
	public void setup() throws Exception{
	 transactionDAO=TransactionDAOImpl.getInstance();
	}
	
	@Test
	public void findAllTest(){
		ArrayList<Transaction>transactionlist= (ArrayList<Transaction>) transactionDAO.findAll();
		assertNotNull(transactionlist);
	}
	
	@Test
	public void findByIDTogetherWithProductTest(){
		//pre condition that transaction no and productID should not be in the dat file
		Transaction t=transactionDAO.findByID(3,"STA/2");
		assertNotNull(t);
		//pre condition that transaction no and productID should not be in the dat file
		Transaction t1=transactionDAO.findByID(99,"STA/99");
		assertNull(t1);
	}
	
	@Test
	public void findByIDTest(){
		//pre condition that transaction no should not be in the dat file
		List<Transaction> transaction=transactionDAO.findByID(1);
		int count=transaction.size();
		assertNotEquals(0, count);
		
		//pre condition that transaction no and productID should not be in the dat file
		List<Transaction> transaction2=transactionDAO.findByID(99);
		int count2=transaction2.size();
		assertEquals(0, count2);
	}
	
	@Test
	public void findByDateTest() throws ParseException{
		
		Date date, date2;
		date = new SimpleDateFormat("yyyy-MM-dd").parse("2013-09-28");
		List<Transaction> listtransaction=transactionDAO.findByDate(date);
		int count=listtransaction.size();
		assertNotEquals(0, count);
		
		
		date2 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-12-31");
		List<Transaction> listtransaction2=transactionDAO.findByDate(date2);
		int count2=listtransaction2.size();
		assertEquals(0, count2);
		

	}
	
	@Test
	public void findByProductTest(){
		List<Transaction>listTransaction=transactionDAO.findByProduct("STA/1");
		int count=listTransaction.size();
		assertNotEquals(0, count);
		
		
		List<Transaction>listTransaction2=transactionDAO.findByProduct("STA/909");
		int count2=listTransaction2.size();
		assertEquals(0, count2);
	}
	
	@Test
	public void insertTransactionTest(){
		
		List<Transaction>listTransaction=new ArrayList<Transaction>();
		Transaction t=new Transaction(33, "CLO/9", "098764367", 3);
		Transaction t1=new Transaction(35,"STA/5", "R64565FG4", 2);
		listTransaction.add(t);
		listTransaction.add(t1);
		assertTrue(transactionDAO.insertTransaction(listTransaction));
		transactionDAO.commit();
	
	}
	
	

}
