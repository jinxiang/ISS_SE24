package sg.edu.nus.iss.se24.pt1.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.exception.ProductCodeInvalidException;
import sg.edu.nus.iss.se24.pt1.exception.ProductCode_BarCodeExistException;
import sg.edu.nus.iss.se24.pt1.exception.ValidationException;
import sg.edu.nus.iss.se24.pt1.model.Transaction;
/**
 * created by Nilar 
 *
 */


public class ProductServiceTest {
ProductServiceI productService;
	
	
	@Before
	public void setup() throws Exception{ 
		productService= new ProductService();
	}
	
	@Test
	public void addTest() {
		try {
			assertTrue(productService.add("TES/1", "TEST1",  "TEST1", "100", "4.5", "8904", "20", "20"));
		} catch (ProductCodeInvalidException | ValidationException | ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertFalse(productService.add("TEST/1", "TEST1",  "TEST1", "100", "4.5", "8904", "20", "20"));
		} catch (ProductCodeInvalidException | ValidationException | ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertFalse(productService.add("678/1", "TEST1",  "TEST1", "100", "4.5", "8904", "20", "20"));
		} catch (ProductCodeInvalidException | ValidationException | ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertFalse(productService.add("0000/1", "TEST1",  "TEST1", "100", "4.5", "8904", "20", "20"));
		} catch (ProductCodeInvalidException | ValidationException | ProductCode_BarCodeExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getProductIDTest(){
		//precondition product dat file must have CLO/2 as last record for CLO category
		assertEquals(productService.getProductID("CLO"), "CLO/3");
		
		//precondition product dat file must not have DEV category product
		assertEquals(productService.getProductID("DEV"), "DEV/1");
	}
	
	@Test
	public void isDuplicateTest() throws ValidationException{
		assertTrue(productService.isDuplicate("Dream catcher", "ACC"));
		assertFalse(productService.isDuplicate("Key Chain NUS", "ACC"));
	}
	
	@Test
	public void removeProductByCategoryTest(){
		assertTrue(productService.removeProductByCategory("CLO"));
	}
	
	@Test
	public void getTransactionListTest() throws ParseException {
		String myTextDate1 = "30/03/2014";
		String myTextDate2 = "28/09/2013";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = sdf.parse(myTextDate1);
		Date date2 = sdf.parse(myTextDate2);
		List<Transaction> list1 = productService.getTransactionList(date1);
		List<Transaction> list2 = productService.getTransactionList(date2);
		assertTrue(list1.size() == 0);
		assertTrue(list2.size() > 0);
	}
}
