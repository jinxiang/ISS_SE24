package sg.edu.nus.iss.se24.pt1.exception;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class DuplicatedMemberIDExceptionTest {

	@Test
	public void testDuplicatedMemberIDException() {
		DuplicatedMemberIDException exception1 = new DuplicatedMemberIDException("F201");
		String string1 = "Member ID : F201 is duplicated";
		String string2 = "Member ID : F202 is duplicated";
		Assert.assertNotNull(exception1.getMessage());
		Assert.assertEquals(string1, exception1.getMessage());
	}

}
