package sg.edu.nus.iss.se24.pt1.dao;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by xinqu on 3/19/2016.
 */
public class DAOTestRunner{
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestStorekeeperDAO.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }

}
