package sg.edu.nus.iss.se24.pt1.dao;

import sg.edu.nus.iss.se24.pt1.model.Storekeeper;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Killu on 3/12/2016.
 */

public class TestStorekeeperDAO {
    @Test
    public void testStoreKeeperDAO() {

        StorekeeperDAO storekeeperDAO = StorekeeperDAOImpl.getInstance();

        //check whether DAO is allow to insert for a new entry
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user1","password1")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user2","password1")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user3","password1")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.insertStorekeeper(new Storekeeper("user4","password1")));
        storekeeperDAO.commit();

        //check whether DAO will reject duplicate entry
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user1","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user2","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user3","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.insertStorekeeper(new Storekeeper("user4","password1")));
        storekeeperDAO.commit();

        //check whether DAO will reject update if no change
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user1","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user2","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user3","password1")));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.updateStorekeeper(new Storekeeper("user4","password1")));
        storekeeperDAO.commit();

        //check whether DAO will allow update
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user1","password2")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user2","password2")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user3","password2")));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.updateStorekeeper(new Storekeeper("user4","password2")));
        storekeeperDAO.commit();

        //check whether DAO will allow delete
        assertTrue(storekeeperDAO.deleteStorekeeper("user1"));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.deleteStorekeeper("user2"));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.deleteStorekeeper("user3"));
        storekeeperDAO.commit();
        assertTrue(storekeeperDAO.deleteStorekeeper("user4"));
        storekeeperDAO.commit();

        //check whether DAO will reject delete if non-exists
        assertFalse(storekeeperDAO.deleteStorekeeper("user1"));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.deleteStorekeeper("user2"));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.deleteStorekeeper("user3"));
        storekeeperDAO.commit();
        assertFalse(storekeeperDAO.deleteStorekeeper("user4"));
        storekeeperDAO.commit();


    }
}
