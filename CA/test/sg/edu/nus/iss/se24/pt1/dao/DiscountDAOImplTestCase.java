package sg.edu.nus.iss.se24.pt1.dao;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.model.Discount;
/**
 * @author Myo Wai Yan Kyaw
 *
 */
public class DiscountDAOImplTestCase {

	DiscountDAO discountDAO;
	@Before
	public void Setup(){
		discountDAO = DiscountDAOImpl.getInstance();
	}
	
	@Test
	public void findAllTest(){
		assertNotNull(discountDAO.findAll());
	}
	
	@Test
	public void findByCodeTest() {
		//test with discountID that is already in dat file
		assertNotNull(discountDAO.findByCode("PRESIDENT_BDAY"));
		
		//test with discountID that i not existing dat file
		assertNull(discountDAO.findByCode("NATIONAL_DAY"));
	}
	
	
	@Test
	public void insertDiscountTest(){
		//test with discount that is not exist in dat file
		assertTrue(discountDAO.insertDiscount(new Discount("NATIONAL_DAY", "National Day","2016-08-09","1",20,'A')));
		//test with discount that is already exist in dat file
		assertFalse(discountDAO.insertDiscount(new Discount("MEMBER_FIRST", "First purchase by member","ALWAYS","ALWAYS",20,'M')));
	}
	
	
	
	@Test
	public void updateDiscountTest(){
		//test with discount that is existed in system
		assertTrue(discountDAO.updateDiscount(new Discount("MEMBER_FIRST", "First purchase by member","ALWAYS","ALWAYS",20,'M')));
		//test with discount that is not existed in system
		assertFalse(discountDAO.updateDiscount(new Discount("NATIONAL_DAY", "National Day","2016-08-09","1",20,'A')));
		
	}
	
	@Test
	public void deleteDiscountTest(){
		//delete discount which is already exists in dat file
		assertTrue(discountDAO.deleteDiscount("NATIONAL_DAY"));
		//delete discount which is not exist in dat file
		assertFalse(discountDAO.deleteDiscount("NATIONAL_DAY"));
	}

}
