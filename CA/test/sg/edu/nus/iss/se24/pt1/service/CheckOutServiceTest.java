package sg.edu.nus.iss.se24.pt1.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sg.edu.nus.iss.se24.pt1.model.Transaction;

public class CheckOutServiceTest {
	
	private CheckOutService service;

	@Before
	public void setUp() throws Exception {
		service = new CheckOutService();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void addLineTest() {
		Transaction t1 = service.addLine("1234", 3);
		Transaction t2 = service.addLine("9876", 4);
		assertNotNull(t1);
		assertEquals(service.getLines().size(), 2);
	}
	
	@Test
	public void editLineTest() {
		Transaction t1 = service.addLine("1234", 3);
		Transaction t2 = service.addLine("9876", 4);
		
		Transaction t3 = service.editLine(t2, 10);
		
		assertNotNull(t3);
		assertTrue(t3.getPurchasedQuantity() == 10);
	}
	
	@Test
	public void deleteLineTest() {
		Transaction t1 = service.addLine("1234", 3);
		Transaction t2 = service.addLine("9876", 4);
		
		assertNotNull(t2);
		assertTrue(service.deleteLine(t2));
		assertEquals(service.getLines().size(), 1);
	}
	
	@Test
	public void getNewTransactionIDTest() {
		Integer id = service.getNewTransactionID();
		
		assertNotNull(id);
	}

}
